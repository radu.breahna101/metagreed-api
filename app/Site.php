<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable =[
        'name',
        'url',
        'homepage_selector',
        'single_post_selector',
        'latest_post_selector',
    ];
}
