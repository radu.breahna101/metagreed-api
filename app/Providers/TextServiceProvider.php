<?php

namespace App\Providers;

use App\Services\Implementations\StringParsing\Text;
use App\Services\Interfaces\TextInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class TextServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [TextInterface::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            TextInterface::class,
            Text::class,
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
