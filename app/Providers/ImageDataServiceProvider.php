<?php

namespace App\Providers;

use App\Services\Implementations\Images\ImageData;
use App\Services\Implementations\Images\ImageDataFactory;
use App\Services\Implementations\Images\ImageOptimizer;
use App\Services\Implementations\Images\ImageSaver;
use App\Services\Interfaces\ImageDataFactoryInterface;
use App\Services\Interfaces\ImageDataInterface;
use App\Services\Interfaces\ImageOptimizerInterface;
use App\Services\Interfaces\ImageSaverInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class ImageDataServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [ImageDataInterface::class, ImageDataFactoryInterface::class, ImageSaverInterface::class, ImageOptimizerInterface::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ImageDataInterface::class,
            ImageData::class,
            );

        $this->app->bind(
            ImageDataFactoryInterface::class,
            ImageDataFactory::class,
            );

        $this->app->bind(
            ImageSaverInterface::class,
            ImageSaver::class);

        $this->app->bind(
            ImageOptimizerInterface::class,
            ImageOptimizer::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
