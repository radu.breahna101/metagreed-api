<?php

namespace App\Providers;

use App\Services\Implementations\PostBuilding\Draft;
use App\Services\Implementations\PostBuilding\DraftRepository;
use App\Services\Interfaces\DraftInterface;
use App\Services\Interfaces\DraftRepositoryInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class DraftServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function provides()
    {
        return [DraftInterface::class, DraftRepositoryInterface::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            DraftInterface::class,
            Draft::class,
            );

        $this->app->bind(
            DraftRepositoryInterface::class,
            DraftRepository::class,
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
