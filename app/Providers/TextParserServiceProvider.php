<?php

namespace App\Providers;

use App\Services\Implementations\StringParsing\TextParser;
use App\Services\Interfaces\TextParserInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class TextParserServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [TextParserInterface::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            TextParserInterface::class,
            TextParser::class,
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
