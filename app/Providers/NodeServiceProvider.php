<?php

namespace App\Providers;

use App\Services\Implementations\Scraping\Node;
use App\Services\Interfaces\DomNodeInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\DomCrawler\Crawler;

class NodeServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function provides()
    {
        return [DomNodeInterface::class, Crawler::class];
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            DomNodeInterface::class,
            Node::class,
            );

        $this->app->when(Node::class)
            ->needs(Crawler::class)
            ->give(function () {
                return new Crawler();
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
