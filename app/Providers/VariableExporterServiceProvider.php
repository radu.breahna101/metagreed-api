<?php

namespace App\Providers;

use App\Services\Interfaces\VariableExporterInterface;
use App\Services\Utilities\VariableExporter;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class VariableExporterServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [VariableExporterInterface::class];
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            VariableExporterInterface::class,
            VariableExporter::class,
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
