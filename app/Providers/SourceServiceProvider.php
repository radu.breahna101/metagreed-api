<?php

namespace App\Providers;

use App\Services\Implementations\PostBuilding\Source;
use App\Services\Interfaces\SourceInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class SourceServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [SourceInterface::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            SourceInterface::class,
            Source::class,
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
