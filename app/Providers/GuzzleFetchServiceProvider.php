<?php

namespace App\Providers;

use App\Services\Implementations\DataFetching\GuzzleFetch;
use App\Services\Interfaces\FetchInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class GuzzleFetchServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [Client::class, FetchInterface::class];
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->when(GuzzleFetch::class)
            ->needs(Client::class)
            ->give(function () {
                return new Client();
            });

        $this->app->bind(
            FetchInterface::class,
            GuzzleFetch::class,
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
