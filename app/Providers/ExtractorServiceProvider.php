<?php

namespace App\Providers;

use App\Services\Implementations\Scraping\Extractor;
use App\Services\Interfaces\ExtractorInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\DomCrawler\Crawler;

class ExtractorServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function provides()
    {
        return [ExtractorInterface::class, Crawler::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ExtractorInterface::class,
            Extractor::class,
            );
        $this->app->when(Extractor::class)
            ->needs(Crawler::class)
            ->give(function () {
                return new Crawler();
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
