<?php

namespace App\Providers;

use App\Services\Implementations\FileManagement\FileManager;
use App\Services\Interfaces\FileManagerInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class FileManagerServiceProvider extends ServiceProvider implements DeferrableProvider
{

    public function provides()
    {
        return [FileManagerInterface::class];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            FileManagerInterface::class,
            FileManager::class,
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
