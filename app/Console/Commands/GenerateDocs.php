<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateDocs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'docs:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'On local environments, generate php docs with ide helper package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->alert('Generating model docs');
        $this->call('ide-helper:models');
        $this->alert('Generating meta');
        $this->call('ide-helper:meta');
        //$this->alert('Generating helper');
        //$this->call('clear-compiled');
        //$this->call('config:clear');
        //$this->call('ide-helper:generate');
    }
}
