<?php

namespace App\Services\Utilities;

use App\Services\Interfaces\VariableExporterInterface;

class VariableExporter implements VariableExporterInterface
{
    private array $array;

    private string $filename;

    private string $exportedString;

    public function export()
    {
        var_dump($this->formatArray());
        die();
    }

    public function exportToFile(): void
    {
        $this->formatArray();
        file_put_contents($this->filename, $this->exportedString);
        chmod($this->filename, 0777);
    }

    private function formatArray()
    {
        if (! isset($this->array)) {
            $this->exportedString = '[];';
        }

        $arrayString = var_export($this->array, true);
        $arrayString = preg_replace('/\d+ => (\n|)/m', '', $arrayString);

        $this->exportedString = $arrayString.';';
    }

    /**
     * @param array $array
     * @return VariableExporter
     */
    public function setArray(array $array): VariableExporterInterface
    {
        $this->array = $array;

        return $this;
    }

    /**
     * @param string $filename
     * @return VariableExporter
     */
    public function setFilename(string $filename): VariableExporterInterface
    {
        $this->filename = $filename;

        return $this;
    }

    public function getExportedString(): string
    {
        $this->formatArray();

        return $this->exportedString;
    }
}
