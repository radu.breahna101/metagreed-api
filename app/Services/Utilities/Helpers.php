<?php

namespace App\Services\Utilities;

use Illuminate\Support\Facades\Log;

class Helpers
{
    /**
     * @param string $variable
     * @param string $file
     * @return mixed
     */
    public static function getVariableFromFile(string $variable, string $file)
    {
        ob_start();
        include $file;
        ob_end_clean();

        return $$variable;
    }

    /**
     * @param array $needle
     * @param array $haystack
     * @return bool
     */
    public static function arrayContainsArray(array $needle, array $haystack)
    {
        $intersection = array_intersect($needle, $haystack);

        return $needle == $intersection;
    }

    public static function invokeMethod(&$object, $methodName, ...$parameters)
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method     = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public static function fileSizeInKilobytes(string $filePath): int//kibibytes
    {
        $bytes = filesize($filePath);

        return self::bytesToKilobytes($bytes);
    }

    public static function bytesToKilobytes(int $bytes) : int
    {
        return round($bytes / 1024);
    }
}
