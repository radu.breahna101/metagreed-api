<?php

namespace App\Services\Implementations\FileManagement;

use App\Services\Interfaces\FileManagerInterface;
use Illuminate\Support\Facades\Storage;

class FileManager implements FileManagerInterface
{

    private string $content;

    private string $disk;

    private string $folderName = '';

    private string $fileName;

    private bool $isSaved = false;

    private string $filePath;

    public function setStringContent(string $content): FileManagerInterface
    {
        $this->content = $content;

        return $this;
    }

    public function setFolderName(string $folderName): FileManagerInterface
    {
        $this->folderName = $folderName;

        return $this;
    }

    public function setFileName(string $fileName): FileManagerInterface
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function setDisk(string $disk): FileManagerInterface
    {

        $this->disk = $disk;

        return $this;
    }

    public function getPathToSavedFile(): string
    {
        return $this->isSaved ?
            Storage::disk($this->disk)->path($this->filePath)
            :
            '';
    }

    private function initializeFilePath()
    {
        $this->filePath = ltrim($this->folderName.'/'.$this->fileName, '/');
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        $this->setIsSaved(false);
        $this->validateRequiredAttributes();
        $this->initializeFilePath();
        $this->saveFile();
        $this->verifyIfFileSavedAtLocation();
    }

    private function setIsSaved(bool $isSaved)
    {
        $this->isSaved = $isSaved;
    }

    private function saveFile()
    {
        Storage::disk($this->disk)->put($this->filePath, $this->content);
    }

    private function verifyIfFileSavedAtLocation()
    {
        $this->isSaved = Storage::disk($this->disk)->exists($this->filePath);
    }

    /**
     * @throws \Exception
     */
    private function validateRequiredAttributes()
    {
        $valid = isset($this->content) && isset($this->fileName);

        if (! $valid) {
            throw new \Exception("File content or file name missing");
        }
    }

    public function isSaved(): bool
    {
        return $this->isSaved;
    }

}
