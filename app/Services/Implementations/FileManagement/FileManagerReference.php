<?php

namespace App\Classes\Logic;

use App\Services\Interfaces\FileManagerInterface;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileManagerReference
{
    const DISK_SOFTWARE    = 'images';

    private $file;

    private string $absolutePath;

    private string $url;

    private string $disk;

    private  string $folder;

    private string $name;

    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * @param $path
     * @param null $disk
     * @return UrlGenerator|string
     */
    public static function getAbsoluteUrlFromPath($path, $disk = null)
    {
        return url(Storage::url(ltrim($disk.'/'.$path), '/'));
    }

    /**
     * @param string $file
     * @param string|null $disk
     * @return bool
     */
    public static function deleteFile(string $file, string $disk = null)
    {
        return $disk ? Storage::disk($disk)->delete($file) : Storage::delete($file);
    }

    /**
     * @param string $file
     * @param string|null $disk
     * @return bool
     */
    public static function fileExists(string $file, string $disk = null)
    {
        return $disk ? Storage::disk($disk)->exists($file) : Storage::exists($file);
    }

    /**
     * @param string $disk
     * @param string $path
     * @return File
     */
    public static function getFile(string $disk, string $path): File
    {
        return new File(Storage::disk($disk)->path($path));
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (isset($this->disk) && isset($this->name) && isset($this->file)) {
            if ($this->file instanceof UploadedFile || $this->file instanceof File) {
                Storage::disk($this->disk)->putFileAs($this->folder, $this->file, $this->name);
            } else {
                Storage::disk($this->disk)->put($this->folder.'/'.$this->name, $this->file);
            }

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAbsolutePath(): string
    {
        $this->absolutePath = $this->absolutePath ?? Storage::disk($this->disk)->path(ltrim($this->folder.'/'.$this->name, '/'));

        return $this->absolutePath;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        $this->url = $this->url ?? Storage::disk($this->disk)->url($this->folder.'/'.$this->name);

        return $this->url;
    }

    /**
     * @param string $folder
     * @return FileManagerReference
     */
    public function setFolder(string $folder): FileManagerReference
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * @param string $name
     * @return FileManagerReference
     */
    public function setName(string $name): FileManagerReference
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisk(): string
    {
        return $this->disk;
    }

    /**
     * @param string $disk
     * @return FileManagerReference
     */
    public function setDisk(string $disk): FileManagerReference
    {
        $this->disk = $disk;

        return $this;
    }

}
