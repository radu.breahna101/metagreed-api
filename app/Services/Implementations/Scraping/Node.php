<?php

namespace App\Services\Implementations\Scraping;

use App\Services\Interfaces\DomNodeInterface;
use Symfony\Component\DomCrawler\Crawler;

class Node implements DomNodeInterface
{
    private string $content;

    private string $selector;

    private Crawler $crawler;

    /**
     * Node constructor.
     *
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     */
    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    public function setContent(string $content): DomNodeInterface
    {
        $this->content = $content;

        return $this;
    }

    public function setSelector(string $selector): DomNodeInterface
    {
        $this->selector = $selector;

        return $this;
    }

    public function getAttribute(string $attribute): string
    {
        $this->setTargetNode();

        return $this->extractAttribute($attribute);
    }

    private function setTargetNode()
    {
        $this->addContentToCrawler();
        $this->applySelector();
        $this->selectFirstNodeFromCrawler();
    }

    private function extractAttribute(string $attribute)
    {
        return $this->crawler->attr($attribute);
    }

    private function selectFirstNodeFromCrawler()
    {
        $this->crawler = $this->crawler->first();
    }

    private function applySelector()
    {
        $this->crawler = $this->crawler->filter($this->selector);
    }

    private function addContentToCrawler()
    {
        $this->crawler->addContent($this->content);
    }

}
