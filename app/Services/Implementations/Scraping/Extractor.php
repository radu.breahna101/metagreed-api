<?php

namespace App\Services\Implementations\Scraping;

use App\Services\Interfaces\ExtractorInterface;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;
use Faker\Generator as Faker;

class Extractor implements ExtractorInterface
{
    /**
     * @var \Symfony\Component\DomCrawler\Crawler
     */
    private Crawler $crawler, $children;

    private string $content;

    /**
     * @var string
     */
    private string $mainSelector;

    /**
     * @var array[]
     */
    private array  $extractionTags;

    /**
     * @var array
     */
    private array $extractedAttributes = [];

    /**
     * @return array
     */
    private function getExtractedAttributes(): array
    {
        return $this->extractedAttributes;
    }

    /**
     * @var string
     */
    private string $filter;

    /**
     * @var array
     */
    private array $filteredChildNodes;

    /**
     * Extractor constructor.
     *
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     */
    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    public function setContent(string $content): ExtractorInterface
    {
        $this->content = $content;

        return $this;
    }

    public function setExtractionTags(array $extractionTags): ExtractorInterface
    {
        $this->extractionTags = $extractionTags;

        return $this;
    }

    public function setContainerSelector(string $mainSelector): ExtractorInterface
    {
        $this->mainSelector = $mainSelector;

        return $this;
    }

    public function extract(): array
    {
        $this->addContentToCrawler();
        $this->filterByMainSelector();
        $this->extractChildNodes();
        $this->buildCssSelectorFromTagsArray();
        $this->applyFilterToChildNodes();
        $this->parseFilteredChildNodes();

        return $this->getExtractedAttributes();
    }

    private function addContentToCrawler()
    {
        $this->crawler->addContent($this->content);
    }

    private function filterByMainSelector()
    {
        $this->crawler = $this->crawler->filter($this->mainSelector);
    }

    private function extractChildNodes()
    {
        $this->children = $this->crawler->children();
    }

    private function buildCssSelectorFromTagsArray()
    {
        $this->filter = implode(',', array_keys($this->extractionTags)); //img,p
    }

    private function applyFilterToChildNodes()
    {
        $filteredChildNodes = [];
        $this->children->each(function ($child) use (&$filteredChildNodes) {
            /**@var Crawler $child * */
            $filteredChildNodes[] = $child->filter($this->filter);
        });

        $this->filteredChildNodes = $filteredChildNodes; // at this stage all nodes are img and p
    }

    private function parseFilteredChildNodes()
    {
        foreach ($this->filteredChildNodes as $childTag) {
            $this->extractAttributesFromTags($childTag);
        }
    }

    private function extractAttributesFromTags(Crawler $childTags)
    {
        $section = [];
        $childTags->each(function ($tag) use (&$section) {
            /**@var Crawler $tag * */
            $attributeValues = $this->getAttributeByNodeType($tag);
            $nodeName        = $tag->nodeName();
            $this->sanitizeAttributes($section, $attributeValues, $nodeName);
        });

        $this->collectSections($section);
    }

    private function collectSections(array $section)
    {
        if (empty($section)) {
            return;
        }
        $this->extractedAttributes[$this->composeSectionKey()] = $section;
    }

    private function composeSectionKey(): string
    {
        return self::SECTION_PREFIX.'_'.count($this->extractedAttributes);
    }

    private function getAttributeByNodeType(Crawler $tag): array
    {
        switch ($tag->nodeName()) {
            case self::TAG_IMAGE:
                return $this->getImageAttributeValues($tag);
                break;
            case self::TAG_PARAGRAPH:
                return $this->getParagraphAttributeValues($tag);
                break;
            default :
                return [];
        }
    }

    private function getImageAttributeValues(Crawler $tag): array
    {
        $attributeNames = $this->extractionTags[self::TAG_IMAGE];
        $values         = [];

        foreach ($attributeNames as $name) {
            $values[$name] = $tag->attr($name);
        }

        return $values;
    }

    private function getParagraphAttributeValues(Crawler $tag): array
    {
        return [
            self::ATTRIBUTE_TEXT => $tag->text(),
        ];
    }

    private function sanitizeAttributes(array &$section, array $attributeValues, string $nodeName)
    {
        $sanitizedValues = array_map(fn($value
        ) => trim(html_entity_decode($value), " \t\n\r\0\x0B\xC2\xA0"), $attributeValues);

        $sanitizedValues = array_filter($sanitizedValues, fn($value) => ! empty($value));

        if (! empty(array_values($sanitizedValues))) {
            $sanitizedValues[self::TAG_IDENTIFIER] = $nodeName;
            $section[]                             = $sanitizedValues;
        }
    }
}
