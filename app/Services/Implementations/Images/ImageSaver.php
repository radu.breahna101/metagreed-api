<?php

namespace App\Services\Implementations\Images;

use App\Services\Interfaces\FetchInterface;
use App\Services\Interfaces\FileManagerInterface;
use App\Services\Interfaces\ImageDataInterface;
use App\Services\Interfaces\ImageOptimizerInterface;
use App\Services\Interfaces\ImageSaverInterface;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ImageSaver implements ImageSaverInterface
{

    private FetchInterface $fetch;

    private ImageDataInterface $imageData;

    private FileManagerInterface $fileManager;

    private ImageOptimizerInterface $imageOptimizer;

    private string $folderName;

    private string $fileName;

    private string $imageContent;

    /**
     * ImageSaver constructor.
     *
     * @param \App\Services\Interfaces\FetchInterface $fetch
     * @param \App\Services\Interfaces\FileManagerInterface $fileManager
     * @param \App\Services\Interfaces\ImageOptimizerInterface $imageOptimizer
     */
    public function __construct(
        FetchInterface $fetch,
        FileManagerInterface $fileManager,
        ImageOptimizerInterface $imageOptimizer
    ) {
        $this->fetch          = $fetch;
        $this->fileManager    = $fileManager;
        $this->imageOptimizer = $imageOptimizer;
    }

    public function setImageData(ImageDataInterface $imageData): ImageSaverInterface
    {
        $this->imageData = $imageData;

        return $this;
    }

    public function save()
    {
        $this->generateFolderName();
        $this->initializeFileManager();
        $this->generateFileName();
        $this->setImageContent();
        $this->saveImage();
        $this->optimizeSavedImage();
    }

    private function initializeFileManager()
    {
        $this->fileManager->setDisk(FileManagerInterface::DISK_IMAGES);
        $this->fileManager->setFolderName($this->folderName);
    }

    private function generateFolderName()
    {
        $time             = Carbon::now();
        $this->folderName = $time->year.'_'.$time->month;
    }

    private function generateFileName()
    {
        $url       = $this->imageData->getUrl();
        $info      = pathinfo($url);
        $extension = $info['extension'];

        $this->fileName = Str::random(40).'.'.$extension;
    }

    private function setImageContent()
    {
        $this->imageContent = $this->fetch->
        setUrl($this->imageData->getUrl())->
        getContent();
    }

    private function optimizeSavedImage()
    {
        $pathToFile = $this->fileManager->getPathToSavedFile();
        $this->imageOptimizer->setPath($pathToFile)->optimize();
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->folderName.'/'.$this->fileName;
    }

    private function saveImage(): void
    {
        $this->fileManager->
        setStringContent($this->imageContent)->
        setFileName($this->fileName)->
        save();
    }
}
