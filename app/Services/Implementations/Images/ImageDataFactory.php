<?php

namespace App\Services\Implementations\Images;

use App\Services\Interfaces\ImageDataFactoryInterface;
use App\Services\Interfaces\ImageDataInterface;

class ImageDataFactory implements ImageDataFactoryInterface
{

    public static function makeImageData(): ImageDataInterface
    {
        return resolve(ImageDataInterface::class);
    }
}
