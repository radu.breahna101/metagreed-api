<?php

namespace App\Services\Implementations\Images;

use App\Services\Interfaces\ImageOptimizerInterface;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer as SpatieOptimizer;

class ImageOptimizer implements ImageOptimizerInterface
{

    private string $path;

    public function setPath(string $path): ImageOptimizerInterface
    {
        $this->path = $path;

        return $this;
    }

    public function optimize()
    {
        if (isset($this->path)) {
            SpatieOptimizer::optimize($this->path);
        }
    }
}
