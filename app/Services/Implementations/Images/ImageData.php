<?php

namespace App\Services\Implementations\Images;

use App\Services\Interfaces\ImageDataInterface;

class ImageData implements ImageDataInterface
{

    private string $url;

    private int $width;

    private int $height;

    private string $alt;

    private array $text;

    private string $localPath;

    public function setUrl(string $url): ImageDataInterface
    {
        $this->url = $url;

        return $this;
    }

    public function setWidth(int $width): ImageDataInterface
    {
        $this->width = $width;

        return $this;
    }

    public function setHeight(int $height): ImageDataInterface
    {
        $this->height = $height;

        return $this;
    }

    public function setAlt(string $alt): ImageDataInterface
    {
        $this->alt = $alt;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getAlt(): string
    {
        return $this->alt;
    }

    public function setAdditionalText(array $text): ImageDataInterface
    {
        $this->text = $text;

        return $this;
    }

    public function getAdditionalText(): array
    {
        return $this->text;
    }

    public function setLocalPath(string $localPath): ImageDataInterface
    {
        $this->localPath = $localPath;

        return $this;
    }

    public function getLocalPath(): string
    {
        return $this->localPath;
    }
}
