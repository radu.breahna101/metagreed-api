<?php

namespace App\Services\Implementations\DataFetching;

use App\Services\Interfaces\FetchInterface;
use GuzzleHttp\Client;
use Faker\Generator as Faker;
use function Psy\debug;

class GuzzleFetch implements FetchInterface
{

    private string $url;

    private Client $client;

    private Faker $faker;

    /**
     * GuzzleFetch constructor.
     *
     * @param Client $client
     * @param \Faker\Generator $faker
     */
    public function __construct(Client $client, Faker $faker)
    {
        $this->client = $client;
        $this->faker  = $faker;
    }

    public function setUrl(string $url): FetchInterface
    {
        $this->url = $url;

        return $this;
    }

    public function getContent(): string
    {
        //'User-Agent' => 'testing/1.0',

        $response = $this->client->request('GET', $this->url, [
            'headers' => [
                'User-Agent' => $this->faker->userAgent,
            ],
        ]);

        // $response = $this->client->get($this->url);
        return $response->getBody();
    }
}
