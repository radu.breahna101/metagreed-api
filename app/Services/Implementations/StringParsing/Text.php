<?php

namespace App\Services\Implementations\StringParsing;

use App\Services\Implementations\Scraping\Extractor;
use App\Services\Interfaces\TextInterface;
use Illuminate\Support\Arr;

class Text implements TextInterface
{
    private array $content;

    private array $allText;

    public function setContent(array $content)
    {
        $this->content = $content;
    }

    public function getAllText(): array
    {
        $this->allText = array_map(function ($group) {
            return $this->extractParagraphsFromGroup($group);
        }, $this->content);

        $this->allText = Arr::flatten($this->allText);

        return $this->allText;
    }

    private function extractParagraphsFromGroup(array $group): array
    {
        if (! key_exists(Extractor::TAG_PARAGRAPH, $group)) {
            return [];
        }

        return array_values($group[Extractor::TAG_PARAGRAPH]);
    }
}
