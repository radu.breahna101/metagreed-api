<?php

namespace App\Services\Implementations\PostBuilding;

use App\Services\Implementations\Images\ImageDataFactory;
use App\Services\Interfaces\ExtractorInterface;
use App\Services\Interfaces\ImageDataInterface;
use App\Services\Interfaces\SourceInterface;
use App\Services\Interfaces\TextParserInterface;
use Illuminate\Support\Arr;

class Source implements SourceInterface
{
    private array $content;

    private array $imageUrls;

    /**
     * @var ImageDataInterface[]
     */
    private array $imageDataList;

    private ImageDataFactory $imageDataFactory;

    private TextParserInterface $textParser;

    private string $rawText = '';

    public function __construct(ImageDataFactory $imageDataFactory, TextParserInterface $textParser)
    {
        $this->imageDataFactory = $imageDataFactory;
        $this->textParser       = $textParser;
    }

    public function setContent(array $content): SourceInterface
    {
        $this->content = $content;

        return $this;
    }

    public function getImagesList(): array
    {
        $this->extractImagesFromContent();

        return $this->getImageUrls();
    }

    private function extractImagesFromContent()
    {
        array_walk_recursive($this->content, fn($item, $key) => $this->addUrlToList($key, $item));
    }

    private function extractParagraphsFromContent()
    {
        array_walk_recursive($this->content, fn($item, $key) => $this->addTextToList($key, $item));
    }

    private function getImageUrls(): array
    {
        return $this->imageUrls;
    }

    private function addUrlToList($key, $item)
    {
        if ($key === ExtractorInterface::ATTRIBUTE_SRC || $key === ExtractorInterface::ATTRIBUTE_DATA_SRC) {
            $this->imageUrls[] = $item;
        }
    }

    public function getImageDataList(): array
    {
        if (empty($this->imageDataList)) {
            $this->createImageDataList();
        }

        return $this->imageDataList;
    }

    private function createImageDataList()
    {
        foreach ($this->content as $section) {
            $this->parseSection($section);
        }
    }

    private function parseSection(array $section)
    {
        foreach ($section as $tag) {
            $this->parseSingleTag($tag);
        }
    }

    private function parseSingleTag(array $tag)
    {
        if (in_array(ExtractorInterface::TAG_IMAGE, $tag)) {
            $this->buildImageData($tag);
        }
    }

    private function buildImageData(array $tag)
    {
        $imageData = $this->imageDataFactory::makeImageData();
        $imageData->setUrl($tag[ExtractorInterface::ATTRIBUTE_SRC] ?? $tag[ExtractorInterface::ATTRIBUTE_DATA_SRC]);
        $imageData->setHeight($tag[ExtractorInterface::ATTRIBUTE_HEIGHT] ?? 0);
        $imageData->setWidth($tag[ExtractorInterface::ATTRIBUTE_WIDTH] ?? 0);
        $imageData->setAlt($tag[ExtractorInterface::ATTRIBUTE_ALT] ?? '');

        $this->imageDataList[] = $imageData;
    }

    public function getKeyWords(): array
    {
        $this->extractParagraphsFromContent();
        $this->textParser->setString($this->rawText);

        return $this->textParser->getKeyWords();
    }

    private function addTextToList($key, $item)
    {
        if ($key === ExtractorInterface::ATTRIBUTE_TEXT || $key === ExtractorInterface::ATTRIBUTE_ALT) {
            $this->rawText = $this->rawText.$item;
        }
    }

    /**
     * @return string
     */
    public function getRawText(): string
    {
        if (empty($this->rawText)) {
            $this->extractParagraphsFromContent();
        }

        return $this->rawText;
    }
}
