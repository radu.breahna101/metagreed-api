<?php

namespace App\Services\Implementations\PostBuilding;

use App\Services\Interfaces\DomNodeInterface;
use App\Services\Interfaces\DraftInterface;
use App\Services\Interfaces\DraftRepositoryInterface;
use App\Services\Interfaces\ExtractorInterface;
use App\Services\Interfaces\FetchInterface;
use App\Services\Interfaces\ImageSaverInterface;
use App\Services\Interfaces\SourceInterface;

class Draft implements DraftInterface
{

    private FetchInterface $fetch;

    private DomNodeInterface $node;

    private ExtractorInterface $extractor;

    private SourceInterface $source;

    private string $siteUrl;

    private string $latestPostSelector;

    private string $singlePostSelector;

    private array $extractionTags;

    private string $homepageContent;

    private string $postUrl;

    private string $singlePostContent;

    private array $extractedContent;

    private array $imageDataList = [];

    /**
     * @var \App\Services\Interfaces\DraftRepositoryInterface
     */
    private DraftRepositoryInterface $draftRepository;

    /**
     * Draft constructor.
     *
     * @param \App\Services\Interfaces\FetchInterface $fetch
     * @param \App\Services\Interfaces\DomNodeInterface $node
     * @param \App\Services\Interfaces\ExtractorInterface $extractor
     * @param \App\Services\Interfaces\SourceInterface $source
     * @param \App\Services\Interfaces\DraftRepositoryInterface $draftRepository
     */
    public function __construct(
        FetchInterface $fetch,
        DomNodeInterface $node,
        ExtractorInterface $extractor,
        SourceInterface $source,
        DraftRepositoryInterface $draftRepository
    ) {
        $this->fetch           = $fetch;
        $this->node            = $node;
        $this->extractor       = $extractor;
        $this->source          = $source;
        $this->draftRepository = $draftRepository;
    }

    public function setSiteUrl(string $siteUrl): DraftInterface
    {
        $this->siteUrl = $siteUrl;

        return $this;
    }

    public function setLatestPostSelector(string $latestPostSelector): DraftInterface
    {
        $this->latestPostSelector = $latestPostSelector;

        return $this;
    }

    public function setSinglePostSelector(string $singlePostSelector): DraftInterface
    {
        $this->singlePostSelector = $singlePostSelector;

        return $this;
    }

    public function setExtractionTags(array $extractionTags): DraftInterface
    {
        $this->extractionTags = $extractionTags;

        return $this;
    }

    public function store()
    {
        $this->fetchPostUrlIfNotProvided();

        $this->fetchSinglePostContent();
        $this->setExtractedContent();
        $this->initializeSource();
        $this->saveImagesToStorage();
        $this->buildDraft();
    }

    private function fetchHomePageContent()
    {
        $this->homepageContent = $this->fetch->
        setUrl($this->siteUrl)->
        getContent();
    }

    private function extractLatestPostUrl()
    {
        $this->postUrl = $this->node->
        setContent($this->homepageContent)->
        setSelector($this->latestPostSelector)->
        getAttribute('href');
    }

    private function fetchSinglePostContent()
    {
        $this->singlePostContent = $this->fetch->
        setUrl($this->postUrl)->
        getContent();
    }

    private function setExtractedContent()
    {
        $this->extractedContent = $this->extractor->
        setContent($this->singlePostContent)->
        setContainerSelector($this->singlePostSelector)->
        setExtractionTags($this->extractionTags)->extract();
    }

    private function initializeSource()
    {
        $this->source->setContent($this->extractedContent);
    }

    private function saveImagesToStorage()
    {

        foreach ($this->source->getImageDataList() as $imageData) {
            /**
             * @var ImageSaverInterface $imageSaver
             * @var \App\Services\Interfaces\ImageDataInterface $imageData
             */
            $imageSaver = resolve(ImageSaverInterface::class); //used as a factory
            $imageSaver->setImageData($imageData)->save();
            $imageData->setLocalPath($imageSaver->getFilePath());
            $this->imageDataList[] = $imageData;
        }
    }

    private function buildDraft()
    {
        $this->draftRepository->
        setRawText($this->source->getRawText())->
        setKeyWords($this->source->getKeyWords())->
        setImageDataList($this->imageDataList)->
        setSourceUrl($this->postUrl)->
        build();
    }


    public function setPostUrl(string $postUrl): DraftInterface
    {
        $this->postUrl = $postUrl;

        return $this;
    }

    private function fetchPostUrlIfNotProvided(): void
    {
        if (! isset($this->postUrl)) {
            $this->fetchHomePageContent();
            $this->extractLatestPostUrl();
        }
    }
}
