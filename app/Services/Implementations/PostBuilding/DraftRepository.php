<?php

namespace App\Services\Implementations\PostBuilding;

use App\Post;
use App\Services\Interfaces\DraftRepositoryInterface;
use App\Services\Interfaces\ImageDataInterface;
use App\Tag;

class DraftRepository implements DraftRepositoryInterface
{

    private Post $post;

    private array $keywords;

    /**
     * @var ImageDataInterface[]
     */
    private array $imageDataList;

    private string $url;

    private string $rawText = '';

    public function setKeyWords(array $keywords): DraftRepositoryInterface
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function setImageDataList(array $imageData): DraftRepositoryInterface
    {
        $this->imageDataList = $imageData;

        return $this;
    }

    public function setSourceUrl(string $url): DraftRepositoryInterface
    {
        $this->url = $url;

        return $this;
    }

    public function build()
    {
        $this->createNewPost();
        $this->createAndAttachTagsToPost();
        $this->createAndAttachImagesToPost();
    }

    private function createNewPost()
    {
        $this->post = Post::create([
            'raw_text'   => $this->rawText,
            'source_url' => $this->url,
            'is_draft'   => true,
        ]);
    }

    private function createAndAttachTagsToPost()
    {
        //$user->roles()->attach($roleId, ['expires' => $expires]);
        foreach ($this->keywords as $order => $tagText) {
            $this->post->tags()->attach(Tag::firstOrCreate(['text' => $tagText]), ['order' => $order]);
        }
    }

    public function setRawText(string $text): DraftRepositoryInterface
    {
        $this->rawText = $text;

        return $this;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

//TODO add more details to images, look into user agent
    private function createAndAttachImagesToPost()
    {
        $filledImageData = [];
        foreach ($this->imageDataList as $order => $imageData) {
            $filledImageData[] = [
                'path'   => $imageData->getLocalPath(),
                'url'    => $imageData->getUrl(),
                'alt'    => $imageData->getAlt(),
                'height' => $imageData->getHeight(),
                'width'  => $imageData->getWidth(),
            ];
        }

        $this->post->images()->createMany($filledImageData);
    }
}
