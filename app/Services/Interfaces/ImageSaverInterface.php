<?php

namespace App\Services\Interfaces;

interface ImageSaverInterface
{
    public function setImageData(ImageDataInterface $imageData): ImageSaverInterface;
    public function save();
    public function getFilePath(): string;
}
