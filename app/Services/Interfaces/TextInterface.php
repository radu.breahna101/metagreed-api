<?php

namespace App\Services\Interfaces;

interface TextInterface
{
    public function setContent(array $source);

    public function getAllText(): array;
}
