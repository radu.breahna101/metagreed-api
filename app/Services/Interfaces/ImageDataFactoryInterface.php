<?php

namespace App\Services\Interfaces;

interface ImageDataFactoryInterface
{
    public static function makeImageData(): ImageDataInterface;
}
