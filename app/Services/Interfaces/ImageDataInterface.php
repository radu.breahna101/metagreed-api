<?php

namespace App\Services\Interfaces;

Interface ImageDataInterface
{
    public function setUrl(string $url): ImageDataInterface;

    public function setLocalPath(string $localPath): ImageDataInterface;

    public function setWidth(int $width): ImageDataInterface;

    public function setHeight(int $height): ImageDataInterface;

    public function setAlt(string $alt): ImageDataInterface;

    public function setAdditionalText(array $text): ImageDataInterface;

    public function getAdditionalText(): array;

    public function getLocalPath(): string;

    public function getUrl(): string;

    public function getWidth(): int;

    public function getHeight(): int;

    public function getAlt(): string;
}
