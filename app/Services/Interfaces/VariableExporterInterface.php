<?php

namespace App\Services\Interfaces;

interface VariableExporterInterface
{
    public function export();

    public function exportToFile(): void;

    public function setArray(array $array): VariableExporterInterface;

    public function setFilename(string $filename): VariableExporterInterface;

    public function getExportedString(): string;
}
