<?php

namespace App\Services\Interfaces;

interface ImageOptimizerInterface
{
    public function setPath(string $path) : ImageOptimizerInterface;

    public function optimize();
}
