<?php

namespace App\Services\Interfaces;

interface TextParserInterface
{
    public function setString(string $string): TextParserInterface;

    public function addStopWords(array $stopWords): TextParserInterface;

    public function getStopWords(): array;

    public function getKeyWords(): array;

    public function getRankedKeyWords(): array;
}
