<?php

namespace App\Services\Interfaces;

interface DomNodeInterface
{
    public function setContent(string $content): DomNodeInterface;

    public function setSelector(string $selector): DomNodeInterface;

    public function getAttribute(string $attribute): string;
}
