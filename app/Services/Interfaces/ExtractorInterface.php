<?php

namespace App\Services\Interfaces;

interface ExtractorInterface
{
    const SECTION_PREFIX     = 'section';

    const TAG_IDENTIFIER     = 'tag';

    const TAG_IMAGE          = 'img';

    const TAG_PARAGRAPH      = 'p';

    const TAG_TEXT           = 'text';

    const TAGS               = [
        self::TAG_IMAGE     => [
            self::ATTRIBUTE_SRC,
            self::ATTRIBUTE_DATA_SRC,
            self::ATTRIBUTE_ALT,
            self::ATTRIBUTE_HEIGHT,
            self::ATTRIBUTE_WIDTH,
        ],
        self::TAG_PARAGRAPH => [
            self::ATTRIBUTE_TEXT,
        ],
        self::TAG_TEXT      => [
            self::ATTRIBUTE_TEXT,
        ],
    ];

    const ATTRIBUTE_SRC      = 'src';

    const ATTRIBUTE_DATA_SRC = 'data-src';

    const ATTRIBUTE_ALT      = 'alt';

    const ATTRIBUTE_WIDTH    = 'width';

    const ATTRIBUTE_HEIGHT   = 'height';

    const ATTRIBUTE_TEXT     = 'text';

    public function setContent(string $content): ExtractorInterface;

    public function setContainerSelector(string $selector): ExtractorInterface;

    public function setExtractionTags(array $tags): ExtractorInterface;

    public function extract(): array;
}
