<?php

namespace App\Services\Interfaces;

interface DraftInterface
{
    public function setSiteUrl(string $siteUrl): DraftInterface;

    public function setPostUrl(string $postUrl): DraftInterface;

    public function setLatestPostSelector(string $latestPostSelector): DraftInterface;

    public function setSinglePostSelector(string $singlePostSelector): DraftInterface;

    public function setExtractionTags(array $extractionTags): DraftInterface;

    public function store();
}
