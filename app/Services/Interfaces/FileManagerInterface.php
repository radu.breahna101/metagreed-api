<?php

namespace App\Services\Interfaces;

interface FileManagerInterface
{
    const DISK_IMAGES = 'images';

    public function setStringContent(string $content): FileManagerInterface;

    public function setFolderName(string $folderName): FileManagerInterface;

    public function setFileName(string $fileName): FileManagerInterface;

    public function setDisk(string $disk): FileManagerInterface;

    public function save();

    public function isSaved(): bool;

    public function getPathToSavedFile(): string;
}
