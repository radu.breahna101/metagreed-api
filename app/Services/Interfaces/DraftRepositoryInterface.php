<?php

namespace App\Services\Interfaces;

use App\Post;

interface DraftRepositoryInterface
{
    public function setKeyWords(array $keywords): DraftRepositoryInterface;

    public function setRawText(string $text): DraftRepositoryInterface;

    /**
     * @return \App\Services\Interfaces\DraftRepositoryInterface
     * @var ImageDataInterface[] $imageData
     */
    public function setImageDataList(array $imageData): DraftRepositoryInterface;

    public function setSourceUrl(string $url): DraftRepositoryInterface;

    public function getPost(): Post;

    public function build();
}
