<?php

namespace App\Services\Interfaces;

interface SourceInterface
{
    public function setContent(array $content): SourceInterface;

    public function getImagesList(): array;

    public function getImageDataList(): array;

    public function getKeyWords(): array;

    public function getRawText(): string;

}
