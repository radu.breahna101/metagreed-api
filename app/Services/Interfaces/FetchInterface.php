<?php


namespace App\Services\Interfaces;


interface FetchInterface
{
    public function setUrl(string $url): FetchInterface;
    public function getContent() : string;
}
