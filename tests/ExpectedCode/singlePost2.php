<?php
$expected = [
    'section_0'  =>
        [
            [
                'text' => 'Unusual animal friendships are perfect proof that love knows no boundaries. And these two know it. Nova the German Shepherd and Pacco the ferret have been crazy about each other since day one. Their owner, Diana Grib, said they’re living together as if they don’t know they’re different.',
                'tag'  => 'p',
            ],
        ],
    'section_1'  =>
        [
            [
                'text' => 'Diana said she first got Nova – Karamba Laukinė Vilkauogė. From the moment she got her, the woman introduced her to all kinds of animals, including rats, cats, and parrots. Diana spent quite some time teaching her pup to get along with everyone. When Nova became an adult, Diana set out to get the other pet she was dreaming about. Contacting many ferret breeders, her main priority was to get one that knows how to be with dogs. Ultimately, her search took her to Krakow where she got Pacco.',
                'tag'  => 'p',
            ],
        ],
    'section_2'  =>
        [
            [
                'text' => 'More info: Instagram',
                'tag'  => 'p',
            ],
        ],
    'section_3'  =>
        [
            [
                'src'    => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-2-5dc29c93eb934__700.jpg',
                'width'  => '700',
                'height' => '647',
                'tag'    => 'img',
            ],
        ],
    'section_4'  =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_5'  =>
        [
            [
                'src'    => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29fe81cdf7__700.jpg',
                'width'  => '700',
                'height' => '327',
                'tag'    => 'img',
            ],
        ],
    'section_6'  =>
        [
            [
                'src'    => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29fe99ac05__700.jpg',
                'width'  => '700',
                'height' => '337',
                'tag'    => 'img',
            ],
        ],
    'section_7'  =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_8'  =>
        [
            [
                'text' => 'Nova and Pacco have some similarities, too. They’re both curious, playful and brave. Their matching personalities and the fact that they both met other animals when they were growing up are probably the main reasons why they get along so nicely. When they first met, Pacco was taking a tour of the new home while Nova was following her new friend like a shadow. After a few days, Pacco started paying attention to her and the fun started.',
                'tag'  => 'p',
            ],
        ],
    'section_9'  =>
        [
            [
                'src'    => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5-5dc29c998e952__700.jpg',
                'width'  => '700',
                'height' => '700',
                'tag'    => 'img',
            ],
        ],
    'section_10' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_11' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-25-5dc29cbe0f0b2__700.jpg',
                'width'    => '700',
                'height'   => '385',
                'tag'      => 'img',
            ],
        ],
    'section_12' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_13' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29feb38c8f__700.jpg',
                'width'    => '700',
                'height'   => '438',
                'tag'      => 'img',
            ],
        ],
    'section_14' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_15' =>
        [
            [
                'text' => 'Now, the two are constantly playing, traveling, even sleeping together. “It sometimes feels that I own two dogs,” Diana told Bored Panda. The ruler of the house, however, is the ferret. “Considering that ferrets are cheeky and stubborn, it’s understandable. Nova is really happy to just have someone by her side.',
                'tag'  => 'p',
            ],
        ],
    'section_16' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-18-5dc29cb11b356__700.jpg',
                'width'    => '700',
                'height'   => '486',
                'tag'      => 'img',
            ],
        ],
    'section_17' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_18' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-6-5dc29c9b88768__700.jpg',
                'width'    => '700',
                'height'   => '592',
                'tag'      => 'img',
            ],
        ],
    'section_19' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_20' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-7-5dc29c9d517a1__700.jpg',
                'width'    => '700',
                'height'   => '698',
                'tag'      => 'img',
            ],
        ],
    'section_21' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_22' =>
        [
            [
                'text' => 'Diana said one has to read a lot if they want to own a ferret. “From creating a safe environment in the house to developing a raw menu, health issues and dealing with poop, you have to know a lot about them,” she explained. “Before I got Pacco, I spent at least half a year gathering information. Still, I’m learning about him every day.”',
                'tag'  => 'p',
            ],
        ],
    'section_23' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc2b93dd81ed__700.jpg',
                'width'    => '700',
                'height'   => '700',
                'tag'      => 'img',
            ],
        ],
    'section_24' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_25' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-1-5dc29c921ddd4__700.jpg',
                'width'    => '700',
                'height'   => '437',
                'tag'      => 'img',
            ],
        ],
    'section_26' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_27' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-9-5dc29ca10e77c__700.jpg',
                'width'    => '700',
                'height'   => '533',
                'tag'      => 'img',
            ],
        ],
    'section_28' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_29' =>
        [
            [
                'text' => 'Diana also warned that ferrets aren’t for everyone. “For starters, they bite. Some do it more, some less, but they definitely let you know if they don’t like something. They also sense fear, so if you get afraid of them, they’ll use it to their advantage.” So, if you have little kids running around or aren’t willing to put up with their neediness, maybe you shouldn’t get one.',
                'tag'  => 'p',
            ],
        ],
    'section_30' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29fee50677__700.jpg',
                'width'    => '700',
                'height'   => '355',
                'tag'      => 'img',
            ],
        ],
    'section_31' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29fefbf201__700.jpg',
                'width'    => '700',
                'height'   => '331',
                'tag'      => 'img',
            ],
        ],
    'section_32' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_33' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-4-5dc29c979f1e3__700.jpg',
                'width'    => '700',
                'height'   => '525',
                'tag'      => 'img',
            ],
        ],
    'section_34' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_35' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-11-5dc29ca4701da__700.jpg',
                'width'    => '700',
                'height'   => '490',
                'tag'      => 'img',
            ],
        ],
    'section_36' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_37' =>
        [
            [
                'text' => 'Another thing to consider is their health. “You really need to pay close attention to the conditions you create for them and the food you give them. If you mess something up, it can quickly snowball into huge veterinary bills.”',
                'tag'  => 'p',
            ],
        ],
    'section_38' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-12-5dc29ca61eee8__700.jpg',
                'width'    => '700',
                'height'   => '450',
                'tag'      => 'img',
            ],
        ],
    'section_39' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_40' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-13-5dc29ca7e5fdb__700.jpg',
                'width'    => '700',
                'height'   => '875',
                'tag'      => 'img',
            ],
        ],
    'section_41' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_42' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29ff175867__700.jpg',
                'width'    => '700',
                'height'   => '509',
                'tag'      => 'img',
            ],
        ],
    'section_43' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_44' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29ff2eef29__700.jpg',
                'width'    => '700',
                'height'   => '353',
                'tag'      => 'img',
            ],
        ],
    'section_45' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_46' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-16-5dc29cad75d94__700.jpg',
                'width'    => '700',
                'height'   => '525',
                'tag'      => 'img',
            ],
        ],
    'section_47' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_48' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc2a0e4a08ac__700.jpg',
                'width'    => '700',
                'height'   => '447',
                'tag'      => 'img',
            ],
        ],
    'section_49' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc2a0e69840d__700.jpg',
                'width'    => '700',
                'height'   => '421',
                'tag'      => 'img',
            ],
        ],
    'section_50' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_51' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc2a0e85ecc5__700.jpg',
                'width'    => '700',
                'height'   => '426',
                'tag'      => 'img',
            ],
        ],
    'section_52' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_53' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc2a0ea2c298__700.jpg',
                'width'    => '700',
                'height'   => '437',
                'tag'      => 'img',
            ],
        ],
    'section_54' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_55' =>
        [
            [
                'text' => 'Nova, on the other hand, is a really friendly dog. She’s always ready to show her love to the entire world, whether it’s someone she knows or a random passer-by. “It also helps that she’s very patient. She doesn’t get mad at Pacco even if he hurts her a little bit while playing.”',
                'tag'  => 'p',
            ],
        ],
    'section_56' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-20-5dc29cb52e8fb__700.jpg',
                'width'    => '700',
                'height'   => '417',
                'tag'      => 'img',
            ],
        ],
    'section_57' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_58' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-21-5dc29cb6bb2b7__700.jpg',
                'width'    => '700',
                'height'   => '469',
                'tag'      => 'img',
            ],
        ],
    'section_59' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_60' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-22-5dc29cb8b05d9__700.jpg',
                'width'    => '700',
                'height'   => '436',
                'tag'      => 'img',
            ],
        ],
    'section_61' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_62' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-23-5dc29cba8d29e__700.jpg',
                'width'    => '700',
                'height'   => '544',
                'tag'      => 'img',
            ],
        ],
    'section_63' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_64' =>
        [
            [
                'data-src' => 'https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-24-5dc29cbc4a53e__700.jpg',
                'width'    => '700',
                'height'   => '525',
                'tag'      => 'img',
            ],
        ],
    'section_65' =>
        [
            [
                'text' => 'Image credits: nova_n_pacco',
                'tag'  => 'p',
            ],
        ],
    'section_66' =>
        [
            [
                'text' => 'Like what you\'re reading? Subscribe to our top stories.',
                'tag'  => 'p',
            ],
        ],
    'section_67' =>
        [
            [
                'text' => 'Bored Panda works best if you switch to our Android app',
                'tag'  => 'p',
            ],
        ],
    'section_68' =>
        [
            [
                'text' => 'Bored Panda works better on our iPhone app!',
                'tag'  => 'p',
            ],
        ],
    'section_69' =>
        [
            [
                'text' => 'Follow Bored Panda on Google News!',
                'tag'  => 'p',
            ],
        ],
    'section_70' =>
        [
            [
                'text' => 'Follow us on Flipboard.com/@boredpanda!',
                'tag'  => 'p',
            ],
        ],
];
