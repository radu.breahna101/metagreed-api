<?php
$expected = [
    'section_0'  =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/Astrum.jpg',
                'alt'    => 'bdbfdbf5644ye  tbr54',
                'width'  => '1200',
                'height' => '1504',
                'tag'    => 'img',
            ],
            [
                'text' => '“Astrum”, 11 x 14 inches. All images courtesy of the artist',
                'tag'  => 'p',
            ],
        ],
    'section_1'  =>
        [
            [
                'text' => 'At the Direktorenhaus Museum in Berlin this past week, a solo exhibition of detailed architectural drawings by Virginia-based artist Benjamin Sack (previously) opened to the public. Titled Labyrinths, the collection of new works features vast cityscapes comprised of impossible inner-geometries. The maze-like urban maps reference musical compositions and various symbols found in cosmology.',
                'tag'  => 'p',
            ],
        ],
    'section_2'  =>
        [
            [
                'text' => 'Often creating based on what he calls a “fear of blank spaces,” Sack tells Colossal that his starting point for each drawing is different. Finding inspiration in history, cartography, and his own travels, the artist starts with a general concept and builds his intricate worlds intuitively as he goes. Star-shaped buildings and pathways meet with rows of houses that spiral out from clusters of skyscrapers. The pieces in Labyrinths range from 11 inches by 14 inches (a standard photo print size) up to 90 inches wide and 69 inches tall. A work titled Library of Babel is drawn on the surface of a globe measuring 16 inches in diameter. “Generally, a large piece is begun with a few very broad and simple demarkations in pencil,” Sack explains. The rest of the lines and spaces are filled in with pen.',
                'tag'  => 'p',
            ],
        ],
    'section_3'  =>
        [
            [
                'text' => '“Over many years my interest in architecture and cityscapes has evolved,” Sack tells Colossal. He adds that drawing such intricate pieces has “become a way and means of expressing the infinite, playing with perspective and exploring a range of histories, cultures, places.”',
                'tag'  => 'p',
            ],
        ],
    'section_4'  =>
        [
            [
                'text' => 'Labyrinths will be exhibited through January 22, 2020. For more of Sack’s imaginative maps, follow the artist on Instagram.',
                'tag'  => 'p',
            ],
        ],
    'section_5'  =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel5-e1572636437178.jpg',
                'width'  => '2000',
                'height' => '2100',
                'tag'    => 'img',
            ],
            [
                'text' => '“Library of Babel” (globe piece), 16 inches in diameter',
                'tag'  => 'p',
            ],
        ],
    'section_6'  =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel7-e1572636476140.jpg',
                'alt'    => 'ststhh strhrth srths',
                'width'  => '2000',
                'height' => '2100',
                'tag'    => 'img',
            ],
            [
                'text' => '“Library of Babel” detail',
                'tag'  => 'p',
            ],
        ],
    'section_7'  =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel4-e1572636512696.jpg',
                'width'  => '2000',
                'height' => '2446',
                'tag'    => 'img',
            ],
            [
                'text' => '“Library of Babel” detail',
                'tag'  => 'p',
            ],
        ],
    'section_8'  =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel3-e1572636542319.jpg',
                'width'  => '2000',
                'height' => '2540',
                'tag'    => 'img',
            ],
            [
                'text' => '“Library of Babel” detail',
                'tag'  => 'p',
            ],
        ],
    'section_9'  =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel6-e1572636576429.jpg',
                'width'  => '2000',
                'height' => '2100',
                'tag'    => 'img',
            ],
            [
                'text' => '“Library of Babel” detail',
                'tag'  => 'p',
            ],
        ],
    'section_10' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/CANTO-IV-Large.jpeg',
                'width'  => '2000',
                'height' => '2049',
                'tag'    => 'img',
            ],
            [
                'text' => '“Canto IV” 70 x 70 inches',
                'tag'  => 'p',
            ],
        ],
    'section_11' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/EDEN_large_1.jpg',
                'width'  => '2000',
                'height' => '1614',
                'tag'    => 'img',
            ],
            [
                'text' => '“Eden” 14 x 11 inches',
                'tag'  => 'p',
            ],
        ],
    'section_12' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/Peregrinations_68x92.75.jpg',
                'width'  => '2000',
                'height' => '1468',
                'tag'    => 'img',
            ],
            [
                'text' => '“Peregrinations” 68 x 93 inches',
                'tag'  => 'p',
            ],
        ],
    'section_13' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/SAMSARA_large_final-copy.jpg',
                'width'  => '1500',
                'height' => '2212',
                'tag'    => 'img',
            ],
            [
                'text' => '“Samsara” 12 x 18 inches',
                'tag'  => 'p',
            ],
        ],
    'section_14' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/11/Stella-Corona-copy.jpg',
                'width'  => '1200',
                'height' => '954',
                'tag'    => 'img',
            ],
            [
                'text' => '“Stella Aurora” 11 x 14 inches',
                'tag'  => 'p',
            ],
        ],
];
