<?php
$expected = [
    'section_0' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/felipe_2up.jpg',
                'width'  => '3856',
                'height' => '1819',
                'tag'    => 'img',
            ],
            [
                'text' => 'Felipe IV a Caballo (1635-36), Diego Velázquez. Images courtesy of Museo del Prado / WWF',
                'tag'  => 'p',
            ],
        ],
    'section_1' =>
        [
            [
                'text' => 'Museo del Prado (Prado Museum) recently collaborated on a project with the World Wildlife Fund (WWF) designed to coincide with the 2019 UN Climate Change Conference in Madrid. Paintings from the museum’s collection were digitally modified to reflect a future world destroyed by inaction. Rising sea levels, barren rivers, and refugee camps transform works by European painters into a campaign to save the environment.',
                'tag'  => 'p',
            ],
        ],
    'section_2' =>
        [
            [
                'text' => 'The project is titled “+ 1,5ºC Lo Cambia Todo,” which translates from Spanish to mean “+ 1.5ºC Changes Everything.” Paintings by three Spanish artists (Francisco de Goya, Diego Velázquez, and Joaquín Sorolla) and one Flemish Renaissance painter (Joachim Patinir) were chosen for the project by WWF and museum experts. The altered works were installed on billboards in Madrid and shared online using the hashtag #LoCambiaTodo as a way to expand and continue political and social conversations through art.',
                'tag'  => 'p',
            ],
        ],
    'section_3' =>
        [
            [
                'text' => '“For the Museum, this project represents an opportunity to continue placing art and its values at the service of society,” Javier Solana, Prado’s Royal Board of Trustees President, said in a statement. “The symbolic value of the masterpieces and the impressive artistic recreation that we present with WWF is an excellent way to transmit to everyone and especially to the young generations what is really at stake in this fight against climate change.” [via Artnet]',
                'tag'  => 'p',
            ],
        ],
    'section_4' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/Patinir_p01616.jpeg',
                'width'  => '2000',
                'height' => '1238',
                'tag'    => 'img',
            ],
            [
                'text' => 'Landscape with Charon Crossing the Styx (c. 1515-1524), Joachim Patinir',
                'tag'  => 'p',
            ],
        ],
    'section_5' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/Dry_Styx_River_WWF.jpeg',
                'width'  => '1920',
                'height' => '1188',
                'tag'    => 'img',
            ],
        ],
    'section_6' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/Sorolla_P04648.jpeg',
                'width'  => '2000',
                'height' => '1231',
                'tag'    => 'img',
            ],
            [
                'text' => 'Boys on the Beach (1909), Joaquín Sorolla',
                'tag'  => 'p',
            ],
        ],
    'section_7' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/Children_on_Dead-Sea_WWF.jpeg',
                'width'  => '1920',
                'height' => '1173',
                'tag'    => 'img',
            ],
        ],
    'section_8' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/Goya_P00773.jpeg',
                'width'  => '2000',
                'height' => '1371',
                'tag'    => 'img',
            ],
            [
                'text' => 'The Parasol (1777), Francisco de Goya',
                'tag'  => 'p',
            ],
        ],
    'section_9' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/12/The_Parasol_Refugees_WWF.jpeg',
                'width'  => '1920',
                'height' => '1316',
                'tag'    => 'img',
            ],
        ],
];
