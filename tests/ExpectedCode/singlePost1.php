<?php
$expected = [
    'section_0' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-2.jpg',
                'alt'    => 'test alt text',
                'width'  => '2000',
                'height' => '2000',
                'tag'    => 'img',
            ],
        ],
    'section_1' =>
        [
            [
                'text' => 'Chicago-based fiber artist and activist Shannon Downey has a particular affinity for unfinished projects. She seeks them out at estate sales, helping women who’ve passed complete their work. Although this has long been an area of interest for Downey, one recent discovery has catapulted to the front page of news sites around the world.',
                'tag'  => 'p',
            ],
        ],
    'section_2' =>
        [
            [
                'text' => 'On a visit this September to a Chicagoland estate sale sale, Downey happened upon a framed embroidery (pictured above) that maps out the United States and illustrates all fifty state flowers. A casual conversation with the cashier at the sale led to Downey finding a large-scale project related to the framed work. An unfinished queen-sized quilt was meant to incorporate another U.S. map along with hexagons featuring each state’s bird and flower along with the year they entered the union.',
                'tag'  => 'p',
            ],
        ],
    'section_3' =>
        [
            [
                'text' => 'Rita Smith of Mount Prospect, Illinois, had begun the project a few decades ago, according to her son, whom Downey subsequently connected with. Smith, who was a nurse, passed away recently at the age of 99. “I have an annoying habit of having to purchase and finish unfinished projects if I think that the person has passed on … but usually I’m just buying a half-done pillow that needs half an hour’s worth of stitching and then it’s done,” Downey told Block Club Chicago. “But this one was massive and it just felt really significant for some reason. And so I bought it.”',
                'tag'  => 'p',
            ],
        ],
    'section_4' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-1.jpg',
                'alt'    => 'ssjbmjdfbbo test 2',
                'width'  => '2000',
                'height' => '2000',
                'tag'    => 'img',
            ],
        ],
    'section_5' =>
        [
            [
                'text' => 'Downey has a substantial following thanks to her work as a community organizer and resource for people looking for an alternative to digital distractions. The self-described ‘craftivist’ tells Colossal that after running a digital marketing company for a decade, she was burned out and needed a break from technology.',
                'tag'  => 'p',
            ],
        ],
    'section_6' =>
        [
            [
                'text' => 'I started stitching to find some digital/analog balance. I was hooked. I am an activist and I quickly fused the two. At first, it was about creating space for myself to think substantively and reflect on various issues and topics. As I started to share my work and thoughts, I found a community of folks on Instagram who were engaged and engaging. I have found ways to move those communities offline and into real life communities through my stitch-ups. Those communities are what inspire me to keep going, level up, find new ways of building and connecting, having hard conversations and tackling challenging topics.',
                'tag'  => 'p',
            ],
        ],
    'section_7' =>
        [
            [
                'text' => 'Once Downey shared her unexpected find on Twitter, inquiries from potential collaborators began flooding in. Now she is coordinating between dozens of stitchers across the country who are volunteering their time to complete Rita’s masterpiece. Downey describes the effort as a strongly feminist project. “It is an opportunity for folks to consider how we define and assign value and meaning to craft,” she tells Colossal. “So many of the stories that people are sharing with me on twitter after reading about #RitasQuilt are about memories and connections that they have to the women in their lives who are/were makers and the significance that their art has come to have for them.”',
                'tag'  => 'p',
            ],
        ],
    'section_8' =>
        [
            [
                'text' => 'The National Quilt Museum in Paducah, Kentucky will be displaying the completed quilt, and Downey hopes that it will be able to come with her for a planned 2020 craftivist tour around the U.S. Keep up with Downey on Twitter and Instagram to see how you can get involved in craftivism in your community, and follow along with the #RitasQuilt hashtag.',
                'tag'  => 'p',
            ],
        ],
    'section_9' =>
        [
            [
                'src'    => 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/rita-composite-2.jpg',
                'alt'    => 'hello, another thest',
                'width'  => '2000',
                'height' => '3007',
                'tag'    => 'img',
            ],
        ],
];
