<?php
$expected = [
    'section_0' =>
        [
            [
                'src'    => 'https://www.booooooom.com/wp-content/uploads/2019/10/greencurtains.jpg',
                'width'  => '1200',
                'height' => '1800',
                'tag'    => 'img',
            ],
            [
                'text' => '07.11.19
                                —
                                Anna Schneider',
                'tag'  => 'p',
            ],
        ],
    'section_1' =>
        [
            [
                'text' => 'A selection of images showcasing the quirky style of New York-based photographer Dan Allegretto (previously featured here). More of Allegretto’s work will be on display in an upcoming solo show at Nepenthes in New York, opening November 14th. See more below!',
                'tag'  => 'p',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/IMG_7626-copy.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/IMG_9420-copy.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/IMG_3309-copy.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/stormcloud.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/showercurtain.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/greentomato-1.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/neonlightcorner-1.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/ddtowel-1.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/promlegs_edit-1.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/greencurtains-1.jpg',
                'tag' => 'img',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/R0001413.jpg',
                'tag' => 'img',
            ],
            [
                'text' => 'Dan Allegretto’s Website',
                'tag'  => 'p',
            ],
            [
                'text' => 'Dan Allegretto on Instagram',
                'tag'  => 'p',
            ],
            [
                'src' => 'https://www.booooooom.com/wp-content/uploads/2019/10/prints-postwidget.jpg',
                'tag' => 'img',
            ],
            [
                'text' => 'We’re really excited to release 5 print editions in our shop today! Each is available until November 12th and after that we won’t sell them again.',
                'tag'  => 'p',
            ],
            [
                'text' => 'Related Articles',
                'tag'  => 'p',
            ],
        ],
];
