<?php

namespace Tests\Unit;

use App\Services\Interfaces\SourceInterface;
use App\Services\Interfaces\VariableExporterInterface;
use App\Services\Utilities\Helpers;
use App\Services\Utilities\VariableExporter;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SourceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    private SourceInterface $source;

    private VariableExporterInterface $variableExport;

    public function setUp(): void
    {
        parent::setUp();
        $this->source         = $this->app->make(SourceInterface::class);
        $this->variableExport = $this->app->make(VariableExporterInterface::class);
    }

    private function exportExpectedResult(array $result)
    {
        $this->variableExport->setArray($result)
            ->setFilename(storage_path('/output.txt'))
            ->exportToFile();
    }

    public function testGetImagesList()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost1.php");
        $this->source->setContent($content);
        $imagesList = $this->source->getImagesList();

        // $this->exportExpectedResult($imagesList);

        $expected = [
            'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-2.jpg',
            'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-1.jpg',
            'https://www.thisiscolossal.com/wp-content/uploads/2019/10/rita-composite-2.jpg',
        ];

        $this->assertEquals($expected, $imagesList);
    }

    public function testGetImageDataCollectionPost1()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost1.php");
        $this->source->setContent($content);
        $imageDataList = $this->source->getImageDataList();
        $this->assertCount(3, $imageDataList);
    }

    public function testGetImageDataCollectionPost12()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost12.php");
        $this->source->setContent($content);
        $imageDataList = $this->source->getImageDataList();
        $this->assertCount(11, $imageDataList);
    }

    public function testGetImageDataCollectionPost2()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost2.php");
        $this->source->setContent($content);
        $imageDataList = $this->source->getImageDataList();
        $this->assertCount(30, $imageDataList);
    }

    public function testGetImageDataCollectionPost3()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost3.php");
        $this->source->setContent($content);
        $imageDataList = $this->source->getImageDataList();
        $this->assertCount(21, $imageDataList);
    }

    public function testGetImageDataCollectionPost4()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost4.php");
        $this->source->setContent($content);
        $imageDataList = $this->source->getImageDataList();
        //dd($imageDataList);
        $this->assertCount(13, $imageDataList);
    }

    public function testGetImageDataCollectionPost5()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost5.php");
        $this->source->setContent($content);
        $imageDataList = $this->source->getImageDataList();
        //dd($imageDataList);
        $this->assertCount(7, $imageDataList);
    }

    public function testGetListOfEntirePostKeywordsPost1()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost1.php");
        $this->source->setContent($content);
        $keywords = $this->source->getKeyWords();

        $this->assertCount(207, $keywords);
    }

    public function testGetListOfEntirePostKeywordsPost2()
    {
        $content = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost2.php");
        $this->source->setContent($content);
        $keywords = $this->source->getKeyWords();
        $this->assertCount(168, $keywords);
    }
}
