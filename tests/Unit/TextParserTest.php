<?php

namespace Tests\Unit;

use App\Services\Interfaces\TextParserInterface;
use App\Services\Utilities\Helpers;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Tests\TestCase;

class TextParserTest extends TestCase
{
    private TextParserInterface $text;

    private string $sampleText1;

    private string $sampleText2;

    private string $sampleText3;

    private string $sampleText4;

    private string $sampleText5;

    public function setUp(): void
    {
        parent::setUp();
        $this->text        = $this->app->make(TextParserInterface::class);
        $this->sampleText1 = file_get_contents(base_path()."/tests/SampleTexts/text1.txt");
        //$this->sampleText2 = file_get_contents(base_path()."/tests/SampleTexts/text2.txt");
        //$this->sampleText3 = file_get_contents(base_path()."/tests/SampleTexts/text3.txt");
        //$this->sampleText4 = file_get_contents(base_path()."/tests/SampleTexts/text4.txt");
        //$this->sampleText5 = file_get_contents(base_path()."/tests/SampleTexts/text5.txt");
    }

    public function testAddStopWords()
    {
        $additionalStopWords = [
            'sane',
            'apple',
            'meat',
        ];

        $this->text->addStopWords($additionalStopWords);

        $this->assertTrue(Helpers::arrayContainsArray($additionalStopWords, $this->text->getStopWords()));
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetKeyWordsText1()
    {
        $this->text->setString($this->sampleText1);
        $keywordsList = $this->text->getKeyWords();

        $this->assertCount(300, $keywordsList);
    }

    public function testInvalidateRepeatingCharacters()
    {
        $isValid = Helpers::invokeMethod($this->text, 'isOfRepeatingCharacters', 'llll');
        $this->assertTrue($isValid);
        $isValid = Helpers::invokeMethod($this->text, 'isOfRepeatingCharacters', 'llllk');
        $this->assertFalse($isValid);
    }

    public function testGetRankedKeyWordsText1()
    {
        $this->text->setString($this->sampleText1);
        $keywordsList = $this->text->getRankedKeyWords();
        $this->assertCount(300, $keywordsList);
    }
}
