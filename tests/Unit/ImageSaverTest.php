<?php

namespace Tests\Unit;

use App\Services\Interfaces\FetchInterface;
use App\Services\Interfaces\FileManagerInterface;
use App\Services\Interfaces\ImageDataInterface;
use App\Services\Interfaces\ImageSaverInterface;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImageSaverTest extends TestCase
{

    private ImageSaverInterface $imageSaver;

    private ImageDataInterface $imageData;

    public function setUp(): void
    {
        parent::setUp();
        $file  = base_path()."/tests/SampleData/images/DSC0446.jpg";
        $fetch = $this->mock(FetchInterface::class);
        $fetch->shouldReceive('setUrl')->andReturnSelf();
        $fetch->shouldReceive('getContent')->andReturn(file_get_contents($file));
        $this->imageSaver = $this->app->makeWith(ImageSaverInterface::class, ['fetch' => $fetch]);
        $this->imageData  = $this->app->make(ImageDataInterface::class);

        $this->imageData->
        setUrl(env('APP_URL').'/sample/DSC0446.jpg')->
        setWidth(1200)->
        setHeight(800)->
        setAlt('good picture');
        Storage::fake(FileManagerInterface::DISK_IMAGES);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testSavesImageToStorage()
    {
        $this->imageSaver->setImageData($this->imageData)->save();
        $fileName = $this->imageSaver->getFilePath();
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($fileName);
    }
}
