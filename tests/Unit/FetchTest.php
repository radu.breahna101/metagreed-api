<?php

namespace Tests\Unit;

use App\Services\Interfaces\FetchInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use GuzzleHttp\Middleware;

class FetchTest extends TestCase
{
    private string $homepageContent;

    /**
     * @var FetchInterface
     */
    private FetchInterface $fetch;

    public function setUp(): void
    {
        parent::setUp();
    }

    private function initializeWithMockedClient(string $content)
    {
        $mock = new MockHandler([
            new Response(200, [], $content),
        ]);

        $client      = new Client(['handler' => HandlerStack::create($mock)]);
        $this->fetch = $this->app->makeWith(FetchInterface::class, ['client' => $client]);
    }

    public function testUserAgentRandomisation()
    {
        $mock         = new MockHandler([
            new Response(200, [], ""),
            new Response(200, [], ""),
            new Response(200, [], ""),
            new Response(200, [], ""),
            new Response(200, [], ""),
            new Response(200, [], ""),
        ]);
        $container    = [];
        $history      = Middleware::history($container);
        $handlerStack = HandlerStack::create($mock);
        $handlerStack->push($history);

        $client      = new Client(['handler' => $handlerStack]);
        $this->fetch = $this->app->makeWith(FetchInterface::class, ['client' => $client]);

        $this->fetch->setUrl('/');
        $this->fetch->getContent();
        $this->fetch->getContent();
        $this->fetch->getContent();
        $this->fetch->getContent();
        $this->fetch->getContent();
        $this->fetch->getContent();

        $userAgents = [];

        foreach ($container as $transaction) {
            $userAgent    = $transaction['request']->getHeaders()['User-Agent'][0];
            $userAgents[] = $userAgent;
        }

        $uniqueAgents = array_unique($userAgents);

        $this->assertGreaterThanOrEqual(2, count($uniqueAgents));
    }

    public function testHomepage1EndpointAvailable()
    {
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage1.html");
        $this->initializeWithMockedClient($this->homepageContent);
        $this->fetch->setUrl(url('/homepage1'));
        $this->assertEquals($this->homepageContent, $this->fetch->getContent());
    }

    public function testHomepage2EndpointAvailable()
    {
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage2.html");
        $this->initializeWithMockedClient($this->homepageContent);
        $this->fetch->setUrl(url('/homepage2'));
        $content = $this->fetch->getContent();
        $this->assertEquals($this->homepageContent, $content);
    }

    public function testHomepage3EndpointAvailable()
    {
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage3.html");
        $this->initializeWithMockedClient($this->homepageContent);
        $this->fetch->setUrl(url('/homepage3'));
        $content = $this->fetch->getContent();
        $this->assertEquals($this->homepageContent, $content);
    }

    public function testHomepage4EndpointAvailable()
    {
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage4.html");
        $this->initializeWithMockedClient($this->homepageContent);
        $this->fetch->setUrl(url('/homepage4'));
        $content = $this->fetch->getContent();
        $this->assertEquals($this->homepageContent, $content);
    }

}
