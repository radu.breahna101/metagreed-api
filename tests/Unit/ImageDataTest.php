<?php

namespace Tests\Unit;

use App\Services\Interfaces\ImageDataInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ImageDataTest extends TestCase
{
    private ImageDataInterface $imageData;

    public function setUp(): void
    {
        parent::setUp();
        $this->imageData = $this->app->make(ImageDataInterface::class);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testImageDataCreation()
    {
        $url            = 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-2.jpg';
        $width          = 2000;
        $height         = 1500;
        $alt            = 'test alt text';
        $additionalText = [
            'gfggctytcytctc',
            'gcggcgcycycyc',
            'hvuvvvjvjvj',
        ];

        $this->imageData->setUrl($url);
        $this->imageData->setWidth($width);
        $this->imageData->setHeight($height);
        $this->imageData->setAlt($alt);
        $this->imageData->setAdditionalText($additionalText);

        $this->assertEquals($this->imageData->getUrl(), $url);
        $this->assertEquals($this->imageData->getWidth(), $width);
        $this->assertEquals($this->imageData->getHeight(), $height);
        $this->assertEquals($this->imageData->getAlt(), $alt);
        $this->assertEquals($this->imageData->getAdditionalText(), $additionalText);
    }
}
