<?php

namespace Tests\Unit;

use App\Services\Interfaces\DomNodeInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NodeTest extends TestCase
{
    private string $homepageContent;

    private DomNodeInterface $node;

    public function setUp(): void
    {
        parent::setUp();
        $this->node = $this->app->make(DomNodeInterface::class);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDomNodeGetsAnchorByFirstPostSelectorHomepage1()
    {
        $expected              = "https://www.thisiscolossal.com/2019/10/ritas-quilt/";
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage1.html");
        $this->node->setContent($this->homepageContent);
        $this->node->setSelector('.entry-content > #posts > h2 > a');
        $href = $this->node->getAttribute('href');
        $this->assertEquals($expected, $href);
    }

    public function testDomNodeGetsAnchorByFirstPostSelectorHomepage2()
    {
        $expected              = "https://www.boredpanda.com/best-celebrity-halloween-costumes-2019/";
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage2.html");
        $this->node->setContent($this->homepageContent);
        $this->node->setSelector('.posts > .post > h2 > a');
        $href = $this->node->getAttribute('href');
        $this->assertEquals($expected, $href);
    }

    public function testDomNodeGetsAnchorByFirstPostSelectorHomepage3()
    {
        $expected              = "https://www.eatliver.com/not-impressed-cats/";
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage3.html");
        $this->node->setContent($this->homepageContent);
        $this->node->setSelector('#content > article > header > h2 > a');
        $href = $this->node->getAttribute('href');
        $this->assertEquals($expected, $href);
    }

    public function testDomNodeGetsAnchorByFirstPostSelectorHomepage4()
    {
        $expected              = "https://www.booooooom.com/2019/11/01/salish-sea-by-artist-klee-larsen/";
        $this->homepageContent = file_get_contents(base_path()."/tests/SampleData/homepage4.html");
        $this->node->setContent($this->homepageContent);
        $this->node->setSelector('.content-grid > article:nth-of-type(2) > a'); //in this case we are targeting the second post
        $href = $this->node->getAttribute('href');
        $this->assertEquals($expected, $href);
    }
}
