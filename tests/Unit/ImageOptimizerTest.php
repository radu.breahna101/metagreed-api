<?php

namespace Tests\Unit;

use App\Services\Interfaces\FileManagerInterface;
use App\Services\Interfaces\ImageOptimizerInterface;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ImageOptimizerTest extends TestCase
{

    private ImageOptimizerInterface $imageOptimizer;

    private FileManagerInterface $fileManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->imageOptimizer = $this->app->make(ImageOptimizerInterface::class);
        $this->fileManager    = $this->app->make(FileManagerInterface::class);
        Storage::fake(FileManagerInterface::DISK_IMAGES);
    }

    private function getCompressionRate(int $initial, int $compressed)
    {
        return 100 - round($compressed * 100 / $initial);
    }

    public function testImageIsOptimizedJpeg()
    {
        $imageName    = "test.jpg";
        $imageContent = file_get_contents(base_path()."/tests/SampleData/images/DSC0446.jpg");

        $this->fileManager->
        setStringContent($imageContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFileName($imageName)->save();

        $this->assertTrue($this->fileManager->isSaved());
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($imageName);

        $initialSize = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);
        $pathToImage = $this->fileManager->getPathToSavedFile();

        $this->imageOptimizer->
        setPath($pathToImage)->
        optimize();

        $compressed = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);
        //dd($this->getCompressionRate($initialSize, $compressed));
        $this->assertGreaterThanOrEqual(77, $this->getCompressionRate($initialSize, $compressed));
    }

    public function testImageIsOptimizedPng()
    {
        $imageName    = "test.png";
        $imageContent = file_get_contents(base_path()."/tests/SampleData/images/samplePng.png");

        $this->fileManager->
        setStringContent($imageContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFileName($imageName)->save();

        $this->assertTrue($this->fileManager->isSaved());
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($imageName);

        $initialSize = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);
        $pathToImage = $this->fileManager->getPathToSavedFile();

        $this->imageOptimizer->
        setPath($pathToImage)->
        optimize();

        $compressed = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);

        //dd($initialSize, $compressed);
       // dd($this->getCompressionRate($initialSize, $compressed));
        $this->assertGreaterThanOrEqual(50, $this->getCompressionRate($initialSize, $compressed));
    }

    public function testImageIsOptimizedGif()
    {
        $imageName    = "test.gif";
        $imageContent = file_get_contents(base_path()."/tests/SampleData/images/sampleGif.gif");

        $this->fileManager->
        setStringContent($imageContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFileName($imageName)->save();

        $this->assertTrue($this->fileManager->isSaved());
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($imageName);

        $initialSize = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);
        $pathToImage = $this->fileManager->getPathToSavedFile();

        $this->imageOptimizer->
        setPath($pathToImage)->
        optimize();

        $compressed = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);

        //dd($initialSize, $compressed);
        // dd($this->getCompressionRate($initialSize, $compressed));
        $this->assertGreaterThanOrEqual(3, $this->getCompressionRate($initialSize, $compressed));
    }

    public function testImageIsOptimizedSvg()
    {
        $imageName    = "test.svg";
        $imageContent = file_get_contents(base_path()."/tests/SampleData/images/sampleSvg.svg");

        $this->fileManager->
        setStringContent($imageContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFileName($imageName)->save();

        $this->assertTrue($this->fileManager->isSaved());
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($imageName);

        $initialSize = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);
        $pathToImage = $this->fileManager->getPathToSavedFile();

        $this->imageOptimizer->
        setPath($pathToImage)->
        optimize();

        $compressed = Storage::disk(FileManagerInterface::DISK_IMAGES)->size($imageName);

        //dd($initialSize, $compressed);
        //dd($this->getCompressionRate($initialSize, $compressed));
        $this->assertGreaterThanOrEqual(2, $this->getCompressionRate($initialSize, $compressed));
    }

}
