<?php

namespace Tests\Unit;

use App\Services\Interfaces\VariableExporterInterface;
use App\Services\Utilities\Helpers;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VariableExporterTest extends TestCase
{
    private VariableExporterInterface $variableExport;

    public function setUp(): void
    {
        parent::setUp();
        $this->variableExport = $this->app->make(VariableExporterInterface::class);
    }

    public function testArray1()
    {
        $content  = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost1.php");
        $expected = file_get_contents(base_path()."/tests/ExpectedText/variable1.txt");
        $result = $this->variableExport->setArray($content)->getExportedString();
        //file_put_contents(base_path()."/tests/ExpectedText/variable1.txt", $result);
        $this->assertEquals($expected, $result);
    }

    public function testArray2()
    {
        $content  = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost2.php");
        $expected = file_get_contents(base_path()."/tests/ExpectedText/variable2.txt");
        $result = $this->variableExport->setArray($content)->getExportedString();
        //file_put_contents(base_path()."/tests/ExpectedText/variable2.txt", $result);
        $this->assertEquals($expected, $result);
    }

    public function testArray3()
    {
        $content  = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost3.php");
        $expected = file_get_contents(base_path()."/tests/ExpectedText/variable3.txt");
        $result = $this->variableExport->setArray($content)->getExportedString();
        //file_put_contents(base_path()."/tests/ExpectedText/variable3.txt", $result);
        $this->assertEquals($expected, $result);
    }

    public function testArray4()
    {
        $content  = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost4.php");
        $expected = file_get_contents(base_path()."/tests/ExpectedText/variable4.txt");
        $result = $this->variableExport->setArray($content)->getExportedString();
        //file_put_contents(base_path()."/tests/ExpectedText/variable4.txt", $result);
        $this->assertEquals($expected, $result);
    }

    public function testArray5()
    {
        $content = [
            0 => 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-2.jpg',
            1 => 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-1.jpg',
            2 => 'https://www.thisiscolossal.com/wp-content/uploads/2019/10/rita-composite-2.jpg',
        ];
        $expected = file_get_contents(base_path()."/tests/ExpectedText/variable5.txt");
        $result = $this->variableExport->setArray($content)->getExportedString();
        //file_put_contents(base_path()."/tests/ExpectedText/variable5.txt", $result);

        $this->assertEquals($expected, $result);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
