<?php

namespace Tests\Unit;

use App\Services\Implementations\Images\ImageSaver;
use App\Services\Interfaces\FileManagerInterface;
use App\Services\Interfaces\ImageSaverInterface;
use App\Services\Utilities\Helpers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class FileManagerTest extends TestCase
{
    private FileManagerInterface $fileManager;

    public function setUp(): void
    {
        parent::setUp();
        $this->fileManager = $this->app->make(FileManagerInterface::class);
        Storage::fake(FileManagerInterface::DISK_IMAGES);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testSavesFileToFolder()
    {
        $time        = Carbon::now();
        $fileContent = file_get_contents(base_path()."/tests/SampleData/images/DSC0446.jpg");
        $folderName  = $time->year.'_'.$time->month;
        $fileName    = 'test.jpeg';

        $this->fileManager->
        setStringContent($fileContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFolderName($folderName)->
        setFileName($fileName)->
        save();

        $this->assertTrue($this->fileManager->isSaved());
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($folderName.'/'.$fileName);
    }

    public function testSavesFileToDiskRoot()
    {
        $fileContent = file_get_contents(base_path()."/tests/SampleData/images/DSC0446.jpg");
        $fileName    = 'test2.jpeg';
        $this->fileManager->
        setStringContent($fileContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFileName($fileName)->
        save();

        $this->assertTrue($this->fileManager->isSaved());
        Storage::disk(FileManagerInterface::DISK_IMAGES)->assertExists($fileName);
    }

    public function testFileManagerReturnsFilePath()
    {
        $time        = Carbon::now();
        $fileContent = file_get_contents(base_path()."/tests/SampleData/images/DSC0446.jpg");
        $folderName  = $time->year.'_'.$time->month;
        $fileName    = 'test.jpeg';

        $this->fileManager->
        setStringContent($fileContent)->
        setDisk(FileManagerInterface::DISK_IMAGES)->
        setFolderName($folderName)->
        setFileName($fileName)->
        save();
        $imageSaver = $this->app->make(ImageSaverInterface::class);

        $reflection = new \ReflectionClass(get_class($imageSaver));
        $method     = $reflection->getMethod('generateFolderName');
        $method->setAccessible(true);
        $method->invokeArgs($imageSaver, []);
        $folderName = $reflection->getProperty('folderName');
        $folderName->setAccessible(true);
        $folderName = $folderName->getValue($imageSaver);

        $path = $this->fileManager->getPathToSavedFile();

        $expectedPath = base_path().'/storage/framework/testing/disks/images/'.$folderName.'/test.jpeg';

        $this->assertEquals($expectedPath, $path);
    }
}
