<?php

namespace Tests\Unit;

use App\Services\Interfaces\DraftRepositoryInterface;
use App\Services\Interfaces\ImageDataInterface;
use App\Tag;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DraftRepositoryTest extends TestCase
{
    //use RefreshDatabase;
    //use DatabaseMigrations;
    use DatabaseTransactions;

    private DraftRepositoryInterface $draftRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->draftRepository = $this->app->make(DraftRepositoryInterface::class);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testDraftRepositoryCreatesEverything()
    {
        $rawText  = "In studios in Oslo and northern Italy, Norwegian sculptor Håkon Anton Fagerås uses a pneumatic hammer and other carving tools to shape blocks of marble into large white pillows. Slumped in natural poses, the realistic pillows feature smooth folds and wrinkles that contradict the properties of the medium. Without the shots of Fagerås in action, our eyes would not believe the finished products to be anything other than fabric and filler.In an interview with Sculpture Atelier, Fagerås explained his interest in the medium, saying marble is best for expressing the nuances of emotion. “Because of the material qualities of marble itself, it appears fragile. It’s quite fragile, but it’s not that fragile, and yet it appears so because of the translucency and pureness of the stone.” He added that it allows for sculpting at a very precise level, but that he tries “not to be too literal about it. I think that my main focus is to create an atmosphere, a sensation, more than a literal representation of something that expresses, for instance, fragility.”Head to Instagram to see more of Fagerås’s marble masterpieces.";
        $keyWords = [
            "fager",
            "marble",
            "fragile",
            "pillows",
            "literal",
            "medium",
            "appears",
            "qualities",
            "pureness",
            "translucency",
            "emotion",
            "material",
            "added",
            "nuances",
            "expressing",
            "interest",
            "explained",
            "atelier",
            "stone",
        ];

        $url = "http://metagreed.test/marble-pillows-by-hakon-anton-fageras";

        //http://docs.mockery.io/en/latest/reference/expectations.html
        $imageData = $this->mock(ImageDataInterface::class);
        $localPath = "sample/local/path";
        $imageData->shouldReceive('getLocalPath')->andReturn($localPath);
        $imageData->shouldReceive('getUrl')->andReturn($url."/sample.jpeg");
        $imageData->shouldReceive('getWidth')->andReturn(1000);
        $imageData->shouldReceive('getHeight')->andReturn(500);
        $imageData->shouldReceive('getAlt')->andReturn('sample alt');
       // $imageData->shouldReceive('getAdditionalText')->andReturn(['sample text']);

        $imageDataList = [$imageData];

        $this->draftRepository->setRawText($rawText);
        $this->draftRepository->setKeyWords($keyWords);
        $this->draftRepository->setSourceUrl($url);
        $this->draftRepository->setImageDataList($imageDataList);
        $this->draftRepository->build();

        $this->assertDatabaseHas('posts', [
            'is_draft'   => true,
            'raw_text'   => $rawText,
            'source_url' => $url,
        ]);

        $post = $this->draftRepository->getPost();

        foreach ($keyWords as $order => $keyWord) {
            $this->assertDatabaseHas('tags', [
                'text' => $keyWord,
            ]);

            $this->assertDatabaseHas('post_tag', [
                'tag_id'  => Tag::whereText($keyWord)->first()->id,
                'post_id' => $post->id,
                'order'   => $order,
            ]);
        }

        foreach ($imageDataList as $imageData) {
            $this->assertDatabaseHas('images', [
                'post_id' => $post->id,
                'path'    => $imageData->getLocalPath(),
                'url'     => $imageData->getUrl(),
                'height'  => $imageData->getHeight(),
                'width'   => $imageData->getWidth(),
                'alt'     => $imageData->getAlt(),
                //'text'    => $imageData->getAdditionalText(),
            ]);
        }
    }
}
