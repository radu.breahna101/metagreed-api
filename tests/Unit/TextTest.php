<?php

namespace Tests\Unit;

use App\Services\Interfaces\TextInterface;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TextTest extends TestCase
{
    private TextInterface $source;

    public function setUp(): void
    {
        parent::setUp();
        $this->source = $this->app->make(TextInterface::class);
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testSourceTextPost1()
    {

        $this->source->setContent($this->post1);
        $text = $this->source->getAllText();

        $expected = [
            "Chicago-based fiber artist and activist Shannon Downey has a particular affinity for unfinished projects. She seeks them out at estate sales, helping women who’ve passed complete their work. Although this has long been an area of interest for Downey, one recent discovery has catapulted to the front page of news sites around the world.",
            "On a visit this September to a Chicagoland estate sale sale, Downey happened upon a framed embroidery (pictured above) that maps out the United States and illustrates all fifty state flowers. A casual conversation with the cashier at the sale led to Downey finding a large-scale project related to the framed work. An unfinished queen-sized quilt was meant to incorporate another U.S. map along with hexagons featuring each state’s bird and flower along with the year they entered the union.",
            "Rita Smith of Mount Prospect, Illinois, had begun the project a few decades ago, according to her son, whom Downey subsequently connected with. Smith, who was a nurse, passed away recently at the age of 99. “I have an annoying habit of having to purchase and finish unfinished projects if I think that the person has passed on … but usually I’m just buying a half-done pillow that needs half an hour’s worth of stitching and then it’s done,” Downey told Block Club Chicago. “But this one was massive and it just felt really significant for some reason. And so I bought it.”",
            "Downey has a substantial following thanks to her work as a community organizer and resource for people looking for an alternative to digital distractions. The self-described ‘craftivist’ tells Colossal that after running a digital marketing company for a decade, she was burned out and needed a break from technology.",
            "I started stitching to find some digital/analog balance. I was hooked. I am an activist and I quickly fused the two. At first, it was about creating space for myself to think substantively and reflect on various issues and topics. As I started to share my work and thoughts, I found a community of folks on Instagram who were engaged and engaging. I have found ways to move those communities offline and into real life communities through my stitch-ups. Those communities are what inspire me to keep going, level up, find new ways of building and connecting, having hard conversations and tackling challenging topics.",
            "Once Downey shared her unexpected find on Twitter, inquiries from potential collaborators began flooding in. Now she is coordinating between dozens of stitchers across the country who are volunteering their time to complete Rita’s masterpiece. Downey describes the effort as a strongly feminist project. “It is an opportunity for folks to consider how we define and assign value and meaning to craft,” she tells Colossal. “So many of the stories that people are sharing with me on twitter after reading about #RitasQuilt are about memories and connections that they have to the women in their lives who are/were makers and the significance that their art has come to have for them.”",
            "The National Quilt Museum in Paducah, Kentucky will be displaying the completed quilt, and Downey hopes that it will be able to come with her for a planned 2020 craftivist tour around the U.S. Keep up with Downey on Twitter and Instagram to see how you can get involved in craftivism in your community, and follow along with the #RitasQuilt hashtag.",
        ];

        $this->assertEquals($expected, $text);
    }

    public function testSourceTextPost12()
    {

        $this->source->setContent($this->post12);
        $text = $this->source->getAllText();

        $expected = [
            '“Astrum”, 11 x 14 inches. All images courtesy of the artist',
            'At the Direktorenhaus Museum in Berlin this past week, a solo exhibition of detailed architectural drawings by Virginia-based artist Benjamin Sack (previously) opened to the public. Titled Labyrinths, the collection of new works features vast cityscapes comprised of impossible inner-geometries. The maze-like urban maps reference musical compositions and various symbols found in cosmology.',
            'Often creating based on what he calls a “fear of blank spaces,” Sack tells Colossal that his starting point for each drawing is different. Finding inspiration in history, cartography, and his own travels, the artist starts with a general concept and builds his intricate worlds intuitively as he goes. Star-shaped buildings and pathways meet with rows of houses that spiral out from clusters of skyscrapers. The pieces in Labyrinths range from 11 inches by 14 inches (a standard photo print size) up to 90 inches wide and 69 inches tall. A work titled Library of Babel is drawn on the surface of a globe measuring 16 inches in diameter. “Generally, a large piece is begun with a few very broad and simple demarkations in pencil,” Sack explains. The rest of the lines and spaces are filled in with pen.',
            '“Over many years my interest in architecture and cityscapes has evolved,” Sack tells Colossal. He adds that drawing such intricate pieces has “become a way and means of expressing the infinite, playing with perspective and exploring a range of histories, cultures, places.”',
            'Labyrinths will be exhibited through January 22, 2020. For more of Sack’s imaginative maps, follow the artist on Instagram.',
            '“Library of Babel” (globe piece), 16 inches in diameter',
            '“Library of Babel” detail',
            '“Library of Babel” detail',
            '“Library of Babel” detail',
            '“Library of Babel” detail',
            '“Canto IV” 70 x 70 inches',
            '“Eden” 14 x 11 inches',
            '“Peregrinations” 68 x 93 inches',
            '“Samsara” 12 x 18 inches',
            '“Stella Aurora” 11 x 14 inches',
        ];

        $this->assertEquals($expected, $text);
    }

    public function testSourceTextPost2()
    {

        $this->source->setContent($this->post2);
        $text = $this->source->getAllText();

        $expected = [
            'Unusual animal friendships are perfect proof that love knows no boundaries. And these two know it. Nova the German Shepherd and Pacco the ferret have been crazy about each other since day one. Their owner, Diana Grib, said they’re living together as if they don’t know they’re different.',
            'Diana said she first got Nova – Karamba Laukinė Vilkauogė. From the moment she got her, the woman introduced her to all kinds of animals, including rats, cats, and parrots. Diana spent quite some time teaching her pup to get along with everyone. When Nova became an adult, Diana set out to get the other pet she was dreaming about. Contacting many ferret breeders, her main priority was to get one that knows how to be with dogs. Ultimately, her search took her to Krakow where she got Pacco.',
            'More info: Instagram',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Nova and Pacco have some similarities, too. They’re both curious, playful and brave. Their matching personalities and the fact that they both met other animals when they were growing up are probably the main reasons why they get along so nicely. When they first met, Pacco was taking a tour of the new home while Nova was following her new friend like a shadow. After a few days, Pacco started paying attention to her and the fun started.',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Now, the two are constantly playing, traveling, even sleeping together. “It sometimes feels that I own two dogs,” Diana told Bored Panda. The ruler of the house, however, is the ferret. “Considering that ferrets are cheeky and stubborn, it’s understandable. Nova is really happy to just have someone by her side.',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Diana said one has to read a lot if they want to own a ferret. “From creating a safe environment in the house to developing a raw menu, health issues and dealing with poop, you have to know a lot about them,” she explained. “Before I got Pacco, I spent at least half a year gathering information. Still, I’m learning about him every day.”',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Diana also warned that ferrets aren’t for everyone. “For starters, they bite. Some do it more, some less, but they definitely let you know if they don’t like something. They also sense fear, so if you get afraid of them, they’ll use it to their advantage.” So, if you have little kids running around or aren’t willing to put up with their neediness, maybe you shouldn’t get one.',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Another thing to consider is their health. “You really need to pay close attention to the conditions you create for them and the food you give them. If you mess something up, it can quickly snowball into huge veterinary bills.”',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Nova, on the other hand, is a really friendly dog. She’s always ready to show her love to the entire world, whether it’s someone she knows or a random passer-by. “It also helps that she’s very patient. She doesn’t get mad at Pacco even if he hurts her a little bit while playing.”',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Image credits: nova_n_pacco',
            'Like what you\'re reading? Subscribe to our top stories.',
            'Bored Panda works best if you switch to our Android app',
            'Bored Panda works better on our iPhone app!',
            'Follow Bored Panda on Google News!',
            'Follow us on Flipboard.com/@boredpanda!',
        ];

        $this->assertEquals($expected, $text);
    }

    public function testSourceTextPost3()
    {

        $this->source->setContent($this->post3);
        $text = $this->source->getAllText();

        $expected = [
            "{1 Comment}",
            "on November 7, 2019",
            "Many laws that still exist throughout the united states are wildly outdated, rendering them completely useless, ridiculous and weird. The absurdity is illustrated by New York-based photographer Olivia Locher, who catalogs the crazy rules and regulations of each state in a playful photographic series “I Fought The Law“. Scroll down to see the funniest examples!",
            "in Russia, drinking perfume is very popular",
            "Leave Name blank to comment as Anonymous.",
        ];

        $this->assertEquals($expected, $text);
    }

    public function testSourceTextPost4()
    {

        $this->source->setContent($this->post4);
        $text = $this->source->getAllText();

        $expected = [
            '07.11.19
                                —
                                Anna Schneider',
            'A selection of images showcasing the quirky style of New York-based photographer Dan Allegretto (previously featured here). More of Allegretto’s work will be on display in an upcoming solo show at Nepenthes in New York, opening November 14th. See more below!',
            'Dan Allegretto’s Website',
            'Dan Allegretto on Instagram',
            'We’re really excited to release 5 print editions in our shop today! Each is available until November 12th and after that we won’t sell them again.',
            'Related Articles',
        ];

        $this->assertEquals($expected, $text);
    }

    private array $post1 = [
        [
            "img" => [
                "https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-2.jpg",
            ],
        ],
        [
            "p" => [
                "Chicago-based fiber artist and activist Shannon Downey has a particular affinity for unfinished projects. She seeks them out at estate sales, helping women who’ve passed complete their work. Although this has long been an area of interest for Downey, one recent discovery has catapulted to the front page of news sites around the world.",
            ],
        ],
        [
            "p" => [
                "On a visit this September to a Chicagoland estate sale sale, Downey happened upon a framed embroidery (pictured above) that maps out the United States and illustrates all fifty state flowers. A casual conversation with the cashier at the sale led to Downey finding a large-scale project related to the framed work. An unfinished queen-sized quilt was meant to incorporate another U.S. map along with hexagons featuring each state’s bird and flower along with the year they entered the union.",
            ],
        ],
        [
            "p" => [
                "Rita Smith of Mount Prospect, Illinois, had begun the project a few decades ago, according to her son, whom Downey subsequently connected with. Smith, who was a nurse, passed away recently at the age of 99. “I have an annoying habit of having to purchase and finish unfinished projects if I think that the person has passed on … but usually I’m just buying a half-done pillow that needs half an hour’s worth of stitching and then it’s done,” Downey told Block Club Chicago. “But this one was massive and it just felt really significant for some reason. And so I bought it.”",
            ],
        ],
        [
            "img" => [
                "https://www.thisiscolossal.com/wp-content/uploads/2019/10/quilt-1.jpg",
            ],
        ],
        [
            "p" => [
                "Downey has a substantial following thanks to her work as a community organizer and resource for people looking for an alternative to digital distractions. The self-described ‘craftivist’ tells Colossal that after running a digital marketing company for a decade, she was burned out and needed a break from technology.",
            ],
        ],
        [
            "p" => [
                "I started stitching to find some digital/analog balance. I was hooked. I am an activist and I quickly fused the two. At first, it was about creating space for myself to think substantively and reflect on various issues and topics. As I started to share my work and thoughts, I found a community of folks on Instagram who were engaged and engaging. I have found ways to move those communities offline and into real life communities through my stitch-ups. Those communities are what inspire me to keep going, level up, find new ways of building and connecting, having hard conversations and tackling challenging topics.",
            ],
        ],
        [
            "p" => [
                "Once Downey shared her unexpected find on Twitter, inquiries from potential collaborators began flooding in. Now she is coordinating between dozens of stitchers across the country who are volunteering their time to complete Rita’s masterpiece. Downey describes the effort as a strongly feminist project. “It is an opportunity for folks to consider how we define and assign value and meaning to craft,” she tells Colossal. “So many of the stories that people are sharing with me on twitter after reading about #RitasQuilt are about memories and connections that they have to the women in their lives who are/were makers and the significance that their art has come to have for them.”",
            ],
        ],
        [
            "p" => [
                "The National Quilt Museum in Paducah, Kentucky will be displaying the completed quilt, and Downey hopes that it will be able to come with her for a planned 2020 craftivist tour around the U.S. Keep up with Downey on Twitter and Instagram to see how you can get involved in craftivism in your community, and follow along with the #RitasQuilt hashtag.",
            ],
        ],
        [
            "img" => [
                "https://www.thisiscolossal.com/wp-content/uploads/2019/10/rita-composite-2.jpg",
            ],
        ],
    ];

    private array $post12 = [
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/Astrum.jpg"],
            "p"   => ["“Astrum”, 11 x 14 inches. All images courtesy of the artist"],
        ],
        [
            "p" => ["At the Direktorenhaus Museum in Berlin this past week, a solo exhibition of detailed architectural drawings by Virginia-based artist Benjamin Sack (previously) opened to the public. Titled Labyrinths, the collection of new works features vast cityscapes comprised of impossible inner-geometries. The maze-like urban maps reference musical compositions and various symbols found in cosmology."],
        ],
        [
            "p" => ["Often creating based on what he calls a “fear of blank spaces,” Sack tells Colossal that his starting point for each drawing is different. Finding inspiration in history, cartography, and his own travels, the artist starts with a general concept and builds his intricate worlds intuitively as he goes. Star-shaped buildings and pathways meet with rows of houses that spiral out from clusters of skyscrapers. The pieces in Labyrinths range from 11 inches by 14 inches (a standard photo print size) up to 90 inches wide and 69 inches tall. A work titled Library of Babel is drawn on the surface of a globe measuring 16 inches in diameter. “Generally, a large piece is begun with a few very broad and simple demarkations in pencil,” Sack explains. The rest of the lines and spaces are filled in with pen."],
        ],
        [
            "p" => ["“Over many years my interest in architecture and cityscapes has evolved,” Sack tells Colossal. He adds that drawing such intricate pieces has “become a way and means of expressing the infinite, playing with perspective and exploring a range of histories, cultures, places.”"],
        ],
        [
            "p" => ["Labyrinths will be exhibited through January 22, 2020. For more of Sack’s imaginative maps, follow the artist on Instagram."],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel5-e1572636437178.jpg"],
            "p"   => ["“Library of Babel” (globe piece), 16 inches in diameter"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel7-e1572636476140.jpg"],
            "p"   => ["“Library of Babel” detail"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel4-e1572636512696.jpg"],
            "p"   => ["“Library of Babel” detail"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel3-e1572636542319.jpg"],
            "p"   => ["“Library of Babel” detail"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/library-of-babel6-e1572636576429.jpg"],
            "p"   => ["“Library of Babel” detail"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/CANTO-IV-Large.jpeg"],
            "p"   => ["“Canto IV” 70 x 70 inches"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/EDEN_large_1.jpg"],
            "p"   => ["“Eden” 14 x 11 inches"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/Peregrinations_68x92.75.jpg"],
            "p"   => ["“Peregrinations” 68 x 93 inches"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/SAMSARA_large_final-copy.jpg"],
            "p"   => ["“Samsara” 12 x 18 inches"],
        ],
        [
            "img" => ["https://www.thisiscolossal.com/wp-content/uploads/2019/11/Stella-Corona-copy.jpg"],
            "p"   => ["“Stella Aurora” 11 x 14 inches"],
        ],
    ];

    private array $post2 = [

        [
            'p' => ['Unusual animal friendships are perfect proof that love knows no boundaries. And these two know it. Nova the German Shepherd and Pacco the ferret have been crazy about each other since day one. Their owner, Diana Grib, said they’re living together as if they don’t know they’re different.',],
        ],

        [
            'p' => ['Diana said she first got Nova – Karamba Laukinė Vilkauogė. From the moment she got her, the woman introduced her to all kinds of animals, including rats, cats, and parrots. Diana spent quite some time teaching her pup to get along with everyone. When Nova became an adult, Diana set out to get the other pet she was dreaming about. Contacting many ferret breeders, her main priority was to get one that knows how to be with dogs. Ultimately, her search took her to Krakow where she got Pacco.',],
        ],
        [
            'p' => ['More info: Instagram'],
        ],
        [
            'img' => ['https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-2-5dc29c93eb934__700.jpg',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco',],
        ],
        [
            'img' => ['https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29fe81cdf7__700.jpg',],
        ],
        [
            'img' => ['https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5dc29fe99ac05__700.jpg'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Nova and Pacco have some similarities, too. They’re both curious, playful and brave. Their matching personalities and the fact that they both met other animals when they were growing up are probably the main reasons why they get along so nicely. When they first met, Pacco was taking a tour of the new home while Nova was following her new friend like a shadow. After a few days, Pacco started paying attention to her and the fun started.',],
        ],
        [
            'img' => ['https://static.boredpanda.com/blog/wp-content/uploads/2019/11/dog-ferret-friendship-nova-and-pacco-5-5dc29c998e952__700.jpg',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco',],
        ],
        [
            'p' => ['Now, the two are constantly playing, traveling, even sleeping together. “It sometimes feels that I own two dogs,” Diana told Bored Panda. The ruler of the house, however, is the ferret. “Considering that ferrets are cheeky and stubborn, it’s understandable. Nova is really happy to just have someone by her side.',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco',],
        ],
        [
            'p' => ['Diana said one has to read a lot if they want to own a ferret. “From creating a safe environment in the house to developing a raw menu, health issues and dealing with poop, you have to know a lot about them,” she explained. “Before I got Pacco, I spent at least half a year gathering information. Still, I’m learning about him every day.”',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Diana also warned that ferrets aren’t for everyone. “For starters, they bite. Some do it more, some less, but they definitely let you know if they don’t like something. They also sense fear, so if you get afraid of them, they’ll use it to their advantage.” So, if you have little kids running around or aren’t willing to put up with their neediness, maybe you shouldn’t get one.',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Another thing to consider is their health. “You really need to pay close attention to the conditions you create for them and the food you give them. If you mess something up, it can quickly snowball into huge veterinary bills.”',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Nova, on the other hand, is a really friendly dog. She’s always ready to show her love to the entire world, whether it’s someone she knows or a random passer-by. “It also helps that she’s very patient. She doesn’t get mad at Pacco even if he hurts her a little bit while playing.”',],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Image credits: nova_n_pacco'],
        ],
        [
            'p' => ['Like what you\'re reading? Subscribe to our top stories.'],
        ],
        [
            'p' => ['Bored Panda works best if you switch to our Android app'],
        ],
        [
            'p' => ['Bored Panda works better on our iPhone app!'],
        ],
        [
            'p' => ['Follow Bored Panda on Google News!'],
        ],
        [
            'p' => ['Follow us on Flipboard.com/@boredpanda!'],
        ],
    ];

    private array $post3 = [
        [
            'p'   =>
                [
                    '{1 Comment}',
                    'on November 7, 2019',
                    'Many laws that still exist throughout the united states are wildly outdated, rendering them completely useless, ridiculous and weird. The absurdity is illustrated by New York-based photographer Olivia Locher, who catalogs the crazy rules and regulations of each state in a playful photographic series “I Fought The Law“. Scroll down to see the funniest examples!',
                ],
            'img' =>
                [
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws1.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws2.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws3.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws4.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws5.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws6.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws7.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws8.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws9.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws10.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws11.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws12.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws13.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws14.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws15.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws16.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws17.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws18.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws19.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws20.jpg',
                    'https://eatliver.b-cdn.net/wp-content/uploads/2019/11/usa-laws21.jpg',
                ],
        ],
        [
            'p' => [
                'in Russia, drinking perfume is very popular',
                'Leave Name blank to comment as Anonymous.',
            ],
        ],
    ];

    private array $post4 = [
        [
            'img' => ['https://www.booooooom.com/wp-content/uploads/2019/10/greencurtains.jpg'],
            'p'   => [
                '07.11.19
                                —
                                Anna Schneider',
            ],
        ],
        [
            'p'   =>
                [
                    'A selection of images showcasing the quirky style of New York-based photographer Dan Allegretto (previously featured here). More of Allegretto’s work will be on display in an upcoming solo show at Nepenthes in New York, opening November 14th. See more below!',
                    'Dan Allegretto’s Website',
                    'Dan Allegretto on Instagram',
                    'We’re really excited to release 5 print editions in our shop today! Each is available until November 12th and after that we won’t sell them again.',
                    'Related Articles',
                ],
            'img' =>
                [
                    'https://www.booooooom.com/wp-content/uploads/2019/10/IMG_7626-copy.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/IMG_9420-copy.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/IMG_3309-copy.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/stormcloud.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/showercurtain.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/greentomato-1.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/neonlightcorner-1.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/ddtowel-1.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/promlegs_edit-1.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/greencurtains-1.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/R0001413.jpg',
                    'https://www.booooooom.com/wp-content/uploads/2019/10/prints-postwidget.jpg',
                ],
        ],

    ];
}
