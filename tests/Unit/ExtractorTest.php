<?php

namespace Tests\Unit;

use App\Services\Interfaces\VariableExporterInterface;
use App\Services\Utilities\VariableExporter;
use App\Services\Interfaces\ExtractorInterface;
use App\Services\Utilities\Helpers;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExtractorTest extends TestCase
{
    private string $postPageContent;

    private ExtractorInterface $extractor;

    private VariableExporterInterface $variableExport;

    public function setUp(): void
    {
        parent::setUp();
        $this->extractor = $this->app->make(ExtractorInterface::class);
        $this->variableExport = $this->app->make(VariableExporterInterface::class);
    }

    private function setExtractionTags()
    {
        $this->extractor->setExtractionTags([
            ExtractorInterface::TAG_IMAGE     => [
                ExtractorInterface::ATTRIBUTE_SRC,
                ExtractorInterface::ATTRIBUTE_DATA_SRC,
                ExtractorInterface::ATTRIBUTE_ALT,
                ExtractorInterface::ATTRIBUTE_WIDTH,
                ExtractorInterface::ATTRIBUTE_HEIGHT,
            ],
            ExtractorInterface::TAG_PARAGRAPH => [
                ExtractorInterface::ATTRIBUTE_TEXT,
            ],
        ]);
    }

    private function exportExpectedResult(array $result)
    {
        $this->variableExport->setArray($result)
            ->setFilename(storage_path('/output.txt'))
            ->exportToFile();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExtractorGetsDataFromSinglePost1()
    {
        $this->postPageContent = file_get_contents(base_path()."/tests/SampleData/singlePost1.html");

        $this->extractor->setContent($this->postPageContent);
        $this->extractor->setContainerSelector('.entry-content');
        $this->setExtractionTags();
        $result = $this->extractor->extract();
       // $this->exportExpectedResult($result);
        $expected = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost1.php");
        $this->assertEquals($expected, $result);
    }

    public function testExtractorGetsDataFromSinglePost12()
    {
        $this->postPageContent = file_get_contents(base_path()."/tests/SampleData/singlePost12.html");
        $this->extractor->setContent($this->postPageContent);
        $this->extractor->setContainerSelector('.entry-content');
        $this->setExtractionTags();
        $result = $this->extractor->extract();
        //$this->exportExpectedResult($result);

        $expected = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost12.php");

        $this->assertEquals($expected, $result);
    }

    public function testExtractorGetsDataFromSinglePost2()
    {
        $this->postPageContent = file_get_contents(base_path()."/tests/SampleData/singlePost2.html");
        $this->extractor->setContent($this->postPageContent);
        $this->extractor->setContainerSelector('.post-content');
        $this->setExtractionTags();
        $result = $this->extractor->extract();
      //  $this->exportExpectedResult($result);
        $expected = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost2.php");
        $this->assertEquals($expected, $result);
    }

    public function testExtractorGetsDataFromSinglePost3()
    {
        $this->postPageContent = file_get_contents(base_path()."/tests/SampleData/singlePost3.html");
        $this->extractor->setContent($this->postPageContent);
        $this->extractor->setContainerSelector('#content');
        $this->setExtractionTags();
        $result = $this->extractor->extract();
       // $this->exportExpectedResult($result);
        $expected = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost3.php");
        $this->assertEquals($expected, $result);
    }

    public function testExtractorGetsDataFromSinglePost4()
    {
        $this->postPageContent = file_get_contents(base_path()."/tests/SampleData/singlePost4.html");
        $this->extractor->setContent($this->postPageContent);
        $this->extractor->setContainerSelector('.single-post-container');
        $this->setExtractionTags();
        $result = $this->extractor->extract();
       // $this->exportExpectedResult($result);
        $expected = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost4.php");

        $this->assertEquals($expected, $result);
    }

    public function testExtractorGetsDataFromSinglePost5()
    {
        $this->postPageContent = file_get_contents(base_path()."/tests/SampleData/singlePost5.html");
        $this->extractor->setContent($this->postPageContent);
        $this->extractor->setContainerSelector('.entry-content');
        $this->setExtractionTags();
        $result = $this->extractor->extract();
       // $this->exportExpectedResult($result);
        $expected = Helpers::getVariableFromFile('expected', base_path()."/tests/ExpectedCode/singlePost5.php");

        $this->assertEquals($expected, $result);
    }

}
