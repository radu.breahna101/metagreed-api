<?php

namespace Tests\Feature;

use App\Services\Interfaces\DraftInterface;
use App\Services\Interfaces\ExtractorInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
//The hand dryer and trash can both individually do their job correctly (they represent your classes, and you test them with unit tests).
//
//But when you put them together something breaks unexpectedly. And to test that you write feature tests that will, instead of testing a single small aspect of your app, try to mimic what happens when a user actually tries to use your app
class ExampleTest extends TestCase
{
    /**
     * @var \App\Services\Interfaces\DraftInterface
     */
    private DraftInterface $draft;

    public function testBasicTest()
    {
        $this->draft = $this->app->make(DraftInterface::class);
        $siteUrl            = 'https://www.thisiscolossal.com/';
        $latestPostSelector = '.entry-content > #posts > h2 > a';
        $singlePostSelector = '.entry-content';
        $extractionTags     = [
            ExtractorInterface::TAG_IMAGE     => [
                ExtractorInterface::ATTRIBUTE_SRC,
                ExtractorInterface::ATTRIBUTE_DATA_SRC,
                ExtractorInterface::ATTRIBUTE_ALT,
                ExtractorInterface::ATTRIBUTE_WIDTH,
                ExtractorInterface::ATTRIBUTE_HEIGHT,
            ],
            ExtractorInterface::TAG_PARAGRAPH => [
                ExtractorInterface::ATTRIBUTE_TEXT,
            ],
        ];


        //$this->draft->
        //setSiteUrl($siteUrl)->
        //setLatestPostSelector($latestPostSelector)->
        //setSinglePostSelector($singlePostSelector)->
        //setExtractionTags($extractionTags)->
        //store();

        $this->assertTrue(true);
    }
}
