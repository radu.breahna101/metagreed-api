<?php

namespace Tests\Feature;

use App\Post;
use App\Services\Interfaces\DraftInterface;
use App\Services\Interfaces\ExtractorInterface;
use App\Services\Interfaces\FetchInterface;
use App\Services\Interfaces\FileManagerInterface;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DraftManualTest extends TestCase
{
    use DatabaseTransactions;

    private DraftInterface $draft;

    public function setUp(): void
    {
        parent::setUp();
        Storage::fake(FileManagerInterface::DISK_IMAGES);
    }

    public function testManualDraftCreation()
    {
        $postUrl            = env('APP_URL').'/marble-pillows-by-hakon-anton-fageras';
        $singlePostSelector = '.entry-content';
        $extractionTags     = [
            ExtractorInterface::TAG_IMAGE     => [
                ExtractorInterface::ATTRIBUTE_SRC,
                ExtractorInterface::ATTRIBUTE_DATA_SRC,
                ExtractorInterface::ATTRIBUTE_ALT,
                ExtractorInterface::ATTRIBUTE_WIDTH,
                ExtractorInterface::ATTRIBUTE_HEIGHT,
            ],
            ExtractorInterface::TAG_PARAGRAPH => [
                ExtractorInterface::ATTRIBUTE_TEXT,
            ],
        ];

        $fetch = $this->mock(FetchInterface::class);
        $fetch->shouldReceive('setUrl')->andReturnSelf();
        $fetch->shouldReceive('getContent')->times(5)->
        andReturn(
            view('draftPost1'),
            file_get_contents(base_path()."/tests/SampleData/images/DSC1423b.jpg"),
            file_get_contents(base_path()."/tests/SampleData/images/DSC0446.jpg"),
            file_get_contents(base_path()."/tests/SampleData/images/DSC0621.jpg"),
            file_get_contents(base_path()."/tests/SampleData/images/ba363daf807586263f4940e8b522936f.jpg"),
            );
        $this->draft = $this->app->makeWith(DraftInterface::class, ['fetch' => $fetch]);

        $this->draft->
        setPostUrl($postUrl)->
        setSinglePostSelector($singlePostSelector)->
        setExtractionTags($extractionTags)->
        store();

        $rawText = 'In studios in Oslo and northern Italy, Norwegian sculptor Håkon Anton Fagerås uses a pneumatic hammer and other carving tools to shape blocks of marble into large white pillows. Slumped in natural poses, the realistic pillows feature smooth folds and wrinkles that contradict the properties of the medium. Without the shots of Fagerås in action, our eyes would not believe the finished products to be anything other than fabric and filler.In an interview with Sculpture Atelier, Fagerås explained his interest in the medium, saying marble is best for expressing the nuances of emotion. “Because of the material qualities of marble itself, it appears fragile. It’s quite fragile, but it’s not that fragile, and yet it appears so because of the translucency and pureness of the stone.” He added that it allows for sculpting at a very precise level, but that he tries “not to be too literal about it. I think that my main focus is to create an atmosphere, a sensation, more than a literal representation of something that expresses, for instance, fragility.”Head to Instagram to see more of Fagerås’s marble masterpieces.';

        $this->assertDatabaseHas('posts', [
            'raw_text'   => $rawText,
            'is_draft'   => true,
            'source_url' => $postUrl,
        ]);

        $post = Post::whereRawText($rawText)->first();

        $this->assertCount(4, $post->images);
        $this->assertCount(69, $post->tags);

        $tags = [
            0  => "fager",
            1  => "marble",
            2  => "fragile",
            3  => "pillows",
            4  => "literal",
            5  => "medium",
            6  => "appears",
            7  => "qualities",
            8  => "pureness",
            9  => "translucency",
            10 => "emotion",
            11 => "material",
            12 => "added",
            13 => "nuances",
            14 => "expressing",
            15 => "interest",
            16 => "explained",
            17 => "atelier",
            18 => "stone",
            19 => "studios",
            20 => "sculpting",
            21 => "interview",
            22 => "precise",
            23 => "level",
            24 => "main",
            25 => "focus",
            26 => "create",
            27 => "atmosphere",
            28 => "sensation",
            29 => "representation",
            30 => "expresses",
            31 => "instance",
            32 => "fragility",
            33 => "head",
            34 => "instagram",
            35 => "sculpture",
            36 => "products",
            37 => "filler",
            38 => "white",
            39 => "northern",
            40 => "italy",
            41 => "norwegian",
            42 => "sculptor",
            43 => "kon",
            44 => "anton",
            45 => "pneumatic",
            46 => "hammer",
            47 => "carving",
            48 => "tools",
            49 => "shape",
            50 => "blocks",
            51 => "large",
            52 => "slumped",
            53 => "fabric",
            54 => "natural",
            55 => "poses",
            56 => "realistic",
            57 => "feature",
            58 => "smooth",
            59 => "folds",
            60 => "wrinkles",
            61 => "contradict",
            62 => "properties",
            63 => "shots",
            64 => "action",
            65 => "eyes",
            66 => "finished",
            67 => "oslo",
            68 => "masterpieces",
        ];
        foreach ($tags as $tag) {
            $this->assertDatabaseHas('tags', [
                'text' => $tag,
            ]);
        }

        $files = Storage::disk(FileManagerInterface::DISK_IMAGES)->allFiles();
        $this->assertCount(4, $files);
    }
}
