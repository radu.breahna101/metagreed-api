<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Site;

class SiteSeederTableSeeder extends Seeder
{
    public function run()
    {
        $sites = [
            [
                'url'                  => 'https://www.thisiscolossal.com/',
                'name'                 => 'Collossal',
                'homepage_selector'    => '.entry-content',
                'single_post_selector' => '.entry-content',
                'latest_post_selector' => '.entry-content > #posts > h2 > a',
            ],
            [
                'url'                  => 'https://www.boredpanda.com',
                'name'                 => 'BoredPanda',
                'homepage_selector'    => '.posts',
                'single_post_selector' => '.post-content',
                'latest_post_selector' => '.posts > .post > h2 > a',
            ],
            [
                'url'                  => 'https://www.eatliver.com/',
                'name'                 => 'EatLiver',
                'homepage_selector'    => '#content',
                'single_post_selector' => '#content',
                'latest_post_selector' => '#content > article > header > h2 > a',
            ],
            //[
            //    'url'                  => 'http://www.beautifuldecay.com/',
            //    'name'                 => 'Decay',
            //    'homepage_selector'    => '#content',
            //    'single_post_selector' => '#content',
            //    'latest_post_selector' => '#content > article > header > h1 > a',
            //    // on each load a random selection of posts is presented
            //],
            [
                'url'                  => 'https://www.booooooom.com/',
                'name'                 => 'Boom',
                'homepage_selector'    => '.content-grid',
                'single_post_selector' => '.single-post-container',
                'latest_post_selector' => '.content-grid > article:nth-of-type(2) > a',
                //first element seems to be selection of videos
            ],

        ];

        $sites = collect($sites);

        $sites->each(function ($site) {
            Site::create($site);
        });
    }
}
