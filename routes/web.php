<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

if (app('env') == 'local') {

    //Route::get('/homepage1', function (Request $request) {
    //    return file_get_contents(base_path()."/tests/SampleData/homepage1.html");
    //});
    //Route::get('/homepage2', function (Request $request) {
    //    return file_get_contents(base_path()."/tests/SampleData/homepage2.html");
    //});
    //
    //Route::get('/homepage3', function (Request $request) {
    //    return file_get_contents(base_path()."/tests/SampleData/homepage3.html");
    //});
    //
    //Route::get('/homepage4', function (Request $request) {
    //    return file_get_contents(base_path()."/tests/SampleData/homepage4.html");
    //});
    //
    //Route::get('/single-post2', function (Request $request) {
    //    return file_get_contents(base_path()."/tests/SampleData/singlePost2.html");
    //});
    //
    //Route::get('/draft1', function (Request $request) {
    //    return view('draftHomepage1');
    //});
    //
    //Route::get('/marble-pillows-by-hakon-anton-fageras', function (Request $request) {
    //    return view('draftPost1');
    //});
    //
    //Route::get('/sample/DSC1423b.jpg', function (Request $request) {
    //    $file = base_path()."/tests/SampleData/images/DSC1423b.jpg";
    //
    //    return response()->file($file);
    //});
    //
    //Route::get('/sample/DSC0446.jpg', function (Request $request) {
    //    $file = base_path()."/tests/SampleData/images/DSC0446.jpg";
    //
    //    return response()->file($file);
    //});
    //
    //Route::get('/sample/DSC0621.jpg', function (Request $request) {
    //    $file = base_path()."/tests/SampleData/images/DSC0621.jpg";
    //
    //    return response()->file($file);
    //});
    //
    //Route::get('/sample/ba363daf807586263f4940e8b522936f.jpg', function (Request $request) {
    //    $file = base_path()."/tests/SampleData/images/ba363daf807586263f4940e8b522936f.jpg";
    //
    //    return response()->file($file);
    //});
}

Route::get('/', function () {
    return view('welcome');
});
