dockerDirectory=docker-metagreed

dev:
	cd $(dockerDirectory) && docker-compose up -d
update:
	composer update --ignore-platform-reqs
rebuild:
	cd $(dockerDirectory) && docker-compose exec php php artisan rebuild:app
autocommit:
	$(eval message=$(shell git status | grep modified))
	git commit -m "$(message)"
generic-commit:
	git commit -m "generic commit"
master:
	git push origin master
master-auto:
	make autocommit
	make master
own:
	#sudo chown -R $USER:$USER $(target)



