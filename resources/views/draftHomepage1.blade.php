

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "httpswww.w3.org/TR/html4/loose.dtd">
<html lang="en" itemscope itemtype="httpsschema.org/WebPage">
<head>

    <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6586576/7750372/css/fonts.css" />
    <title>Colossal | Art, design, and visual culture.</title>

    <meta charset="UTF-8" />
    <meta name="referrerpolicy" content="no-referrer" />
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="description" content="Colossal | Art, design, and visual culture" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta itemprop="name" content="Colossal" />
    <meta itemprop="description" content="Art, design, and visual culture." />
    <link rel="pingback" href="https://www.thisiscolossal.com/xmlrpc.php" />

    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js' type="68fa48e47a44e50bd58b7be7-text/javascript"></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
    </script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
      googletag.cmd.push(function() {

        // Define a size mapping object. The first parameter to addSize is
        // a viewport size, while the second is a list of allowed ad sizes.

        var header_map = googletag.sizeMapping()
          .addSize([0,0], [[320,160],[320,100],[320,50]])
          .addSize([360,0], [[360,180],[320,160],[320,100],[320,50]])
          .addSize([740,0], [[720,180],[728,90]])
          .addSize([1000,0], [[900,225],[970,250],[970,90]])
          .addSize([1100,0], [[1080,270],[970,250],[970,90]])
          .addSize([1260,0], [[1260,315],[1080,270],[970,250],[970,90]])
          .build();

        var sidebar_map = googletag.sizeMapping()
          .addSize([0, 0], [])
          .addSize([740, 0], [])
          .addSize([1000, 0], [[300,600],[300,250]])
          .build();

        var content_map = googletag.sizeMapping()
          .addSize([0, 0], [[320,480],[320,160],[300,250]])
          .addSize([360, 0], [[320,480],[360,180],[320,160],[300,250]])
          .addSize([740, 0], [[720,360],[720,180],[728,90]])
          .addSize([1000, 0], [[900,450],[900,225],[970,250],[970,90]])
          .addSize([1100, 0], [[1080,540],[1080,270],[970,250],[970,90]])
          .addSize([1260, 0], [[1260,630],[1260,315],[1080,540],[1080,270],[970,250],[970,90]])
          .build();

        // Define the GPT slot

        googletag.defineSlot('/16307266/colossal/col-header', [[1260,315],[1080,270],[900,225],[720,180],[360,180],[320,160],[970,250],[970,90],[728,90],[320,100],[320,50]], 'div-gpt-ad-1502230835991-0').defineSizeMapping(header_map).addService(googletag.pubads());
        googletag.defineSlot('/16307266/colossal/col-sidebar', [[300,600],[300,250]], 'div-gpt-ad-1502230835991-1').defineSizeMapping(sidebar_map).addService(googletag.pubads());
        googletag.defineSlot('/16307266/colossal/col-content', [[1260,630],[1260,315],[1080,540],[1080,270],[900,450],[900,225],[720,360],[720,180],[320,480],[970,250],[970,90],[728,90],[300,250]], 'div-gpt-ad-1502230835991-2').defineSizeMapping(content_map).addService(googletag.pubads());

        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
      });
    </script>


    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Colossal &raquo; Feed" href="https://www.thisiscolossal.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Colossal &raquo; Comments Feed" href="https://www.thisiscolossal.com/comments/feed/" />
    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.thisiscolossal.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css' href='https://www.thisiscolossal.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.5' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-email-css' href='https://www.thisiscolossal.com/wp-content/plugins/wp-email/email-css.css?ver=2.67.6' type='text/css' media='all' />
    <link rel='stylesheet' id='wordpress-popular-posts-css-css' href='https://www.thisiscolossal.com/wp-content/plugins/wordpress-popular-posts/assets/css/wpp.css?ver=5.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='colossal-reset-css' href='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/css/colossal_reset.css' type='text/css' media='all' />
    <link rel='stylesheet' id='colossal-main-css' href='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/css/colossal.css' type='text/css' media='all' />
    <link rel='stylesheet' id='colossal-nav-css' href='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/css/nav.css' type='text/css' media='all' />
    <link rel='stylesheet' id='colossal-mq-css' href='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/css/colossal_mq.css' type='text/css' media='all' />
    <link rel='stylesheet' id='twentytwelve-fonts-css' href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&#038;subset=latin,latin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='twentytwelve-style-css' href='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/style.css?ver=5.2.5' type='text/css' media='all' />
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='twentytwelve-ie-css'  href='https://www.thisiscolossal.com/wp-content/themes/colossalv3/css/ie.css?ver=20121010' type='text/css' media='all' />
    <![endif]-->
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/plugins/wp-retina-2x-pro/js/picturefill.min.js?ver=3.0.2'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
/* <![CDATA[ */
var wpp_params = {"sampling_active":"0","sampling_rate":"100","ajax_url":"https:\/\/www.thisiscolossal.com\/wp-json\/wordpress-popular-posts\/v1\/popular-posts","ID":"","token":"1ca2ff6227","debug":""};
/* ]]> */
</script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/plugins/wordpress-popular-posts/assets/js/wpp-5.0.0.min.js?ver=5.0.0'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/js/jquery-2.2.0.min.js?ver=1.0'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/js/functions.js?ver=1.0'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/js/ads.js?ver=1.0'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/js/jquery.lazy.min.js?ver=1.7.4'></script>
    <script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/themes/colossal-v4/js/lazy-config.js?ver=1.0'></script>
    <link rel='https://api.w.org/' href='https://www.thisiscolossal.com/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.thisiscolossal.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.thisiscolossal.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.2.5" />
    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
  window.MemberfulOptions = {
    site: "https://colossal.memberful.com",
    intercept: [
            "http://www.thisiscolossal.com/?memberful_endpoint=auth",
            "https://www.thisiscolossal.com/?memberful_endpoint=auth",
            "http://www.thisiscolossal.com?memberful_endpoint=auth",
            "https://www.thisiscolossal.com?memberful_endpoint=auth",
          ],
    memberSignedIn: false  };

  (function() {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = 'https://d35xxde4fgg0cx.cloudfront.net/assets/embedded.js';

    setup = function() { window.MemberfulEmbedded.setup(); }

    if(s.addEventListener) {
      s.addEventListener("load", setup, false);
    } else {
      s.attachEvent("onload", setup);
    }

    ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
  })();
</script>


    <meta property="og:locale" content="en_US" />
    <meta property="og:site_name" content="Colossal" />
    <meta property="og:title" content="Colossal" />
    <meta property="og:url" content="https://www.thisiscolossal.com" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Art, design, and visual culture." />
    <meta property="fb:app_id" content="221486471245975" />

    <meta itemprop="name" content="Colossal" />
    <meta itemprop="headline" content="Colossal" />
    <meta itemprop="description" content="Art, design, and visual culture." />

    <meta name="twitter:title" content="Colossal" />
    <meta name="twitter:url" content="https://www.thisiscolossal.com" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@colossal" />






    <script type="68fa48e47a44e50bd58b7be7-text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-325851-30']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'httpswww') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>

    <script type="68fa48e47a44e50bd58b7be7-text/javascript">

        function hide(id) {
            document.getElementById(id).style.visibility = 'hidden';
            event.preventDefault();
        }

    </script>

    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
		var _theframe = document.getElementById("iframe_sidebar");
		_theframe.contentWindow.location.href = https://www.thisiscolossal.com/wp-content/themes/colossal-v4/includes//mailchimp_form_aside.html;
    </script>
</head>
<body class="home blog custom-font-enabled">

<script type="68fa48e47a44e50bd58b7be7-text/javascript">
      if( window.canRunAds === undefined ){
        // adblocker detected, show fallback
        showFallbackImage();
      }
    </script>
<div id="container">
    <div id="header_flex">
        <div id="nectar_head" class="notblocked">

            <div id='div-gpt-ad-1502230835991-0' class='control'>
                <script type="68fa48e47a44e50bd58b7be7-text/javascript">
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1502230835991-0'); });
            </script>
            </div>
            <hr />
        </div>


        <header id="header" class="sticky_header">

            <hr />
            <div id="header_background"></div>
            <div id="header_shop" class="col_02">
                <div id="shop">
                    <div class="col_01"><a href="https://colossalshop.com/" class="shop"></a><span class="caption green">Fun Things for Creative People</span></div>
                    <div class="divider"></div>
                    <div class="col_02">
                        <h2 class="events"><a href="https://www.thisiscolossal.com/members/"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="220px" height="24px" viewBox="0 0 220 24" style="enable-background:new 0 0 220 24;" xml:space="preserve"><path class="st0" d="M24.9,22.1c-0.2,0.1-0.5,0.2-0.7,0.3c-0.2,0.1-0.4,0.1-0.7,0.1c-0.9-0.7-1.4-1.9-1.5-3.7s0.3-3.9,1.1-6.6	c0.1-0.1,0.2-0.2,0.2-0.4L23,11.4c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0.1-0.2,0.3c-0.6,0.5-1.3,0.9-2.1,1.4c-0.7,0.5-1.5,1-2.3,1.4	c-0.8,0.4-1.6,0.8-2.5,1C14.8,15.8,14,16,13.2,16c-2,0-3.5-1-4.6-3.1l-0.3-0.5l-0.3,0.5c-0.1,0.1-0.2,0.3-0.3,0.6	c-0.1,0.3-0.3,0.6-0.4,1c-0.1,0.4-0.3,0.8-0.4,1.2c-0.1,0.4-0.2,0.7-0.1,1c-0.2,0.2-0.4,0.5-0.7,0.8c-0.2,0.3-0.4,0.6-0.5,1	c0,0.2,0,0.4,0.1,0.5c0,0.2-0.2,0.4-0.3,0.5c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0.1-0.2,0.2-0.3,0.3c0,0.1-0.1,0.2-0.1,0.4	c0,0.1,0,0.2-0.1,0.4c0,0.2,0,0.3-0.1,0.5c-0.4,0.8-0.7,1.4-1,1.6c-0.2,0.3-0.6,0.4-1,0.4c-0.2,0-0.5,0-0.9-0.1	c-0.3-0.3-0.6-0.8-0.9-1.4c-0.3-0.6-0.4-1.2-0.6-1.9C0,19.3,0,18.6,0,17.9c0-0.7,0.1-1.3,0.3-1.8c0,0,0.1-0.3,0.3-0.7	c0.2-0.5,0.4-1,0.7-1.8c0.3-0.7,0.6-1.5,1-2.4c0.4-0.9,0.8-1.7,1.2-2.6C4,7.8,4.3,7,4.7,6.3C5,5.6,5.3,5.1,5.6,4.7	c0.2-0.3,0.4-0.6,0.5-0.8c0.2-0.3,0.3-0.5,0.5-0.7C7,2.4,7.4,1.8,7.8,1.3c0.4-0.5,1-0.9,1.8-1.2c0.5,0.2,0.9,0.5,1.2,0.8l0.1,0.4	c0.1,0.2,0.2,0.4,0.4,0.4c0.3,0.1,0.5,0.1,0.7,0.2C12.1,1.9,12.3,2,12.5,2c0.2,0.2,0.3,0.4,0.5,0.7c0.2,0.3,0.4,0.6,0.6,0.9	c0.2,0.3,0.3,0.6,0.4,0.8c0.1,0.3,0.2,0.6,0.2,0.9c0,0.3,0,0.6-0.2,0.9l0,0.4c0.1,0.2,0.1,0.5,0.1,0.9c0,0.2,0,0.3,0,0.5	s0,0.4,0.1,0.6c0.4,0.3,0.8,0.5,1.2,0.5c0.4,0,0.7-0.1,1.1-0.3c0.3-0.2,0.6-0.4,0.9-0.6c0.2-0.1,0.4-0.3,0.6-0.4	c0.2-0.1,0.4-0.2,0.6-0.2c0.4-0.4,0.8-0.8,1.2-1.1c0.4-0.3,0.7-0.6,1.1-0.9c0.7-0.6,1.4-1.2,2.1-1.9c0.7-0.7,1.2-1.5,1.6-2.5	C24.7,1,24.9,1,25.2,1C25.5,1,25.7,1,26,1.2c0.3,0.1,0.6,0.2,0.8,0.4c0.1,0,0.1,0.1,0.2,0.1c0.1,0,0.1,0,0.2,0.1	c0,0.2,0.1,0.4,0.3,0.6c0.1,0.1,0.2,0.1,0.2,0.1c0,0,0.1,0,0.1,0.1c0.4,0.2,0.6,0.4,0.7,0.6c0.2,0.6,0.5,1,0.9,1.3	c0,0.2,0,0.5,0,0.7c0.1,0.2,0.1,0.4,0.2,0.6c0,0.1,0,0.1,0.1,0.2c0,0.1,0.1,0.2,0.1,0.3l-0.7-0.1l0.1,0.7c0,0.3,0,0.7-0.1,1.1	c-0.1,0.4-0.2,0.7-0.4,1c0,0.6,0,1.1,0.1,1.5c0,0.4,0,0.7-0.1,1c0,0.3-0.1,0.6-0.2,0.9c-0.1,0.3-0.2,0.6-0.3,0.9	c0.1,1.5-0.1,3-0.5,4.2c-0.4,1.3-0.9,2.5-1.4,3.6c0,0.1-0.1,0.2-0.2,0.4c-0.1,0.1-0.1,0.2-0.2,0.4c-0.2,0.1-0.4,0.2-0.6,0.2	C25.2,21.9,25.1,22,24.9,22.1z M51,2.3c0,0.2,0,0.3,0,0.4s0,0.2,0,0.2c0,0.1,0,0.2,0,0.2c0,0.1,0,0.2,0.1,0.4	c-0.5,0.8-1.1,1.4-1.8,1.7c-0.7,0.3-1.4,0.5-2.2,0.7c-0.8,0.1-1.6,0.3-2.4,0.4c-0.8,0.1-1.6,0.5-2.4,0.9c-0.3-0.1-0.6-0.1-0.9,0	c-0.3,0.1-0.6,0.2-1.1,0.1c-0.1,0.1-0.3,0.2-0.5,0.3c-0.2,0.1-0.4,0.1-0.7,0.2c0.1,0.2,0.1,0.4,0.1,0.7c0,0.2,0,0.4,0.1,0.6	c0.8,0.1,1.8,0.1,2.8,0c1-0.1,2-0.2,3-0.2c0,0.1,0,0.2,0,0.2c0,0-0.1,0.1,0,0.2c0.2,0.1,0.3,0.3,0.5,0.5c0.2,0.2,0.3,0.4,0.5,0.5	c0.2,0.2,0.3,0.3,0.5,0.4c0.2,0.1,0.4,0.2,0.6,0.1c0.1,0.2,0.2,0.4,0.2,0.6c0,0.2,0,0.5-0.1,0.7c-0.4,0.3-0.8,0.5-1.2,0.8	c-0.4,0.3-0.8,0.5-1.2,0.7c-0.4,0.2-0.9,0.4-1.3,0.5s-1,0.2-1.6,0.2c-0.1,0-0.2,0-0.3,0s-0.1,0-0.2-0.1c-0.2,0.1-0.5,0.2-0.8,0.1	c-0.4,0-0.6,0-0.8,0.1c-0.3-0.1-0.6-0.2-1-0.2c-0.4,0-0.7,0-1-0.1c-0.4,0.5-0.7,1.1-1.1,1.6c-0.3,0.6-0.6,1.2-0.8,1.8	c0.8,0.1,1.5,0.2,2,0.1c0.5,0,1.1-0.1,1.5-0.1c0.5-0.1,1-0.1,1.5-0.2c0.5-0.1,1.1-0.1,1.9-0.1c0.5-0.3,1.1-0.4,1.7-0.5	c0.7-0.1,1.3-0.2,1.8-0.4c0.2,0.1,0.4,0.2,0.5,0.4c0.1,0.2,0.3,0.4,0.4,0.5c0.1,0.2,0.3,0.3,0.4,0.4c0.1,0.1,0.3,0.2,0.5,0.1	c0.1,0.1,0.2,0.2,0.2,0.3c0,0.1,0.1,0.3,0.1,0.4c0,0.1,0.1,0.2,0.1,0.3c0.1,0.1,0.2,0.1,0.3,0.1c-0.4,0.8-0.8,1.4-1.2,1.8	c-0.5,0.4-1,0.7-1.6,1c-0.6,0.2-1.3,0.4-2,0.5c-0.7,0.1-1.6,0.3-2.4,0.4c-0.2,0-0.4,0-0.5,0c-0.1-0.1-0.3-0.1-0.5-0.1	c-0.6,0.1-1.2,0.2-2,0.3c-0.8,0.1-1.6,0.1-2.4,0.1c-0.8,0-1.6-0.1-2.3-0.3s-1.3-0.5-1.8-1c-0.2-0.2-0.3-0.4-0.4-0.6	c-0.1-0.2-0.2-0.4-0.5-0.5c0-0.3-0.1-0.7-0.1-1.1c-0.1-0.4-0.1-0.8-0.2-1.2c-0.1-0.4-0.1-0.8-0.2-1.2s0-0.8,0.1-1.1	c0.1-0.4,0.2-0.7,0.5-1c0.2-0.3,0.3-0.6,0.2-0.9c0.1,0,0.2-0.1,0.2-0.2c0-0.1,0.1-0.2,0.3-0.1c0.1-0.4,0.3-0.8,0.5-1.2	c0.2-0.4,0.3-0.8,0.5-1.1c0.2-0.4,0.4-0.7,0.5-1.1c0.2-0.4,0.4-0.8,0.5-1.2c-0.3-0.3-0.6-0.7-0.7-1c-0.2-0.4-0.3-0.7-0.3-1.1	s-0.1-0.8-0.2-1.2c0-0.4-0.1-0.8-0.3-1.2c0.2-0.4,0.5-0.7,0.9-0.9c0.3-0.2,0.7-0.4,1.1-0.6c0.4-0.1,0.9-0.3,1.3-0.4	c0.5-0.1,1-0.2,1.5-0.3c1-0.3,1.8-0.6,2.6-0.7c0.8-0.2,1.5-0.3,2.2-0.4c0.7-0.1,1.5-0.2,2.3-0.3s1.7-0.2,2.7-0.4	C47.9,0.9,48.1,1,48.3,1c0.1-0.2,0.2-0.3,0.3-0.2s0.2,0,0.2-0.2c0.2,0.4,0.5,0.8,0.9,1C50.1,1.9,50.5,2.1,51,2.3z M75.9,22.1	c-0.2,0.1-0.5,0.2-0.7,0.3c-0.2,0.1-0.4,0.1-0.7,0.1c-0.9-0.7-1.4-1.9-1.5-3.7s0.3-3.9,1.1-6.6c0.1-0.1,0.2-0.2,0.2-0.4L74,11.4	c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0.1-0.2,0.3c-0.6,0.5-1.3,0.9-2.1,1.4c-0.7,0.5-1.5,1-2.3,1.4c-0.8,0.4-1.6,0.8-2.5,1	C65.9,15.8,65,16,64.2,16c-2,0-3.5-1-4.6-3.1l-0.3-0.5l-0.3,0.5c-0.1,0.1-0.2,0.3-0.3,0.6c-0.1,0.3-0.3,0.6-0.4,1	c-0.1,0.4-0.3,0.8-0.4,1.2c-0.1,0.4-0.2,0.7-0.1,1c-0.2,0.2-0.4,0.5-0.7,0.8c-0.2,0.3-0.4,0.6-0.5,1c0,0.2,0,0.4,0.1,0.5	c0,0.2-0.2,0.4-0.3,0.5c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0.1-0.2,0.2-0.3,0.3c0,0.1-0.1,0.2-0.1,0.4c0,0.1,0,0.2-0.1,0.4	c0,0.2,0,0.3-0.1,0.5c-0.4,0.8-0.7,1.4-1,1.6c-0.2,0.3-0.6,0.4-1,0.4c-0.2,0-0.5,0-0.9-0.1c-0.3-0.3-0.6-0.8-0.9-1.4	c-0.3-0.6-0.4-1.2-0.6-1.9c-0.1-0.7-0.2-1.4-0.2-2c0-0.7,0.1-1.3,0.3-1.8c0,0,0.1-0.3,0.3-0.7c0.2-0.5,0.4-1,0.7-1.8	c0.3-0.7,0.6-1.5,1-2.4c0.4-0.9,0.8-1.7,1.2-2.6C55,7.8,55.4,7,55.7,6.3c0.4-0.7,0.6-1.2,0.9-1.6c0.2-0.3,0.4-0.6,0.5-0.8	c0.2-0.3,0.3-0.5,0.5-0.7c0.4-0.7,0.9-1.4,1.2-1.9c0.4-0.5,1-0.9,1.8-1.2c0.5,0.2,0.9,0.5,1.2,0.8l0.1,0.4c0.1,0.2,0.2,0.4,0.4,0.4	c0.3,0.1,0.5,0.1,0.7,0.2C63.1,1.9,63.3,2,63.5,2c0.2,0.2,0.3,0.4,0.5,0.7c0.2,0.3,0.4,0.6,0.6,0.9c0.2,0.3,0.3,0.6,0.4,0.8	c0.1,0.3,0.2,0.6,0.2,0.9c0,0.3,0,0.6-0.2,0.9l0,0.4c0.1,0.2,0.1,0.5,0.1,0.9c0,0.2,0,0.3,0,0.5s0,0.4,0.1,0.6	c0.4,0.3,0.8,0.5,1.2,0.5c0.4,0,0.7-0.1,1.1-0.3c0.3-0.2,0.6-0.4,0.9-0.6c0.2-0.1,0.4-0.3,0.6-0.4c0.2-0.1,0.4-0.2,0.6-0.2	c0.4-0.4,0.8-0.8,1.2-1.1c0.4-0.3,0.7-0.6,1.1-0.9c0.7-0.6,1.4-1.2,2.1-1.9c0.7-0.7,1.2-1.5,1.6-2.5C75.7,1,76,1,76.2,1	c0.3,0,0.5,0.1,0.8,0.2c0.3,0.1,0.6,0.2,0.8,0.4c0.1,0,0.1,0.1,0.2,0.1c0.1,0,0.1,0,0.2,0.1c0,0.2,0.1,0.4,0.3,0.6	c0.1,0.1,0.2,0.1,0.2,0.1c0,0,0.1,0,0.1,0.1c0.4,0.2,0.6,0.4,0.7,0.6c0.2,0.6,0.5,1,0.9,1.3c0,0.2,0,0.5,0,0.7	c0.1,0.2,0.1,0.4,0.2,0.6c0,0.1,0,0.1,0.1,0.2c0,0.1,0.1,0.2,0.1,0.3l-0.7-0.1l0.1,0.7c0,0.3,0,0.7-0.1,1.1c-0.1,0.4-0.2,0.7-0.4,1	c0,0.6,0,1.1,0.1,1.5c0,0.4,0,0.7-0.1,1c0,0.3-0.1,0.6-0.2,0.9c-0.1,0.3-0.2,0.6-0.3,0.9c0.1,1.5-0.1,3-0.5,4.2	c-0.4,1.3-0.9,2.5-1.4,3.6c0,0.1-0.1,0.2-0.2,0.4c-0.1,0.1-0.1,0.2-0.2,0.4c-0.2,0.1-0.4,0.2-0.6,0.2C76.3,21.9,76.1,22,75.9,22.1z	 M103.2,16.8c0.1,0.1,0.2,0.2,0.3,0.4c0,0.2,0,0.3,0,0.4c-0.4,0.6-0.7,0.9-0.9,1.2c-0.2,0.2-0.3,0.4-0.4,0.5	c-0.1,0.1-0.2,0.2-0.2,0.3c-0.1,0.1-0.3,0.2-0.6,0.5c-1.1,0.7-2.2,1.2-3.4,1.7s-2.3,0.9-3.6,1.1c-0.2,0.3-0.6,0.5-1.1,0.6	c-0.5,0.1-1,0.2-1.5,0.3c-0.1,0-0.2-0.1-0.2-0.2c0-0.1-0.1-0.2-0.2-0.2c0,0.2-0.1,0.4-0.3,0.4c-0.2,0-0.4,0-0.7-0.1	c-0.3-0.1-0.6-0.3-0.9-0.4c-0.3-0.2-0.6-0.3-0.8-0.5c-0.1,0.2-0.2,0.4-0.4,0.6c-0.1,0.2-0.3,0.3-0.5,0.4c-0.3,0-0.5-0.1-0.6-0.1	c-0.1,0-0.3,0-0.4,0.1c-0.1-0.1-0.3-0.2-0.3-0.3c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.2-0.2-0.3-0.2c-0.1-0.1-0.3-0.1-0.5-0.2	c0-0.1,0-0.2-0.1-0.5c-0.1-0.2-0.2-0.3-0.4-0.4c0.1-0.2,0.1-0.4-0.1-0.6c-0.2-0.3-0.3-0.5-0.4-0.9c0-0.3,0.1-0.6,0.1-0.9	c0-0.3,0.2-0.5,0.4-0.5c0-0.2-0.1-0.3-0.1-0.4c0-0.1-0.1-0.2-0.1-0.4c0.2-0.1,0.3-0.3,0.4-0.5c0.1-0.2,0.1-0.4,0.2-0.6	c0-0.2,0.1-0.4,0.2-0.7c0.1-0.2,0.2-0.4,0.4-0.5c0.2-0.7,0.4-1.3,0.5-1.6c0.1-0.3,0.2-0.7,0.3-1c0.1-0.3,0.1-0.7,0.2-1.1	c0.1-0.4,0.2-0.9,0.5-1.4c0-0.4,0-0.7,0.1-1c0.1-0.3,0.2-0.6,0.3-0.8c0.1-0.3,0.2-0.6,0.3-0.9c0.1-0.3,0.1-0.7,0-1.1	c-0.3,0-0.6,0.1-0.7,0.3C87.2,7.4,87.1,7.6,87,8c-0.2,0.2-0.4,0.3-0.7,0.3c-0.3,0-0.5,0-0.7,0.1C84.7,8,84,7.5,83.5,6.9	c-0.6-0.6-1-1.4-1.3-2.2c0.2-0.6,0.5-1,1-1.4c0.4-0.4,0.9-0.7,1.5-0.9c0.6-0.3,1.1-0.5,1.8-0.6c0.6-0.2,1.2-0.3,1.8-0.5	c0.9-0.2,1.9-0.4,2.8-0.6c0.9-0.2,2-0.3,3-0.2c0.1-0.1,0.3-0.1,0.5-0.1c0.2,0,0.4,0,0.6,0c0.2,0,0.4,0,0.6,0c0.2,0,0.4-0.1,0.5-0.1	c0.1,0.1,0.3,0.2,0.5,0.3c0.2,0,0.5,0.2,0.7,0.3c0.3-0.2,0.5-0.3,0.7-0.1C98.6,0.8,98.8,0.9,99,1c0.2,0.1,0.3,0.3,0.5,0.4	c0.2,0.1,0.3,0.2,0.5,0.2c0.2,0.2,0.4,0.4,0.6,0.6c0.3,0.2,0.5,0.4,0.7,0.6c0.2,0.2,0.4,0.4,0.6,0.7s0.2,0.5,0.2,0.8	c0,0.1,0,0.2,0.1,0.2c0.1,0,0.2,0.1,0.2,0.1c0,0.7-0.1,1.4-0.3,1.9c-0.3,0.5-0.6,1-0.9,1.5c-0.4,0.5-0.8,0.9-1.2,1.3	c-0.4,0.4-0.9,0.8-1.2,1.3c0.2,0.3,0.5,0.6,0.8,0.8c0.3,0.2,0.6,0.5,1,0.7c0.3,0.2,0.6,0.5,0.9,0.8c0.3,0.3,0.5,0.6,0.7,0.9	c0,0,0.1,0.1,0.2,0.1s0.1,0.1,0.2,0.1c0.1,0.2,0.2,0.5,0.3,0.7c0.1,0.3,0.2,0.5,0.3,0.7c0.1,0.2,0.1,0.4,0.2,0.6	C103.3,16.5,103.3,16.7,103.2,16.8z M96.8,4.7c-1.2,0-2.5,0.1-3.7,0.3c-1.2,0.3-2.2,0.6-3.1,1.1c0.4,0.3,0.8,0.6,1.2,0.9	c0.4,0.3,0.8,0.6,1,1.1c0.9-0.4,1.8-0.9,2.5-1.5C95.5,6.1,96.2,5.4,96.8,4.7z M97.3,16.3c0.1-0.2,0.2-0.4,0.4-0.6	c0.2-0.1,0.4-0.3,0.6-0.4c0.2-0.1,0.4-0.3,0.5-0.4c0.1-0.2,0.2-0.4,0.2-0.7c-0.8-0.4-1.5-0.7-2.1-0.7c-0.6-0.1-1.2-0.1-1.8,0	c-0.6,0.1-1.1,0.2-1.6,0.4c-0.5,0.2-1,0.3-1.6,0.4c-0.3,0.7-0.5,1.5-0.8,2.3c-0.3,0.8-0.6,1.6-0.8,2.4c0.7-0.1,1.4-0.2,2-0.5	c0.6-0.2,1.2-0.4,1.8-0.7s1.1-0.5,1.7-0.7C96.1,16.7,96.7,16.5,97.3,16.3z M125.5,2.3c0,0.2,0,0.3,0,0.4c0,0.1,0,0.2,0,0.2	c0,0.1,0,0.2,0,0.2c0,0.1,0,0.2,0.1,0.4c-0.5,0.8-1.1,1.4-1.8,1.7c-0.7,0.3-1.4,0.5-2.2,0.7c-0.8,0.1-1.6,0.3-2.4,0.4	c-0.8,0.1-1.6,0.5-2.4,0.9c-0.3-0.1-0.6-0.1-0.9,0c-0.3,0.1-0.6,0.2-1.1,0.1c-0.1,0.1-0.3,0.2-0.5,0.3c-0.2,0.1-0.4,0.1-0.7,0.2	c0.1,0.2,0.1,0.4,0.1,0.7c0,0.2,0,0.4,0.1,0.6c0.8,0.1,1.8,0.1,2.8,0c1-0.1,2-0.2,3-0.2c0,0.1,0,0.2,0,0.2c0,0-0.1,0.1,0,0.2	c0.2,0.1,0.3,0.3,0.5,0.5c0.2,0.2,0.3,0.4,0.5,0.5c0.2,0.2,0.3,0.3,0.5,0.4c0.2,0.1,0.4,0.2,0.6,0.1c0.1,0.2,0.2,0.4,0.2,0.6	c0,0.2,0,0.5-0.1,0.7c-0.4,0.3-0.8,0.5-1.2,0.8c-0.4,0.3-0.8,0.5-1.2,0.7c-0.4,0.2-0.9,0.4-1.3,0.5c-0.5,0.1-1,0.2-1.6,0.2	c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.2-0.1c-0.2,0.1-0.5,0.2-0.8,0.1c-0.4,0-0.6,0-0.8,0.1c-0.3-0.1-0.6-0.2-1-0.2	c-0.4,0-0.7,0-1-0.1c-0.4,0.5-0.7,1.1-1.1,1.6c-0.3,0.6-0.6,1.2-0.8,1.8c0.8,0.1,1.5,0.2,2,0.1c0.5,0,1.1-0.1,1.5-0.1	c0.5-0.1,1-0.1,1.5-0.2c0.5-0.1,1.1-0.1,1.9-0.1c0.5-0.3,1.1-0.4,1.7-0.5c0.7-0.1,1.3-0.2,1.8-0.4c0.2,0.1,0.4,0.2,0.5,0.4	c0.1,0.2,0.3,0.4,0.4,0.5c0.1,0.2,0.3,0.3,0.4,0.4c0.1,0.1,0.3,0.2,0.5,0.1c0.1,0.1,0.2,0.2,0.2,0.3c0,0.1,0.1,0.3,0.1,0.4	c0,0.1,0.1,0.2,0.1,0.3c0.1,0.1,0.2,0.1,0.3,0.1c-0.4,0.8-0.8,1.4-1.2,1.8c-0.5,0.4-1,0.7-1.6,1c-0.6,0.2-1.3,0.4-2,0.5	c-0.7,0.1-1.6,0.3-2.4,0.4c-0.2,0-0.4,0-0.5,0c-0.1-0.1-0.3-0.1-0.5-0.1c-0.6,0.1-1.2,0.2-2,0.3c-0.8,0.1-1.6,0.1-2.4,0.1	c-0.8,0-1.6-0.1-2.3-0.3s-1.3-0.5-1.8-1c-0.2-0.2-0.3-0.4-0.4-0.6c-0.1-0.2-0.2-0.4-0.5-0.5c0-0.3-0.1-0.7-0.1-1.1	c-0.1-0.4-0.1-0.8-0.2-1.2c-0.1-0.4-0.1-0.8-0.2-1.2c0-0.4,0-0.8,0.1-1.1c0.1-0.4,0.2-0.7,0.5-1c0.2-0.3,0.3-0.6,0.2-0.9	c0.1,0,0.2-0.1,0.2-0.2c0-0.1,0.1-0.2,0.3-0.1c0.1-0.4,0.3-0.8,0.5-1.2c0.2-0.4,0.3-0.8,0.5-1.1c0.2-0.4,0.4-0.7,0.5-1.1	c0.2-0.4,0.4-0.8,0.5-1.2c-0.3-0.3-0.6-0.7-0.7-1c-0.2-0.4-0.3-0.7-0.3-1.1s-0.1-0.8-0.2-1.2c0-0.4-0.1-0.8-0.3-1.2	c0.2-0.4,0.5-0.7,0.9-0.9c0.3-0.2,0.7-0.4,1.1-0.6c0.4-0.1,0.9-0.3,1.3-0.4c0.5-0.1,1-0.2,1.5-0.3c1-0.3,1.8-0.6,2.6-0.7	c0.8-0.2,1.5-0.3,2.2-0.4c0.7-0.1,1.5-0.2,2.3-0.3s1.7-0.2,2.7-0.4c0.2,0.2,0.4,0.3,0.6,0.3c0.1-0.2,0.2-0.3,0.3-0.2	c0.1,0.1,0.2,0,0.2-0.2c0.2,0.4,0.5,0.8,0.9,1C124.6,1.9,125,2.1,125.5,2.3z M131.6,18.9c-0.3,1-0.5,1.7-0.7,2.2	c-0.2,0.5-0.5,1-0.9,1.5c-0.3,0-0.5,0-0.7,0.1c-0.1,0.1-0.3,0.2-0.4,0.3c-0.2,0-0.4,0-0.5,0c-0.1,0-0.2-0.1-0.3-0.2	c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.3-0.1-0.5-0.1c0-0.2-0.1-0.4-0.2-0.5c-0.1-0.2-0.2-0.2-0.5-0.2c0-0.2,0.1-0.4,0-0.5	c0-0.1-0.1-0.3-0.1-0.4c0-0.1-0.1-0.2-0.1-0.3c0-0.1,0-0.2,0-0.4c-0.1,0-0.2-0.1-0.3-0.3c-0.1-0.2-0.2-0.3-0.3-0.5	c0.1-0.4,0.1-0.8,0.2-1.2c0.1-0.4,0.2-0.7,0.3-1.1s0.2-0.7,0.3-1c0.1-0.3,0-0.7-0.1-1.1c0.1,0.1,0.2,0.1,0.2-0.1	c0-0.1,0.1-0.3,0.1-0.4c0-0.2,0-0.4,0-0.5s0-0.3,0-0.5c0.2-0.6,0.4-1.3,0.6-1.9s0.3-1.3,0.4-2c0.1-0.7,0.3-1.3,0.4-1.9	c0.1-0.6,0.3-1.2,0.6-1.7c0.3,0,0.6,0,0.9,0.1c0.3,0.1,0.5,0.2,0.6,0.4c0.6-0.5,1.2-1.1,1.7-1.8c0.5-0.7,1-1.4,1.5-2	c0.5-0.7,1.1-1.2,1.7-1.7c0.6-0.5,1.4-0.8,2.3-0.9c0.5,0.4,1,0.7,1.6,1.1c0.6,0.4,1.1,0.9,1.5,1.5c0.2,0.1,0.4,0.2,0.5,0.3	c0.1,0.1,0.3,0.2,0.5,0.3v0.4c0.3,0.3,0.5,0.6,0.7,0.9c0.2,0.4,0.3,0.7,0.5,1.1c0.1,0.4,0.3,0.8,0.4,1.2c0.1,0.4,0.3,0.8,0.5,1.1	c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.2,0.1,0.3c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0,0.2,0,0.4c-0.1,1.1-0.4,1.9-1,2.6	c-0.6,0.7-1.4,1.2-2.2,1.6c-0.9,0.4-1.8,0.8-2.8,1.1c-1,0.3-1.9,0.6-2.7,0.9c0,0.1,0,0.1,0.1,0.1c0.1,0,0.1,0.1,0.1,0.2	c0.6,0.4,1,0.7,1.4,0.9c0.3,0.2,0.7,0.4,1,0.5c0.3,0.1,0.6,0.3,1,0.4c0.3,0.1,0.8,0.2,1.5,0.4c0.6,0.1,1.1,0.3,1.5,0.3	c0.3,0.1,0.7,0.2,1,0.2c0.3,0.1,0.6,0.1,0.9,0.1c0.3,0,0.7,0.1,1.3,0.1c0.2,0.2,0.4,0.5,0.6,0.8c0.2,0.3,0.3,0.6,0.3,0.9	c0,0.3,0.1,0.6,0,0.9c0,0.3-0.1,0.5-0.3,0.7c0,0.1,0.1,0.1,0.2,0.1c0.1,0,0.2,0,0.3,0c-0.1,0.1-0.3,0.2-0.4,0.4	c-0.1,0.1-0.2,0.3-0.3,0.4c-0.1,0.1-0.2,0.3-0.3,0.4c-0.1,0.1-0.3,0.2-0.5,0.2c-0.7,0.3-1.7,0.3-2.8,0.1c-1.1-0.3-2.6-0.7-4.3-1.4	c-0.9-0.3-1.6-0.6-2.1-0.9s-1-0.5-1.4-0.8c-0.4-0.3-0.8-0.5-1.1-0.8C132.4,19.7,132,19.4,131.6,18.9z M136.7,6.5	c-0.3,0.4-0.6,0.7-0.9,1.1c-0.3,0.3-0.6,0.6-0.9,1c-0.3,0.3-0.6,0.7-0.8,1.1c-0.2,0.4-0.4,0.9-0.5,1.4c0.8-0.1,1.7-0.3,2.6-0.7	c0.9-0.4,1.8-0.7,2.6-1.1c-0.1-0.7-0.4-1.4-0.8-1.8C137.6,7,137.2,6.7,136.7,6.5z M147.1,6.4c0.2-0.5,0.4-0.9,0.8-1.3	c0.4-0.4,0.8-0.7,1.2-0.9c0.4-0.3,0.9-0.5,1.4-0.6c0.5-0.2,0.9-0.3,1.4-0.4c1-0.4,2-0.8,3.1-1.2c1.1-0.4,2.2-0.7,3.4-1	c1.1-0.3,2.3-0.5,3.5-0.6c1.2-0.1,2.5,0.1,3.8,0.5c0.1,0.2,0.2,0.5,0.3,0.7c0.1,0.2,0.3,0.4,0.5,0.7c0.2,0.2,0.3,0.4,0.4,0.6	c0.1,0.2,0.2,0.4,0.2,0.6c0.2,0.2,0.4,0.5,0.6,0.7c0.1,0.3,0.2,0.6,0.1,1c-0.2,0.2-0.5,0.4-0.7,0.5c-0.2,0.2-0.5,0.3-0.7,0.4	c-0.9-0.2-1.8-0.3-2.7-0.2c-0.9,0.1-1.7,0.1-2.5,0.2c-0.2-0.2-0.5-0.3-0.8-0.3c-0.3,0-0.6,0-0.9,0.1c-0.3,0.1-0.7,0.2-1,0.3	c-0.3,0.1-0.6,0.1-0.9,0.1c-0.2,0.2-0.5,0.3-0.9,0.4c-0.4,0.1-0.7,0.1-1.1,0.2c0,0.1,0,0.2,0.1,0.2c0.1,0,0.1,0.1,0,0.2	c0.1,0.2,0.3,0.3,0.6,0.4c0.2,0.1,0.5,0.1,0.7,0.2s0.5,0.2,0.7,0.3c0.2,0.1,0.3,0.3,0.4,0.5c0.4,0,0.7,0.1,0.9,0.3	c0.3,0.2,0.5,0.4,0.7,0.6c0.2,0.2,0.3,0.5,0.4,0.8c0.1,0.3,0.1,0.5,0.1,0.7c0.1,0.2,0.2,0.3,0.3,0.3c0.1,0,0.2,0,0.3,0.1	c0.1,0.3,0.3,0.6,0.5,1c0.3,0.3,0.5,0.7,0.7,1.1c0,0.6,0.1,1.1,0.1,1.6c0,0.5-0.1,0.9-0.3,1.4c-0.4,0.9-0.9,1.7-1.5,2.4	c-0.6,0.7-1.4,1.3-2.2,1.9c-0.8,0.5-1.7,1-2.6,1.4c-0.9,0.4-1.8,0.7-2.7,1c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.1-0.2	c-0.2,0.2-0.4,0.3-0.6,0.3c-0.2,0-0.5,0.1-0.7,0.1c-0.3,0-0.5,0-0.8-0.1c-0.3,0-0.5,0-0.8,0c-0.2-0.2-0.3-0.3-0.5-0.5	c-0.1-0.2-0.2-0.4-0.3-0.6c-0.1,0-0.2-0.1-0.2-0.2c0-0.1,0-0.2-0.1-0.2c0.1-0.2,0.2-0.4,0.2-0.4c0,0,0-0.1-0.1-0.1	c-0.1,0-0.1-0.1-0.2-0.1c0,0-0.1-0.1,0-0.2c0.1-0.2,0.1-0.4,0.1-0.7c0-0.3,0-0.5-0.1-0.8c0-0.3,0-0.5,0.1-0.8	c0.1-0.2,0.2-0.4,0.5-0.6c0.8,0.1,1.6,0.1,2.3-0.1c0.7-0.2,1.4-0.4,2.1-0.7c0.7-0.3,1.3-0.6,1.9-0.9c0.6-0.3,1.2-0.6,1.8-0.9	c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.3-0.3,0.4-0.4c0.1-0.1,0.3-0.3,0.4-0.4c0.1-0.1,0.2-0.3,0.3-0.5c0-0.2,0-0.3-0.1-0.3	c-0.1,0-0.2-0.1-0.2-0.3l-0.4-0.1c-0.5-0.3-1-0.5-1.6-0.7c-0.6-0.2-1.1-0.4-1.5-0.6c-0.3,0.1-0.5,0.2-0.7,0.1	c-0.2-0.1-0.5-0.1-0.9-0.2c-0.8,0-1.6-0.1-2.4-0.3c-0.8-0.1-1.5-0.3-2.2-0.7c0-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.2-0.1-0.4-0.1	c-0.1-0.2-0.3-0.4-0.4-0.6c-0.1-0.2-0.3-0.4-0.4-0.6c0-0.3,0.1-0.5,0.1-0.7c0-0.2,0-0.4-0.2-0.6c0.1-0.3,0.2-0.6,0.2-0.8	C147.2,7,147.2,6.7,147.1,6.4z M185.3,19.2c0,0.6,0,1.1-0.2,1.6c-0.2,0.4-0.5,0.8-0.8,1.1c-0.3,0.3-0.7,0.5-1.2,0.7	c-0.4,0.2-0.9,0.3-1.3,0.5c-0.2-0.1-0.4-0.2-0.6-0.4c-0.2-0.1-0.4-0.2-0.6-0.3c-0.3-0.6-0.6-1.1-0.9-1.5c-0.3-0.4-0.4-1-0.4-1.5	c0,0,0-0.2,0.1-0.4c0-0.2,0.1-0.5,0.1-0.8c0.1-0.3,0.1-0.6,0.1-0.8c0-0.3,0.1-0.4,0.1-0.4c-0.6,0-1.1,0.2-1.7,0.4	c-0.6,0.2-1.2,0.4-1.8,0.7c-0.6,0.3-1.1,0.5-1.6,0.8c-0.5,0.3-0.9,0.5-1.2,0.7c0,0.1,0,0.2-0.1,0.5c-0.1,0.3-0.2,0.6-0.4,0.9	c-0.1,0.3-0.3,0.6-0.5,0.8c-0.2,0.2-0.3,0.4-0.5,0.4c0,0.1,0.1,0.1,0.1,0.2c0,0.1,0,0.2,0,0.3c-0.3,0.1-0.6,0.2-0.9,0.4	c-0.3,0.2-0.5,0.3-0.8,0.4c-0.3,0.1-0.6,0.2-0.9,0.2c-0.3,0-0.7-0.1-1.2-0.4c0-0.1-0.1-0.3-0.2-0.4c-0.1-0.1-0.2-0.2-0.4-0.3	c-0.1-0.1-0.2-0.2-0.3-0.2c-0.1-0.1-0.1-0.1-0.1-0.2c-0.1,0-0.2-0.1-0.1-0.3c0-0.2,0.1-0.2,0.3-0.1c-0.3-0.3-0.4-0.6-0.5-0.9	c-0.1-0.3-0.2-0.6-0.4-1c0,0,0.1-0.1,0.1-0.3c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0.1-0.2,0.1-0.2c0,0,0.1,0,0.2,0c0-0.2,0-0.4,0-0.5	s0.1-0.3,0.1-0.4c0-0.1,0.1-0.2,0.1-0.4c0-0.1,0-0.3-0.1-0.4c0.4-0.5,0.6-1.1,0.8-1.7c0.1-0.6,0.2-1.2,0.2-1.7	c0.2-0.2,0.3-0.4,0.5-0.7c0.1-0.3,0.2-0.6,0.3-1c0.1-0.3,0.1-0.6,0.1-0.9c0-0.3-0.1-0.5-0.1-0.7c0-0.1,0.2-0.2,0.4-0.1	c0.1-0.8,0.2-1.7,0.3-2.7c0.1-1,0.2-2,0.3-2.9c0.1-0.9,0.4-1.7,0.7-2.4c0.3-0.7,0.9-1.1,1.6-1.4c0.1,0.5,0.3,1,0.6,1.3	c0.2,0.4,0.5,0.8,0.8,1.1c0.3,0.4,0.6,0.7,0.8,1.1c0.3,0.4,0.5,0.8,0.8,1.2c0,0.5-0.1,1-0.1,1.6s-0.2,1.2-0.3,1.8	c-0.1,0.6-0.2,1.2-0.3,1.8c-0.1,0.6-0.2,1.1-0.3,1.5c0,0.1,0.1,0.2,0.2,0.2c0.1,0,0.1,0.1,0.1,0.3c0.6-0.1,1.2-0.3,1.8-0.4	c0.6-0.1,1.3-0.3,1.9-0.4c0.6-0.1,1.2-0.3,1.8-0.5s1.1-0.4,1.6-0.7c0.1-0.9,0.3-1.9,0.5-2.8c0.2-0.9,0.2-1.9,0.1-2.9	c0.1-0.1,0.2-0.2,0.3-0.4c0.1-0.1,0.2-0.2,0.4-0.3c-0.1-0.5-0.2-0.9-0.1-1.4c0-0.5,0.1-1,0.3-1.4c0.2-0.5,0.4-0.9,0.7-1.2	c0.3-0.3,0.7-0.5,1.1-0.6c0.3,0.4,0.5,0.8,0.7,1.2c0.2,0.4,0.4,0.9,0.6,1.3c0.2,0.4,0.4,0.9,0.7,1.3c0.2,0.4,0.5,0.7,0.9,1	c0.1,0.7,0.1,1.4,0.2,2.2c0,0.7,0.1,1.5,0.1,2.3c-0.2,0.9-0.4,1.8-0.6,2.7c-0.2,0.9-0.4,1.8-0.5,2.7c-0.2,0.2-0.3,0.4-0.4,0.7	c-0.1,0.2-0.2,0.5-0.3,0.7c-0.1,0.3-0.2,0.5-0.3,0.7c-0.1,0.2-0.3,0.4-0.4,0.6c0.1,0.3,0.1,0.6,0.1,0.8	C185,18.5,185.1,18.8,185.3,19.2z M196,20.2c0.1,0.7,0.1,1.3-0.1,1.7c-0.2,0.4-0.5,0.7-0.9,0.9c-0.4,0.2-0.8,0.3-1.3,0.3	c-0.5,0-0.9,0-1.4-0.1c-0.2-0.3-0.5-0.8-0.7-1.3c-0.2-0.5-0.4-1-0.5-1.6c-0.2-0.5-0.3-1.1-0.5-1.6c-0.1-0.5-0.2-1-0.3-1.4	c0.1-0.3,0.2-0.6,0.2-1c0-0.4,0.1-0.8,0.1-1.1c0-0.4,0-0.8,0-1.2c0-0.4,0-0.8,0.1-1.1c0.2-1,0.4-2,0.7-2.9c0.3-0.9,0.6-1.9,0.8-2.9	c0-0.2,0-0.4-0.1-0.6c-0.1-0.2-0.1-0.4-0.1-0.5c0-0.1,0.1-0.2,0.2-0.2c0.1,0,0.2-0.1,0.2-0.2c0.1-0.3,0.1-0.5,0.1-0.7	c0-0.2,0-0.4,0.1-0.6c0.1-0.2,0.3-0.5,0.5-0.9c0.2-0.4,0.2-0.8,0.2-1.2c0.2-0.1,0.4-0.3,0.6-0.5s0.4-0.4,0.6-0.5	c0.2-0.2,0.4-0.3,0.7-0.4c0.3-0.1,0.6-0.2,0.9-0.2c0.2,0.2,0.3,0.5,0.4,0.8c0.1,0.3,0.1,0.6,0.2,0.9c0.1,0.3,0.2,0.6,0.3,0.8	c0.1,0.3,0.3,0.5,0.6,0.6c0,0.2,0,0.4,0,0.7c0,0.3,0,0.6,0,0.9c0,0.3,0.1,0.5,0.2,0.8c0.1,0.2,0.2,0.4,0.4,0.5	c-0.4,1.2-0.8,2.3-1.2,3.4c-0.4,1.1-0.7,2.3-0.9,3.4c-0.2,1.1-0.4,2.3-0.5,3.5C195.7,17.8,195.8,19,196,20.2z M213.1,0.3	c1,0.1,1.9,0.3,2.7,0.6s1.5,0.7,2,1.2c0.4,0.6,0.8,1.3,1.1,2s0.6,1.4,0.8,2.2s0.3,1.5,0.3,2.3c0,0.8-0.2,1.6-0.5,2.4	c-0.7,0.9-1.6,1.6-2.6,2.1c-1,0.5-2.1,1-3.2,1.3c-1.2,0.3-2.4,0.6-3.6,0.9c-1.2,0.2-2.5,0.5-3.6,0.7c0,0.6,0,1.1,0,1.8	c0,0.6-0.1,1.2-0.2,1.8c-0.1,0.6-0.3,1.2-0.5,1.8c-0.2,0.6-0.5,1.1-0.9,1.6c-0.4,0-0.8,0.1-1.3,0.2c-0.4,0.1-0.8,0.2-1.2,0.3	c-0.2-0.1-0.3-0.2-0.5-0.3c-0.1-0.1-0.3-0.1-0.6-0.2c0-0.2,0-0.5-0.2-0.7c-0.2-0.3-0.3-0.5-0.5-0.8c-0.2-0.3-0.3-0.5-0.3-0.8	c-0.1-0.2,0.1-0.5,0.3-0.7c-0.2-0.2-0.3-0.4-0.3-0.7c0.2-0.3,0.3-0.7,0.5-1.2c0.1-0.5,0.2-1,0.3-1.5c0.1-0.5,0.1-0.9,0.1-1.4	c0-0.4-0.3-0.7-0.7-0.9c0,0,0.1-0.1,0.1-0.1c0.1,0,0.1,0,0.2,0c-0.2-0.1-0.3-0.3-0.3-0.6c0-0.3-0.1-0.5-0.3-0.7	c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0.1-0.1,0.3-0.1c-0.1-0.4-0.1-0.8,0-1.2c0.1-0.4,0.2-0.8,0.4-1.1c0.2-0.3,0.4-0.7,0.6-1	c0.2-0.3,0.5-0.7,0.7-1c0.2,0,0.3,0,0.3,0.1c0,0.1,0.1,0.1,0.2,0c0.3-0.2,0.5-0.5,0.7-0.8c0.1-0.3,0.2-0.6,0.2-0.9	c0.2-0.5,0.4-1,0.6-1.5c0.2-0.5,0.5-1,0.8-1.4c0.3-0.4,0.6-0.8,1-1.2c0.4-0.3,0.7-0.6,1.1-0.8c0.3-0.2,0.7-0.5,1.1-0.7	c0.4-0.2,0.9-0.4,1.4-0.5c0.5-0.1,1-0.2,1.5-0.2c0.5,0,1,0,1.4,0.2c0.1,0,0.2,0,0.2-0.1C213,0.3,213,0.3,213.1,0.3z M211.6,8.9	c0.6-0.3,1.3-0.5,1.9-0.6c0.7-0.2,1.3-0.5,1.7-0.9c-0.3-0.3-0.7-0.5-1.3-0.7c-0.6-0.2-1.2-0.3-1.8-0.4c-0.7,0-1.3,0-2,0.2	c-0.7,0.2-1.2,0.5-1.7,0.9c-0.1,0.4-0.3,0.8-0.4,1c-0.1,0.3-0.2,0.6-0.4,0.9c0,0.1,0.1,0.2,0.2,0.3c0.1,0.1,0.2,0.2,0.2,0.3	c0.6-0.3,1.1-0.5,1.7-0.5C210.5,9.3,211,9.2,211.6,8.9z" /></svg></a></h2>
                        <h2 class="events"><a href="https://www.thisiscolossal.com/events/">Events</a></h2>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="header_logo_and_info" class="col_01">
                <div class="logo">
                    <figure></figure>
                    <figure></figure>
                    <figure></figure>
                    <figure></figure>
                    <figure></figure>
                    <a href="/"><img src="https://www.thisiscolossal.com/wp-content/themes/colossal-v4/images/colossal_logo_header.png" alt="Colossal" /></a>
                </div>
                <nav id="info">
                    <ul id="colossal-info-menu" class=""><li id="menu-item-85728" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85728"><a href="https://www.thisiscolossal.com/visual-archive/">Archive</a></li>
                        <li id="menu-item-85729" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85729"><a href="https://www.thisiscolossal.com/about/">About</a></li>
                        <li id="menu-item-117173" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-117173"><a href="https://colossal.art/projects/">Projects</a></li>
                        <li id="menu-item-85731" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-85731"><a href="/contact">Contact</a></li>
                        <li id="menu-item-85733" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-85733"><a href="http://nectarads.com/publishers/colossal/">Advertise</a></li>
                        <li class="stretch"></li></ul>
                    <div class="divider"><hr /></div>
                    <div class="social">
                        <a href="https://instagram.com/colossal" target="_blank" class="big_target social_media_instagram"></a>
                        <a href="https://apple.news/TPte4elrHQ1O1tz8cUnLlpQ" target="_blank" class="big_target social_media_applenews"></a>
                        <a href="https://facebook.com/thisiscolossal" target="_blank" class="big_target social_media_facebook"></a>
                        <a href="https://twitter.com/colossal" target="_blank" class="big_target social_media_twitter"></a>
                        <a href="https://www.pinterest.com/itscolossal/boards/" target="_blank" class="big_target social_media_pinterest"></a>
                        <a href="https://links.thisiscolossal.com/" target="_blank" class="big_target social_media_tumblr"></a>
                        <a href="https://www.thisiscolossal.com/feed/" target="_blank" class="big_target social_media_rss"></a>
                    </div>
                </nav>
            </div>
            <div class="clear"></div>
            <div class="divider"></div>
            <div class="m_shop">
                <a class="shop" href="https://colossalshop.com/"></a>
            </div>
            <div id="search_random" class="col_02">
                <div id="header_search">
                    <form method="get" action="/" class="searchbox sbx-custom" name="searchform" novalidate>
                        <div role="search" class="sbx-custom__wrapper">
                            <input type="search" name="s" placeholder="Search" autocomplete="off" required="required" class="sbx-custom__input" />
                            <button type="submit" title="Submit your search query." class="sbx-custom__submit">
                                <svg version="1.1" id="icon_search_header" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path d="M16,14.912l-4.799-4.799c0.831-1.062,1.331-2.396,1.331-3.847C12.532,2.811,9.721,0,6.266,0C2.811,0,0,2.812,0,6.266c0,3.456,2.811,6.266,6.266,6.266c1.451,0,2.785-0.5,3.848-1.331L14.912,16L16,14.912z M6.266,10.994c-2.607,0-4.728-2.121-4.728-4.728c0-2.607,2.121-4.728,4.728-4.728s4.728,2.121,4.728,4.728C10.994,8.874,8.873,10.994,6.266,10.994z" /></svg>
                            </button>
                            <button type="reset" title="Clear the search query." class="sbx-custom__reset">
                                Clear
                            </button>
                        </div>
                    </form>
                </div>
                <div id="header_random_post">
                    <h2><a href="https://www.thisiscolossal.com/2019/12/unstudio-amsterdam-store/">Random post<span class="icon"><svg version="1.1" id="shuffle_icon_header" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="16px" viewBox="0 0 20 16" enable-background="new 0 0 20 16" xml:space="preserve"><path d="M16,10l4,3l-4,3v-2.25h-2c-1.341,0-2.992-0.86-3.762-1.958l-0.67-0.958l0.915-1.308l0.984,1.406c0.491,0.702,1.675,1.319,2.532,1.319h2V10z M5.532,5.069l0.984,1.405l0.915-1.308l-0.67-0.958C5.992,3.11,4.341,2.25,3,2.25H0v1.5h3C3.857,3.75,5.041,4.366,5.532,5.069z M14,3.75h2V6l4-3l-4-3v2.25h-2c-1.341,0-2.992,0.86-3.762,1.958l-4.706,6.722C5.041,11.634,3.857,12.25,3,12.25H0v1.5h3c1.341,0,2.992-0.86,3.762-1.958l4.706-6.722C11.959,4.366,13.143,3.75,14,3.75z" /></svg></span></a></h2>
                </div>
            </div>
            <div class="m_clear"></div>
            <div class="m_divider"></div>
            <div id="header_nav_editorial" class="col_01">
                <div class="nav_editorial_fixed"></div>
                <div class="nav">
                    <ul id="colossal-header-main-menu" class=""><li class="hamburger_nav">
                        <div class="hamburger">
                            <div class="hamburger_menu">
                                <div class="top"></div>
                                <div class="middle"></div>
                                <div class="bottom"></div>
                            </div>
                        </div>
                    </li><li id="menu-item-85738" class="more_nav menu-item menu-item-type-custom menu-item-object-custom menu-item-85738"><a id="more_menu">More</a><a id="more_menu_mobile">More</a></li>
                        <li id="menu-item-120159" class="menu-item menu-item-type-post_type_archive menu-item-object-interview menu-item-120159"><a href="https://www.thisiscolossal.com/interviews/">Interviews</a></li>
                        <li id="menu-item-85753" class="photography menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85753"><a href="https://www.thisiscolossal.com/category/photography/">Photography</a></li>
                        <li id="menu-item-85760" class="crafts menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85760"><a href="https://www.thisiscolossal.com/category/craft/">Craft</a></li>
                        <li id="menu-item-85762" class="design menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85762"><a href="https://www.thisiscolossal.com/category/design/">Design</a></li>
                        <li id="menu-item-85763" class="art menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85763"><a href="https://www.thisiscolossal.com/category/art/">Art</a></li>
                    </ul>
                </div>
            </div>
            <div class="more_menu">
                <div class="more_menu_fixed"></div>
                <ul id="colossal-header-more-menu" class=""><li id="menu-item-85768" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85768"><a href="https://www.thisiscolossal.com/category/animation/">Animation</a></li>
                    <li id="menu-item-85773" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85773"><a href="https://www.thisiscolossal.com/tags/sculpture/">sculpture</a></li>
                    <li id="menu-item-85774" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85774"><a href="https://www.thisiscolossal.com/tags/installation/">installation</a></li>
                    <li id="menu-item-85777" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85777"><a href="https://www.thisiscolossal.com/category/science/">Science</a></li>
                    <li id="menu-item-85780" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85780"><a href="https://www.thisiscolossal.com/tags/street-art/">street art</a></li>
                    <li id="menu-item-85781" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85781"><a href="https://www.thisiscolossal.com/category/food/">Food</a></li>
                    <li id="menu-item-85782" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-85782"><a href="https://www.thisiscolossal.com/category/history/">History</a></li>
                    <li id="menu-item-85786" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85786"><a href="https://www.thisiscolossal.com/tags/video/">video</a></li>
                    <li id="menu-item-85787" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85787"><a href="https://www.thisiscolossal.com/tags/paper/">paper</a></li>
                    <li id="menu-item-85788" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85788"><a href="https://www.thisiscolossal.com/tags/painting/">painting</a></li>
                    <li id="menu-item-85789" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85789"><a href="https://www.thisiscolossal.com/tags/architecture/">architecture</a></li>
                    <li id="menu-item-85790" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85790"><a href="https://www.thisiscolossal.com/tags/animals/">animals</a></li>
                    <li id="menu-item-85791" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85791"><a href="https://www.thisiscolossal.com/tags/portraits/">portraits</a></li>
                    <li id="menu-item-85793" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85793"><a href="https://www.thisiscolossal.com/tags/humor/">humor</a></li>
                    <li id="menu-item-85794" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85794"><a href="https://www.thisiscolossal.com/tags/nature/">nature</a></li>
                    <li id="menu-item-85797" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85797"><a href="https://www.thisiscolossal.com/tags/drawing/">drawing</a></li>
                    <li id="menu-item-85798" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85798"><a href="https://www.thisiscolossal.com/tags/books/">books</a></li>
                    <li id="menu-item-85799" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85799"><a href="https://www.thisiscolossal.com/tags/birds/">birds</a></li>
                    <li id="menu-item-85801" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85801"><a href="https://www.thisiscolossal.com/tags/anatomy/">anatomy</a></li>
                    <li id="menu-item-85802" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85802"><a href="https://www.thisiscolossal.com/tags/light/">light</a></li>
                    <li id="menu-item-85804" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85804"><a href="https://www.thisiscolossal.com/tags/murals/">murals</a></li>
                    <li id="menu-item-85805" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85805"><a href="https://www.thisiscolossal.com/tags/landscapes/">landscapes</a></li>
                    <li id="menu-item-85807" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85807"><a href="https://www.thisiscolossal.com/tags/plants/">plants</a></li>
                    <li id="menu-item-85808" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85808"><a href="https://www.thisiscolossal.com/tags/ceramics/">ceramics</a></li>
                    <li id="menu-item-85809" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85809"><a href="https://www.thisiscolossal.com/tags/flowers/">flowers</a></li>
                    <li id="menu-item-85810" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85810"><a href="https://www.thisiscolossal.com/tags/embroidery/">embroidery</a></li>
                    <li id="menu-item-85812" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85812"><a href="https://www.thisiscolossal.com/tags/miniature/">miniature</a></li>
                    <li id="menu-item-85813" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85813"><a href="https://www.thisiscolossal.com/tags/collage/">collage</a></li>
                    <li id="menu-item-85814" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85814"><a href="https://www.thisiscolossal.com/tags/posters-and-prints/">posters and prints</a></li>
                    <li id="menu-item-85816" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85816"><a href="https://www.thisiscolossal.com/tags/glass/">glass</a></li>
                    <li id="menu-item-85819" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85819"><a href="https://www.thisiscolossal.com/tags/gifs/">gifs</a></li>
                    <li id="menu-item-85820" class="menu-item menu-item-type-taxonomy menu-item-object-post_tag menu-item-85820"><a href="https://www.thisiscolossal.com/tags/surreal/">surreal</a></li>
                </ul>
            </div>
            <div class="hamburger">
                <div class="hamburger_menu">
                    <div class="top"></div>
                    <div class="middle"></div>
                    <div class="bottom"></div>
                </div>
            </div>
            <div class="hamburger_items">
                <div class="col_02">
                    <div class="collapse_shop"><a class="shop" href="https://colossalshop.com/"></a></div>
                    <div class="events">
                        <h2><a href="https://www.thisiscolossal.com/members">Membership</a></h2>
                    </div>
                    <div class="events">
                        <h2><a href="https://www.thisiscolossal.com/events">Events</a></h2>
                    </div>
                </div>
                <div class="col_01">
                    <div class="social">
                        <a href="https://instagram.com/colossal" target="_blank" class="big_target social_media_instagram"></a>
                        <a href="https://apple.news/TPte4elrHQ1O1tz8cUnLlpQ" target="_blank" class="big_target social_media_applenews"></a>
                        <a href="https://facebook.com/thisiscolossal" target="_blank" class="big_target social_media_facebook"></a>
                        <a href="https://twitter.com/colossal" target="_blank" class="big_target social_media_twitter"></a>
                        <a href="https://pinterest.com/itscolossal/colossal/" target="_blank" class="big_target social_media_pinterest"></a>
                        <a href="http://links.thisiscolossal.com/" target="_blank" class="big_target social_media_tumblr"></a>
                        <a href="https://www.thisiscolossal.com/feed/" target="_blank" class="big_target social_media_rss"></a>
                        <span class="stretch"></span>
                    </div>
                    <div class="info">
                        <ul id="colossal-info-menu" class=""><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85728"><a href="https://www.thisiscolossal.com/visual-archive/">Archive</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-85729"><a href="https://www.thisiscolossal.com/about/">About</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-117173"><a href="https://colossal.art/projects/">Projects</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-85731"><a href="/contact">Contact</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-85733"><a href="http://nectarads.com/publishers/colossal/">Advertise</a></li>
                            <li class="stretch"></li></ul> </div>
                </div>
            </div>
            <div class="collapse_search">
                <form method="get" action="/" class="searchbox sbx-custom" name="searchform" novalidate>
                    <div role="search" class="sbx-custom__wrapper">
                        <input type="search" name="s" placeholder="Search" autocomplete="off" required="required" class="sbx-custom__input" />
                        <button type="submit" title="Submit your search query." class="sbx-custom__submit">
                            <svg version="1.1" id="icon_search" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve"><path d="M16,14.912l-4.799-4.799c0.831-1.062,1.331-2.396,1.331-3.847C12.532,2.811,9.721,0,6.266,0C2.811,0,0,2.812,0,6.266c0,3.456,2.811,6.266,6.266,6.266c1.451,0,2.785-0.5,3.848-1.331L14.912,16L16,14.912z M6.266,10.994c-2.607,0-4.728-2.121-4.728-4.728c0-2.607,2.121-4.728,4.728-4.728s4.728,2.121,4.728,4.728C10.994,8.874,8.873,10.994,6.266,10.994z" /></svg>
                        </button>
                        <button type="reset" title="Clear the search query." class="sbx-custom__reset">
                            Clear
                        </button>
                    </div>
                </form>
            </div>
            <div class="collapse_shop">
                <a class="shop" href="https://colossalshop.com/"></a>
            </div>
            <div class="collapse_clear"></div>
            <div class="collapse_divider"></div>
        </header>
    </div>
    <style>
        .layout-container {
            margin-bottom: 20px;
        }
        .layout-container .full,
        .layout-container .excerpt {
            cursor: pointer;
            color: rgba(128,128,128, 0.5);
        }
        .layout-full .full {
            font-weight: bold;
            color: rgba(128,128,128);
        }
        .layout-excerpt .excerpt {
            font-weight: bold;
            color: rgba(128,128,128);
        }
        .col-blue {
            color: rgba(80,191,242);
        }
    </style>
    <div id="content">
        <aside>
            <div id="secondary" class="widget-area" role="complementary">
                <aside id="text-11" class="widget widget_text"> <div class="textwidget"><div id="nectar_sidebar">
                    </p>
                    <div id='div-gpt-ad-1502230835991-1'>
                        <script type="68fa48e47a44e50bd58b7be7-text/javascript">
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1502230835991-1'); });
    </script>
                    </div>
                </div>
                    <h2>Advertise on Colossal with <a href="https://nectarads.com/" target="_blank" rel="noopener noreferrer">Nectar Ads</a>.</h2>
                </div>
                </aside><aside id="text-12" class="widget widget_text"> <div class="textwidget"><div>
                <div id="mediatemple">
                    <p><a href="http://mediatemple.net/webhosting/vps/" target="_blank" rel="noopener noreferrer"><svg version="1.1" id="mediatemple_logo" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 68 27" enable-background="new 0 0 68 27" xml:space="preserve"><path d="M7.073,0C5.104,4.365,3.765,8.069,3.765,13.462c0,6.184,2.193,10.95,3.192,13.112c0.029,0.031,0.087,0.146,0.087,0.174H4.365C1.74,22.787,0,18.083,0,13.347C0,7.757,2.198,3.2,4.42,0H7.073z M63.639,0.059h-2.682c0,0.028,0.057,0.143,0.084,0.17c1.001,2.169,3.197,6.929,3.197,13.118c0,5.387-1.342,9.092-3.311,13.46h2.654C65.806,23.607,68,19.051,68,13.462C68,8.729,66.261,4.022,63.639,0.059z M60.061,17.496c-0.876,0.166-1.697,0.275-2.627,0.275c-1.957,0-1.957-0.551-1.957-2.232v-4.761h4.789V8.294h-4.789V4.104l-7.105,1.844v2.347h-3.243v2.483h3.243v5.147c0,1.8,0.104,2.505,0.979,3.235c1.028,0.885,3.812,1.205,5.973,1.205c0.72,0,3.244-0.045,5.559-0.475l-0.616-2.417L60.061,17.496z M36.158,7.876c-4.093,0-6.501,1.239-7.608,1.88c-0.627-0.505-2.265-1.88-7.082-1.88c-1.785,0-4.771,0.138-7.661,1.88h-0.098V8.147H7.593v11.882h6.597v-5.774c0-1.747,0.34-2.297,0.869-2.825c0.676-0.687,2.457-1.146,4.191-1.146c3.467,0,3.42,1.214,3.42,3.232v6.513h6.6v-5.71c0-1.467,0.242-2.063,0.821-2.729c0.625-0.779,2.408-1.307,4.141-1.307c3.518,0,3.518,1.558,3.518,2.963v6.782h6.602v-7.614C44.352,11.109,44.352,7.876,36.158,7.876z"></path></svg></a></p>
                    <p><span class="caption">Colossal is hosted by <a href="http://mediatemple.net/webhosting/vps/" target="_blank" rel="noopener noreferrer">(mt) Media Temple VPS Hosting</a></span></p>
                    <div class="clear"></div>
                </div>
            </div>
            </div>
            </aside><aside id="text-10" class="widget widget_text"> <div class="textwidget">
                <div class="highlights">
                    <h1>Editor&#8217;s Picks: Photography</h1>
                    <p class="small_caps">Highlights below. For the full collection <a href="/collections/photography">click here</a>.</p>
                    <p> </p>
                    <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/09/dick-van-duijn-squirrel/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/09/squirrel-4-640x428@2x.jpg)" class="image"></a></span></p>
                        <h2><a href="https://www.thisiscolossal.com/2019/09/dick-van-duijn-squirrel/">Stop and Smell the Flowers: Dick van Duijn Captured a Squirrel&#8217;s Floral Delight</a></h2>
                    </div>
                    <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/beauty-standards-and-albinism-by-justin-dingwall/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/25f93b83524511.5d40256f3c715-640x960.jpg)" class="image"></a></span></p>
                        <h2><a href="https://www.thisiscolossal.com/2019/08/beauty-standards-and-albinism-by-justin-dingwall/">Models with Albinism Challenge Standards of Beauty in Photographs by Justin Dingwall</a></h2>
                    </div>
                    <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/stationary-milky-way-timelapse/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/Milkyway_01-640x360@2x.jpg)" class="image"></a></span></p>
                        <h2><a href="https://www.thisiscolossal.com/2019/08/stationary-milky-way-timelapse/">Earth&#8217;s Rotation Visualized in a Timelapse of the Milky Way Galaxy by Aryeh Nirenberg</a></h2>
                    </div>
                    <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/passport-photos-max-siedentopf/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/MaxSiedentopf_01-640x452@2x.jpg)" class="image"></a></span></p>
                        <h2><a href="https://www.thisiscolossal.com/2019/08/passport-photos-max-siedentopf/">Passport Photos Widened to Reveal Unexpected Chaos Hiding Just Beyond of the Frame</a></h2>
                    </div>
                    <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/afroart-photo-series/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/SFNov16-9747-Edit-640x960@2x.jpg)" class="image"></a></span></p>
                        <h2><a href="https://www.thisiscolossal.com/2019/08/afroart-photo-series/">AfroArt Photo Series Challenges Beauty Standards with Young Black Models</a></h2>
                    </div>
                    <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/02/andrew-mccarthy-high-def-moon-photography/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/02/moon_crop-640x640@2x.jpg)" class="image"></a></span></p>
                        <h2><a href="https://www.thisiscolossal.com/2019/02/andrew-mccarthy-high-def-moon-photography/">50,000 Photographs Combine to Form a Detailed Image of the Moon and Stars</a></h2>
                    </div>
                    <p><em><a href="/collections/photography" class="circle">See<br />the rest of the<br />Collection<br />››</a></em></p>
                    </p></div>
            </div>
            </aside> </div>
        </aside>
        <main class="entry-content">
            <h3 class="category" style="display:none">
                <div class="layout-container layout-full">
                    <span class="col-blue">TOGGLE LAYOUT:</span> <a href="" id="toggle-full-layout" class="full">FULL POSTS</a> | <a href="" id="toggle-excerpt-layout" class="excerpt">POST PREVIEWS</a>
                </div>
            </h3>
            ﻿<div id="posts">

            <h3 class="category ">
                <a href="https://www.thisiscolossal.com/category/art/">Art</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="{{env('APP_URL').'/marble-pillows-by-hakon-anton-fageras'}}">Realistic Pillows Sculpted from Blocks of White Marble by Håkon Anton Fagerås</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/marble-pillows-by-hakon-anton-fageras/">January 11, 2020</a></h3>
                <h3 class="author">Andrew LaSane</h3>
                <div class="clear"></div>
            </div>
            <p><img class="alignnone size-full wp-image-122778" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b.jpg" alt="" width="1200" height="974" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b-640x519.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b-768x623.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b-960x779.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b-624x506.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /></p>
            <p>In studios in Oslo and northern Italy, Norwegian sculptor <a href="http://fageras.com/" target="_blank" rel="noopener noreferrer">Håkon Anton Fagerås</a> uses a pneumatic hammer and other carving tools to shape blocks of marble into large white pillows. Slumped in natural poses, the realistic pillows feature smooth folds and wrinkles that contradict the properties of the medium. Without the shots of Fagerås in action, our eyes would not believe the finished products to be anything other than fabric and filler.</p>
            <p>In an interview with <a href="http://www.sculptureatelier.com/blog/2018/fageras" target="_blank" rel="noopener noreferrer"><em>Sculpture Atelier</em></a>, Fagerås explained his interest in the medium, saying marble is best for expressing the nuances of emotion. &#8220;Because of the material qualities of marble itself, it appears fragile. It’s quite fragile, but it’s not that fragile, and yet it appears so because of the translucency and pureness of the stone.&#8221; He added that it allows for sculpting at a very precise level, but that he tries &#8220;not to be too literal about it. I think that my main focus is to create an atmosphere, a sensation, more than a literal representation of something that expresses, for instance, fragility.&#8221;</p>
            <p>Head to <a href="https://www.instagram.com/fageras_sculpture/" target="_blank" rel="noopener noreferrer">Instagram</a> to see more of Fagerås&#8217;s marble masterpieces.</p>
            <p><img class="alignnone size-full wp-image-122776" style="font-size: 1rem" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0446.jpg" alt="" width="1200" height="800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0446.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0446-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0446-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0446-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0446-624x416.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /></p>
            <p><img class="alignnone size-full wp-image-122777" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0621.jpg" alt="" width="1200" height="1800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0621.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0621-640x960.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0621-768x1152.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0621-960x1440.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC0621-624x936.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /></p>
            <p><img class="alignnone size-full wp-image-122792" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/ba363daf807586263f4940e8b522936f.jpg" alt="" width="1200" height="1800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/ba363daf807586263f4940e8b522936f.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/ba363daf807586263f4940e8b522936f-640x960.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/ba363daf807586263f4940e8b522936f-768x1152.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/ba363daf807586263f4940e8b522936f-960x1440.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/ba363daf807586263f4940e8b522936f-624x936.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /></p>
            <div style="width: 625px;" class="wp-video"><!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
                <video class="wp-video-shortcode" id="video-122767-1" width="625" height="625" preload="metadata" controls="controls"><source type="video/mp4" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/78850726_153230339292313_7269836677675734724_n.mp4?_=1" /><a href="https://www.thisiscolossal.com/wp-content/uploads/2020/01/78850726_153230339292313_7269836677675734724_n.mp4">https://www.thisiscolossal.com/wp-content/uploads/2020/01/78850726_153230339292313_7269836677675734724_n.mp4</a></video></div>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/marble-pillows-by-hakon-anton-fageras/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/marble-pillows-by-hakon-anton-fageras/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="Realistic Pillows Sculpted from Blocks of White Marble by Håkon Anton Fagerås">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/marble-pillows-by-hakon-anton-fageras/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b-150x150.jpg&description=Realistic Pillows Sculpted from Blocks of White Marble by Håkon Anton Fagerås" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/marble-pillows-by-hakon-anton-fageras/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>﻿<div id="posts">

            <h3 class="category ">
                <a href="https://www.thisiscolossal.com/category/photography/">Photography</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/">Portraits of Venezuelan Families Reframe the Harrowing Journey of Immigrants</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/">January 10, 2020</a></h3>
                <h3 class="author">Grace Ebert</h3>
                <div class="clear"></div>
            </div>
            <div id="attachment_122784" style="width: 1210px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122784" class="wp-image-122784 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3.jpg" alt="" width="1200" height="1800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3-640x960.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3-768x1152.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3-960x1440.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3-624x936.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /><p id="caption-attachment-122784" class="wp-caption-text">&#8220;Arianny Torres packed a few changes of clothes, a couple toys, medicine, diapers, a baby bottle, photos of relatives and her bible into her backpack. With her son, Lucas and daughter, Alesia, she traveled 976 kilometers from Maracaibo to Bogotá. Sometimes they hitched a ride. Other times they caught a bus, cutting into the small amount of money Arianny had put aside for food. Now she sells candy in Bolivar Square and though things could be better, at least life is more stable than it was in Venezuela and her kids are able to eat three times a day. I see Arianny’s determination to find a more hopeful life in her fixed gaze.&#8221; All images © Gregg Segal, shared with permission</p></div>
            <p>In his <em>Undaily Bread</em> series, <a href="https://www.greggsegal.com/" target="_blank" rel="noopener noreferrer">Gregg Segal</a> photographs Venezuelan immigrants with the entirety of their belongings lying around them. Created in collaboration with <a href="https://www.acnur.org/" target="_blank" rel="noopener noreferrer">UNHCR</a>, an organization that helps refugees worldwide, the affective project shows a glimpse at what life as a Venezuelan refugee looks like, from the meager ingredients of their daily meals to the battered sneakers on their feet. Every image posted on Segal&#8217;s <a href="https://www.instagram.com/greggsegal/" target="_blank" rel="noopener noreferrer">Instagram</a> also includes a lengthy caption describing each family&#8217;s difficult journey.</p>
            <p>“For me, photography communicates better than simply words. Statistics are important, but people are not that interested in statistics,&#8221; Segal tells Colossal. &#8220;They&#8217;re emotional because they describe how little the people have.” This consequential series is an offshoot of <em><a href="https://www.greggsegal.com/P-Projects/Daily-Bread/1/caption" target="_blank" rel="noopener noreferrer">Daily Bread</a></em>, Segal&#8217;s well-known project that captures images of kids from around the world surrounded by what they eat each day.</p>
            <div id="attachment_122782" style="width: 1210px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122782" class="wp-image-122782 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-1.jpg" alt="" width="1200" height="1800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-1.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-1-640x960.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-1-768x1152.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-1-960x1440.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-1-624x936.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /><p id="caption-attachment-122782" class="wp-caption-text">&#8220;Nathalia Rodriguez (9) who walked from Barquisimeto, Venezuela to Bogota with her mom, ate only bread, crackers, arepas, chips, water, juice, lollipops and the one fruit they could afford, bananas. It’s been 3 years since Nathalia’s eaten an apple. Apples run 5,000 Bolivas now in Venezuela, about $12 US. Despite the harsh road she traveled, Nathalia projects resilience and resolve.&#8221; </p></div>
            <div id="attachment_122783" style="width: 1210px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122783" class="wp-image-122783 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-2.jpg" alt="" width="1200" height="1800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-2.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-2-640x960.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-2-768x1152.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-2-960x1440.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-2-624x936.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /><p id="caption-attachment-122783" class="wp-caption-text">&#8220;Yosiahanny’s daughter feels for the kick of her brother or sister in her mother’s womb. They made the journey from Venezuela surviving on arepas and water. Though life in Bogotá is difficult, Yosiahanny is grateful she’s able to eat more than once a day. What makes the crisis tolerable is love, she says.&#8221;</p></div>
            <div id="attachment_122785" style="width: 1210px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122785" class="wp-image-122785 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-4.jpg" alt="" width="1200" height="1800" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-4.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-4-640x960.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-4-768x1152.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-4-960x1440.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-4-624x936.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /><p id="caption-attachment-122785" class="wp-caption-text">&#8220;When I met 7 year old Williams, he showed me his backpack in which he carried a few things from home including his last homework assignment. He misses his grandmother’s arepas and stewed chicken. On the long walk from Venezuela, there was only bread, water, cookies and fruit to eat.&#8221;</p></div>
            <div id="attachment_122786" style="width: 1210px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122786" class="wp-image-122786 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-5.jpg" alt="" width="1200" height="1620" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-5.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-5-640x864.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-5-768x1037.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-5-960x1296.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-5-624x842.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /><p id="caption-attachment-122786" class="wp-caption-text">&#8220;Michell, a single mom, made the trip with her two kids twice. During the 2nd attempt, Michell had an epileptic seizure and lost consciousness. 16 days later she made it to Bogotá and was admitted. In her portrait, Michell contends with the dueling energy of her kids, trying to soothe her daughter while her son appears to be driving the bus. After the shoot, her little boy held onto two loaves of bread, carrying them around the studio, tucked under his arms for later.&#8221;</p></div>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="Portraits of Venezuelan Families Reframe the Harrowing Journey of Immigrants">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3-150x150.jpg&description=Portraits of Venezuelan Families Reframe the Harrowing Journey of Immigrants" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>﻿<div id="posts">

            <h3 class="category ">
                <a href="https://www.thisiscolossal.com/category/art/">Art</a>
                <a href="https://www.thisiscolossal.com/category/history/">History</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/">Paris Musées Releases 100,000 Images of Artworks for Unrestricted Public Use</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/">January 10, 2020</a></h3>
                <h3 class="author">Grace Ebert</h3>
                <div class="clear"></div>
            </div>
            <div id="attachment_122737" style="width: 1470px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122737" class="size-full wp-image-122737" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1.jpg" alt="" width="1460" height="976" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1.jpg 1460w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-640x428.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-768x513.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-960x642.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-624x417.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-640x428@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-624x417@2x.jpg 1248w" sizes="(max-width: 1460px) 100vw, 1460px" /><p id="caption-attachment-122737" class="wp-caption-text">“<a href="http://parismuseescollections.paris.fr/en/node/226737" target="_blank" rel="noopener noreferrer">Setting Sun on the Seine at Lavacourt</a>&#8221; by Claude Monet (1880), oil on canvas, part of the collection at Petit Palais, Paris</p></div>
            <p>This week the <a href="http://parismuseescollections.paris.fr/en" target="_blank" rel="noopener noreferrer">Paris Musées</a> added 100,000 digital copies of its artworks to the public domain, making them free and unrestricted for the public to download and use. From Claude Monet&#8217;s &#8220;Setting Sun on the Seine at Lavacourt&#8221; to Paul Cézanne&#8217;s &#8220;Portrait of Ambroise Vollard,&#8221; the collection contains work from artists, such as Gustave Courbet, Victor Hugo, and Rembrandt, that are housed at 14 museums in Paris like the Musée d’Art Moderne, Petit Palais, and even the catacombs.</p>
            <p>Each file contains the high-resolution image, a description about the piece, and the location of the original work, in addition to an exhibition history and citation tips. Most of the images available right now capture 2D works, although there are lower resolution files available of pieces that are not yet in the public domain, providing visitors to the site a chance to view more of the museums&#8217; collections. The site also offers <a href="http://parismuseescollections.paris.fr/fr/expositions-virtuelles" target="_blank" rel="noopener noreferrer">virtual exhibitions</a>, with <a href="http://parismuseescollections.paris.fr/en/expositions-virtuelles/light-and-shade-l-ombre-et-la-lumiere" target="_blank" rel="noopener noreferrer">a project</a> centered on the collections at Maison de Victor Hugo currently on view. (via <a href="https://hyperallergic.com/536360/images-of-100000-artworks-from-paris-museum-collections-now-freely-available-to-the-public/" target="_blank" rel="noopener noreferrer">Hyperallergic</a>)</p>
            <div id="attachment_122765" style="width: 2010px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122765" class="size-full wp-image-122765" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7.jpg" alt="" width="2000" height="2484" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-640x795.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-768x954.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-960x1192.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-624x775.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-640x795@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-768x954@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-960x1192@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musee-7-624x775@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /><p id="caption-attachment-122765" class="wp-caption-text">&#8220;<a href="http://parismuseescollections.paris.fr/fr/petit-palais/oeuvres/portrait-de-juliette-courbet" target="_blank" rel="noopener noreferrer">Portrait of Juliette Courbet</a>&#8221; by Gustave Courbet (1844), oil on canvas, part of the collection at Petit Palais</p></div>
            <div id="attachment_122738" style="width: 1470px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122738" class="size-full wp-image-122738" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2.jpg" alt="" width="1460" height="1799" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2.jpg 1460w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2-640x789.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2-768x946.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2-960x1183.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2-624x769.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2-640x789@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-2-624x769@2x.jpg 1248w" sizes="(max-width: 1460px) 100vw, 1460px" /><p id="caption-attachment-122738" class="wp-caption-text">“<a href="http://parismuseescollections.paris.fr/en/node/226216" target="_blank" rel="noopener noreferrer">Portrait of Ambroise Vollard</a>” by Paul Cézanne (1899), oil on canvas, part of the collection at Petit Palais, Paris</p></div>
            <div id="attachment_122766" style="width: 2010px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122766" class="size-full wp-image-122766" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8.jpg" alt="" width="2000" height="2699" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-640x864.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-768x1036.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-960x1296.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-624x842.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-640x864@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-768x1036@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-960x1296@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-8-624x842@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /><p id="caption-attachment-122766" class="wp-caption-text">&#8220;<a href="http://parismuseescollections.paris.fr/fr/maison-de-victor-hugo/oeuvres/julia-jackson-de-face-stella#infos-principales" target="_blank" rel="noopener noreferrer">Julia Jackson from the front &#8216;Stella&#8217;</a>&#8221; by Julia Margaret Cameron (1867), photograph printed on albumen paper, part of the collection at Maison de Victor Hugo</p></div>
            <div id="attachment_122739" style="width: 2010px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122739" class="wp-image-122739 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3.jpg" alt="" width="2000" height="2230" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-640x714.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-768x856.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-960x1070.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-624x696.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-640x714@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-768x856@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-960x1070@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-3-624x696@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /><p id="caption-attachment-122739" class="wp-caption-text">&#8220;<a href="http://parismuseescollections.paris.fr/en/node/228230#infos-principales" target="_blank" rel="noopener noreferrer">Presentation in the Temple</a>&#8221; by Jacques Daret (1434-1435), oil on wood, part of the collection at Petit Palais, Paris</p></div>
            <div id="attachment_122741" style="width: 1404px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122741" class="wp-image-122741 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5.jpg" alt="" width="1394" height="1042" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5.jpg 1394w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-640x478.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-768x574.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-960x718.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-624x466.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-280x210.jpg 280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-285x214.jpg 285w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-536x402.jpg 536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-640x478@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-624x466@2x.jpg 1248w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-280x210@2x.jpg 560w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-285x214@2x.jpg 570w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-5-536x402@2x.jpg 1072w" sizes="(max-width: 1394px) 100vw, 1394px" /><p id="caption-attachment-122741" class="wp-caption-text"><a href="http://parismuseescollections.paris.fr/en/node/129515#infos-principales" target="_blank" rel="noopener noreferrer">Bronze medal</a> of Jean-Jacques Rousseau (19th century), part of the collection at Musée Carnavalet</p></div>
            <div id="attachment_122740" style="width: 1210px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122740" class="wp-image-122740 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-4.jpg" alt="" width="1200" height="1514" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-4.jpg 1200w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-4-640x807.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-4-768x969.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-4-960x1211.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-4-624x787.jpg 624w" sizes="(max-width: 1200px) 100vw, 1200px" /><p id="caption-attachment-122740" class="wp-caption-text">&#8220;<a href="http://parismuseescollections.paris.fr/en/node/212391#infos-principales" target="_blank" rel="noopener noreferrer">Portrait of Mr. Victor Hugo</a>&#8221; by Léon Bonnat (1879), oil on canvas, part of the collection at Maison de Victor Hugo</p></div>
            <div id="attachment_122754" style="width: 2010px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122754" class="size-full wp-image-122754" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6.jpg" alt="" width="2000" height="2836" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-640x908.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-768x1089.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-960x1361.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-624x885.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-640x908@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-768x1089@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-960x1361@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-6-624x885@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /><p id="caption-attachment-122754" class="wp-caption-text">&#8220;<a href="http://parismuseescollections.paris.fr/en/node/143011#infos-principales" target="_blank" rel="noopener noreferrer">Thirty-seven portraits of Voltaire</a>&#8221; by Dominique Vivant-Denon (1775), print, part of the collection at Musée Carnavalet</p></div>
            <p>&nbsp;</p>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="Paris Musées Releases 100,000 Images of Artworks for Unrestricted Public Use">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1-150x150.jpg&description=Paris Musées Releases 100,000 Images of Artworks for Unrestricted Public Use" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>﻿<div id="posts">

            <h3 class="category ">
                <a href="https://www.thisiscolossal.com/category/design/">Design</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/">Small Shapes Slot Together to Construct Vessels That Can Be Reconfigured</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/">January 10, 2020</a></h3>
                <h3 class="author">Grace Ebert</h3>
                <div class="clear"></div>
            </div>
            <div id="attachment_122619" style="width: 2010px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122619" class="wp-image-122619 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /><p id="caption-attachment-122619" class="wp-caption-text">All images © Kenji Abe</p></div>
            <p>To combat single-use plastic waste, Tokyo-based designer <a href="https://kenjiabe.com/projects" target="_blank" rel="noopener noreferrer">Kenji Abe</a> has conceived of a packaging material that can be arranged in various shapes and refashioned multiple times. The six-tipped <a href="https://kenjiabe.com/cy-bo" target="_blank" rel="noopener noreferrer">CY-BO</a> pieces can be woven together to create pouches, placemats, and other vessels that then can be deconstructed and reused. The project even reached the final rounds of the 2018 <a href="https://www.kokuyo.com/en/award/archive/prizepast/2018.html" target="_blank" rel="noopener noreferrer">Kokuyo Design Awards</a>.</p>
            <p>Because of the shape&#8217;s flexibility, Abe says other materials like leather can be used in its place to create similar products. “It is a new packaging material that can be used depending on one’s ideas,” Abe told <a href="https://plainmagazine.com/kenji-abe-cy-bo/" target="_blank" rel="noopener noreferrer">Plain Magazine</a>. “Because in order to reduce discarded plastic, it’s necessary to make packing materials that can be reused as many times as possible.” You can follow more of Abe&#8217;s inventive designs on <a href="https://www.instagram.com/abknj/" target="_blank" rel="noopener noreferrer">Instagram</a>.</p>
            <p><img class="alignnone size-full wp-image-122620" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-2-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <p><img class="alignnone size-full wp-image-122621" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-3-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <p><img class="alignnone size-full wp-image-122622" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-4-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <p><img class="alignnone size-full wp-image-122623" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5.jpg" alt="" width="2000" height="1298" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-640x415.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-768x498.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-960x623.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-624x405.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-640x415@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-768x498@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-960x623@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-5-624x405@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <p><img class="alignnone size-full wp-image-122624" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-6-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <p><img class="alignnone size-full wp-image-122625" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-7-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <p><img class="alignnone size-full wp-image-122626" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8.jpg" alt="" width="2000" height="1333" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-624x416.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-640x427@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-768x512@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-960x640@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-8-624x416@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></p>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="Small Shapes Slot Together to Construct Vessels That Can Be Reconfigured">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1-150x150.jpg&description=Small Shapes Slot Together to Construct Vessels That Can Be Reconfigured" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>﻿<div id="posts">

            <h3 class="category ">
                <a href="https://www.thisiscolossal.com/category/art/">Art</a>
                <a href="https://www.thisiscolossal.com/category/colossal-2/">Colossal</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/">Fundraiser: Buy This Artwork and Support the Bushfire Crisis in Australia</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/">January 9, 2020</a></h3>

                <h3 class="author"><a href="http://christopher.jobs/on/" title="Visit Christopher Jobson&#8217;s website" rel="author external">Christopher Jobson</a></h3>
                <div class="clear"></div>
            </div>
            <p><iframe src="https://player.vimeo.com/video/383034313" width="960" height="540" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
            <p>Earlier this week we were sent this heartbreaking new animation from <a href="http://ohyeahwow.com/" target="_blank" rel="noopener noreferrer">Oh Yeah Wow</a> (<a href="/?s=%22oh+yeah+wow%22">previously</a>) that was created in direct response to the horrific <a href="https://disasterphilanthropy.org/disaster/2019-australian-wildfires/" target="_blank" rel="noopener noreferrer">Bushfire Crisis</a> currently unfolding across Australia. Titled &#8220;<a href="https://vimeo.com/383034313" target="_blank" rel="noopener noreferrer">Tomorrow&#8217;s on Fire</a>,&#8221; the short addresses the collective hopelessness felt in the face of political inaction, and the loss of 28 lives, thousands of homes, and potentially hundreds of millions of animals, in a fire season greatly exacerbated by the effects of global warming.</p>
            <p>Oh Yeah Wow&#8217;s animation inspired us to put together a quick fundraiser. We reached out to artists across the globe and asked if anyone might be willing to donate a print, painting, or object, with a percentage of sales going toward humanitarian, wildlife, and firefighter support in Australia. More than 50 artists answered the call offering their work, so many that we were unable to include everyone here. All the pieces below are available now, with proceeds going toward various relief organizations. Click through to each work to see the terms and beneficiary.</p>
            <p><strong>Thank you</strong> to everyone for contributing. It means the world. If you&#8217;re unable to afford a purchase right now, please consider donating directly to the <a href="https://www.wildlifevictoria.org.au/" target="_blank" rel="noopener noreferrer">Wildlife Victoria</a>. Without further adieu, <strong>PLEASE buy this art</strong>.</p>
            <div id="attachment_122691" style="width: 810px" class="wp-caption alignnone"><a href="https://shop.hanwriting.com/products/in-the-shade-of-trees"><img aria-describedby="caption-attachment-122691" class="size-full wp-image-122691" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9.jpg" alt="" width="800" height="520" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9.jpg 800w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9-640x416.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9-768x499.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9-624x406.jpg 624w" sizes="(max-width: 800px) 100vw, 800px" /></a><p id="caption-attachment-122691" class="wp-caption-text"><a href="https://shop.hanwriting.com/products/in-the-shade-of-trees">In the Shade of Trees, Embroidered vintage postcard by Han Cao</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122696" style="width: 1090px" class="wp-caption alignnone"><a href="https://tripletiger.teemill.com/"><img aria-describedby="caption-attachment-122696" class="size-full wp-image-122696" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-10.jpg" alt="" width="1080" height="1137" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-10.jpg 1080w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-10-640x674.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-10-768x809.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-10-960x1011.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-10-624x657.jpg 624w" sizes="(max-width: 1080px) 100vw, 1080px" /></a><p id="caption-attachment-122696" class="wp-caption-text"><a href="https://tripletiger.teemill.com/">Australia Tees &amp; Totes by Triple Tiger</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122687" style="width: 1510px" class="wp-caption alignnone"><a href="https://thebirdmachine.com/collections/all/products/wombats-stay-safe"><img aria-describedby="caption-attachment-122687" class="size-full wp-image-122687" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8.jpg" alt="" width="1500" height="2000" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-640x853.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-768x1024.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-960x1280.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-624x832.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-1122x1496.jpg 1122w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-840x1120.jpg 840w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-687x916.jpg 687w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-414x552.jpg 414w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-354x472.jpg 354w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-640x853@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-624x832@2x.jpg 1248w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-687x916@2x.jpg 1374w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-414x552@2x.jpg 828w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-8-354x472@2x.jpg 708w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122687" class="wp-caption-text"><a href="https://thebirdmachine.com/collections/all/products/wombats-stay-safe">Wombats Stay Safe Signed &amp; Numbered Limited Edition by Jay Ryan</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122705" style="width: 1510px" class="wp-caption alignnone"><a href="http://www.joshharker.com/store/"><img aria-describedby="caption-attachment-122705" class="size-full wp-image-122705" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15.jpg" alt="" width="1500" height="1029" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15-640x439.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15-768x527.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15-960x659.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15-624x428.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15-640x439@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-15-624x428@2x.jpg 1248w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122705" class="wp-caption-text"><a href="http://www.joshharker.com/store/">Anatomical and Botanical Filigree Sculptures by Joshua Harker</a> (use code COLOSSAL at checkout, 40% donation to WIRES)</p></div>
            <p>&nbsp;</p>
            <div id="attachment_122702" style="width: 2010px" class="wp-caption alignnone"><a href="https://www.etsy.com/shop/ErikFremstad"><img aria-describedby="caption-attachment-122702" class="size-full wp-image-122702" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13.jpg" alt="" width="2000" height="2770" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13.jpg 2000w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-640x886.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-768x1064.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-960x1330.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-624x864.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-640x886@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-768x1064@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-960x1330@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-13-624x864@2x.jpg 1248w" sizes="(max-width: 2000px) 100vw, 2000px" /></a><p id="caption-attachment-122702" class="wp-caption-text"><a href="https://www.etsy.com/shop/ErikFremstad">Wolf &amp; Bison Prints by Erik Fremstad</a> (100% of proceeds to WIRES)</p></div>
            <p>&nbsp;</p>
            <div id="attachment_122678" style="width: 1510px" class="wp-caption alignnone"><a href="https://www.etsy.com/listing/689882662/art-for-donation-for-australian?ref=shop_home_active_1"><img aria-describedby="caption-attachment-122678" class="size-full wp-image-122678" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new.jpg" alt="" width="1500" height="1500" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-150x150.jpg 150w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-640x640.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-768x768.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-960x960.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-624x624.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-50x50.jpg 50w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-1472x1472.jpg 1472w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-1104x1104.jpg 1104w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-912x912.jpg 912w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-550x550.jpg 550w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-470x470.jpg 470w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-150x150@2x.jpg 300w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-640x640@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-624x624@2x.jpg 1248w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-50x50@2x.jpg 100w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-550x550@2x.jpg 1100w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-3-new-470x470@2x.jpg 940w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122678" class="wp-caption-text"><a href="https://www.etsy.com/listing/689882662/art-for-donation-for-australian?ref=shop_home_active_1">Cut Paper Koala by Nayan Vaishali</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122680" style="width: 860px" class="wp-caption alignnone"><a href="https://wootbear.com/dcon2019-koala-spirit-by-nathan-jurevicius/"><img aria-describedby="caption-attachment-122680" class="wp-image-122680 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-4.jpg" alt="" width="850" height="1075" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-4.jpg 850w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-4-640x809.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-4-768x971.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-4-624x789.jpg 624w" sizes="(max-width: 850px) 100vw, 850px" /></a><p id="caption-attachment-122680" class="wp-caption-text"><a href="https://wootbear.com/dcon2019-koala-spirit-by-nathan-jurevicius/">Koala Spirit by Nathan Jurevicius</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122684" style="width: 1510px" class="wp-caption alignnone"><a href="https://www.etsy.com/listing/754436448/fox?ref=shop_home_feat_2"><img aria-describedby="caption-attachment-122684" class="size-full wp-image-122684" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6.jpg" alt="" width="1500" height="762" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6-640x325.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6-768x390.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6-960x488.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6-624x317.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6-640x325@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-6-624x317@2x.jpg 1248w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122684" class="wp-caption-text"><a href="https://www.etsy.com/listing/768317977/jackrabbit">Jackrabbit</a> and <a href="https://www.etsy.com/listing/754436448/fox">Fox</a> prints by <a href="https://www.etsy.com/shop/SophyTuttle?ref=simple-shop-header-name&amp;listing_id=768317977">Sophy Tuttle</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122682" style="width: 1510px" class="wp-caption alignnone"><a href="http://www.fionaharrington.com/shop/lace/farmyard-collection/the-chicken/"><img aria-describedby="caption-attachment-122682" class="wp-image-122682 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5.jpg" alt="" width="1500" height="1827" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5-640x780.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5-768x935.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5-960x1169.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5-624x760.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5-640x780@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-5-624x760@2x.jpg 1248w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122682" class="wp-caption-text"><a href="http://www.fionaharrington.com/shop/lace/farmyard-collection/the-chicken/">The Chicken by Fiona Harrington</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122672" style="width: 912px" class="wp-caption alignnone"><a href="https://www.etsy.com/uk/listing/713252131/silver-edition-snake-and-poppies-lino?ref=shop_home_active_8&amp;frs=1"><img aria-describedby="caption-attachment-122672" class="size-full wp-image-122672" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-1.jpg" alt="" width="902" height="1588" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-1.jpg 902w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-1-640x1127.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-1-768x1352.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-1-624x1099.jpg 624w" sizes="(max-width: 902px) 100vw, 902px" /></a><p id="caption-attachment-122672" class="wp-caption-text"><a href="https://www.etsy.com/uk/listing/713252131/silver-edition-snake-and-poppies-lino?ref=shop_home_active_8&amp;frs=1">Silver Edition Snake &amp; Poppies Print by Rachael Hibbs</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122674" style="width: 2244px" class="wp-caption alignnone"><a href="https://www.etsy.com/listing/636757398/order-up-7?ref=shop_home_active_22&amp;frs=1"><img aria-describedby="caption-attachment-122674" class="size-full wp-image-122674" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2.jpg" alt="" width="2234" height="2257" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2.jpg 2234w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-640x647.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-768x776.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-960x970.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-624x630.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-50x50.jpg 50w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-640x647@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-768x776@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-960x970@2x.jpg 1920w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-624x630@2x.jpg 1248w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-2-50x50@2x.jpg 100w" sizes="(max-width: 2234px) 100vw, 2234px" /></a><p id="caption-attachment-122674" class="wp-caption-text"><a href="https://www.etsy.com/listing/636757398/order-up-7?ref=shop_home_active_22&amp;frs=1">Order Up! #7 Acrylic on wood by Melissa Shawnee</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122686" style="width: 886px" class="wp-caption alignnone"><a href="https://www.etsy.com/listing/588231073/australia-wildfire-fundraiser-artwork?ref=shop_home_active_40&amp;frs=1http://"><img aria-describedby="caption-attachment-122686" class="size-full wp-image-122686" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-7.jpg" alt="" width="876" height="1200" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-7.jpg 876w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-7-640x877.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-7-768x1052.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-7-624x855.jpg 624w" sizes="(max-width: 876px) 100vw, 876px" /></a><p id="caption-attachment-122686" class="wp-caption-text"><a href="https://www.etsy.com/listing/588231073/australia-wildfire-fundraiser-artwork?ref=shop_home_active_40&amp;frs=1">Handmade Collage by Naomi Vona</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122698" style="width: 1510px" class="wp-caption alignnone"><a href="https://www.nonecrire.com/shop/3nvmam34in9dqwy85dggkixh6kztzi-lrr2w"><img aria-describedby="caption-attachment-122698" class="size-full wp-image-122698" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11.jpg" alt="" width="1500" height="1481" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-640x632.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-768x758.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-960x948.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-624x616.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-50x50.jpg 50w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-640x632@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-624x616@2x.jpg 1248w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-11-50x50@2x.jpg 100w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122698" class="wp-caption-text"><a href="https://www.nonecrire.com/shop/3nvmam34in9dqwy85dggkixh6kztzi-lrr2w">Photographs by Ewa Szawlowska</a></p></div>
            <p>&nbsp;</p>
            <div id="attachment_122700" style="width: 1810px" class="wp-caption alignnone"><a href="https://www.freshlypressed.ch/fine-art-prints"><img aria-describedby="caption-attachment-122700" class="size-full wp-image-122700" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12.jpg" alt="" width="1800" height="1204" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12.jpg 1800w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-640x428.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-768x514.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-960x642.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-624x417.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-640x428@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-768x514@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-12-624x417@2x.jpg 1248w" sizes="(max-width: 1800px) 100vw, 1800px" /></a><p id="caption-attachment-122700" class="wp-caption-text"><a href="https://www.freshlypressed.ch/fine-art-prints">Pressed Flower Fine Art Prints by Lindsay Buck</a> (100% proceeds to WIRES)</p></div>
            <p>&nbsp;</p>
            <div id="attachment_122707" style="width: 1510px" class="wp-caption alignnone"><a href="https://www.dasilvastudios.com/shop-1/bushfire-wildlife-relief-art-enamel-eucalyptus-earrings-hand-painted"><img aria-describedby="caption-attachment-122707" class="size-full wp-image-122707" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16.jpg" alt="" width="1500" height="1773" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16.jpg 1500w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16-640x756.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16-768x908.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16-960x1135.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16-624x738.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16-640x756@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-16-624x738@2x.jpg 1248w" sizes="(max-width: 1500px) 100vw, 1500px" /></a><p id="caption-attachment-122707" class="wp-caption-text"><a href="https://www.dasilvastudios.com/shop-1/bushfire-wildlife-relief-art-enamel-eucalyptus-earrings-hand-painted">Handpainted Eucalyptus Earrings by Jessica daSilva</a></p></div>
            <p>&nbsp;</p>
            <p><strong>AND THERE&#8217;S MORE&#8230;</strong></p>
            <p>See also work by <a href="https://culturmodern.com/products/west" target="_blank" rel="noopener noreferrer">Cultur</a>, <a href="https://elisadore.com/store/mt-st-helens" target="_blank" rel="noopener noreferrer">Elisa Dore</a>, <a href="https://www.3fishstudios.com/products/i-love-you-australia-print" target="_blank" rel="noopener noreferrer">3 Fish Studios</a>, <a href="https://stefanieshank.bigcartel.com/product/signed-australia-relief-print" target="_blank" rel="noopener noreferrer">Stephanie Shank</a>, <a href="https://www.etsy.com/listing/539324757/glass-spirit-bear-fetish-southwestern?ref=shop_home_active_5" target="_blank" rel="noopener noreferrer">Marcy Lamberson</a>, <a href="https://www.etsy.com/listing/754520378/koala-and-cub-eco-friendly-fine-art-wood" target="_blank" rel="noopener noreferrer">LittleGoldFoxDesigns</a>, <a href="https://www.jennybelin.com/shop/koala-bear-painting" target="_blank" rel="noopener noreferrer">Jenny Belin</a>, <a href="https://danalvarado.bigcartel.com/product/damaged-ecosystem" target="_blank" rel="noopener noreferrer">Dan Alvarado</a>, <a href="https://www.etsy.com/listing/730466592/moth-painting-cecropia-moth" target="_blank" rel="noopener noreferrer">Natalie Wernimont</a>, <a href="https://www.hrothstein.com/shop/limited-edition-great-smoky-national-parks-2050-print" target="_blank" rel="noopener noreferrer">Hannah Rothstein</a>, <a href="https://www.raynalo.com/product/futurebear-and-friends-sticker-set-for-australia/" target="_blank" rel="noopener noreferrer">Rayna Lo</a>, <a href="https://www.etsy.com/listing/705427350/deep-blue-large-print-12-x-18?ref=shop_home_active_1&amp;frs=1" target="_blank" rel="noopener noreferrer">Sally Bartos</a>, and <a href="https://www.cherismith.co.uk/fire" target="_blank" rel="noopener noreferrer">Cheri Smith</a>.</p>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="Fundraiser: Buy This Artwork and Support the Bushfire Crisis in Australia">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9-150x150.jpg&description=Fundraiser: Buy This Artwork and Support the Bushfire Crisis in Australia" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>﻿<div id="posts">

            <h3 class="category sponsor_category">
                <a href="https://www.thisiscolossal.com/category/sponsors/">Sponsor</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="https://www.thisiscolossal.com/2020/01/nycb-art-series-lauren-redniss/">NYCB Art Series Presents Lauren Redniss</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/nycb-art-series-lauren-redniss/">January 9, 2020</a></h3>
                <h3 class="author">Colossal</h3>
                <div class="clear"></div>
            </div>
            <p><iframe src="https://player.vimeo.com/video/383141008" width="640&quot;" height="480" frameborder="0&quot;" allowfullscreen="allowfullscreen"></iframe></p>
            <p>Lauren Redniss, an artist and author of several works of visual non-fiction, has been invited by New York City Ballet to create a signature installation for their eighth annual <a href="http://bit.ly/37M7lKU" target="_blank" rel="noopener noreferrer">Art Series</a>.</p>
            <p>She will create a 360-degree panorama of portraits depicting more than 100 people who work at New York City Ballet, celebrating the behind-the-scenes efforts that are the lifeblood of the theater.</p>
            <p>Redniss wants visitors to be surrounded by faces, color, and light. She says, “By looking into the eyes of each person and reading their words, I hope visitors’ experience of coming to the theater will gain a new dimension.”</p>
            <p>The installations will be on view at three special New York City Ballet Art Series performances on February 1 eve, 7, and 27 where all tickets are priced at $35. Performances are on sale now at <a href="http://bit.ly/37M7lKU" target="_blank" rel="noopener noreferrer">nycballet.com/artseries</a>.</p>
            <p><a href="http://bit.ly/37M7lKU"><img class="alignnone wp-image-122349 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb.jpg" alt="" width="1080" height="720" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb.jpg 1080w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb-640x427.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb-768x512.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb-960x640.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb-624x416.jpg 624w" sizes="(max-width: 1080px) 100vw, 1080px" /></a></p>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/nycb-art-series-lauren-redniss/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/nycb-art-series-lauren-redniss/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="NYCB Art Series Presents Lauren Redniss">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/nycb-art-series-lauren-redniss/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/nycb-150x150.jpg&description=NYCB Art Series Presents Lauren Redniss" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/nycb-art-series-lauren-redniss/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>﻿<div id="posts">

            <h3 class="category ">
                <a href="https://www.thisiscolossal.com/category/design/">Design</a>
            </h3>
            <div class="clear"></div>
            <h2><a href="https://www.thisiscolossal.com/2020/01/human-chairs-chris-wolston/">Playful Chairs Designed by Chris Wolston Impersonate the Humans Who Sit on Them</a></h2>
            <div class="post_details">
                <h3 class="date"><a href="https://www.thisiscolossal.com/2020/01/human-chairs-chris-wolston/">January 9, 2020</a></h3>
                <h3 class="author">Grace Ebert</h3>
                <div class="clear"></div>
            </div>
            <div id="attachment_122646" style="width: 1610px" class="wp-caption alignnone"><img aria-describedby="caption-attachment-122646" class="wp-image-122646 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-6-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /><p id="caption-attachment-122646" class="wp-caption-text">All images © Chris Wolston, by <a href="https://www.davidsierrastudio.com/" target="_blank" rel="noopener noreferrer">David Sierra</a></p></div>
            <p>Brooklyn-based designer <a href="https://www.chriswolston.com/" target="_blank" rel="noopener noreferrer">Chris Wolston</a> wonders why traditional furniture created for people to lounge and rest on lacks human-like qualities. &#8220;Wouldn’t it be nice to actually embrace these similarities?&#8221; asks a <a href="https://www.thefutureperfect.com/product/furniture/seating/nalgona-dining-chair-04/" target="_blank" rel="noopener noreferrer">statement</a> describing his recent Nalgona Chair line, which attempts to rectify the problems he sees with conventional seating models. Wolston&#8217;s imitative chairs have distinct appendages displayed in a way that mimics a person with their hands in the air or resting gently on their knees.</p>
            <p>The playful seats are made entirely of wicker harvested in the Colombian Amazon. &#8220;The human form riffs on the iconic shape of the plastic Remax Chair, ubiquitous through Colombia, and the playful humanoid quality found in pre-Columbian ceramics,&#8221; reads the <a href="https://www.thefutureperfect.com/product/furniture/seating/nalgona-dining-chair-04/" target="_blank" rel="noopener noreferrer">product&#8217;s description</a>. Head over to <a href="https://www.thefutureperfect.com/made-by/designer/chris-wolston/" target="_blank" rel="noopener noreferrer">The Future Perfect</a> to add one these unconventional furnishings to your collection, and follow Wolston on <a href="https://www.instagram.com/chriswolston/" target="_blank" rel="noopener noreferrer">Instagram</a> for his latest projects.</p>
            <p><img class="wp-image-122641 size-full" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <p><img class="alignnone size-full wp-image-122645" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-5-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <p><img class="alignnone size-full wp-image-122642" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-2-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <p><img class="alignnone size-full wp-image-122644" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-4-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <p><img class="alignnone size-full wp-image-122643" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-3-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <p><img class="alignnone size-full wp-image-122647" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-7-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <p><img class="alignnone size-full wp-image-122648" src="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8.jpg" alt="" width="1600" height="1133" srcset="https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8.jpg 1600w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-640x453.jpg 640w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-768x544.jpg 768w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-960x680.jpg 960w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-624x442.jpg 624w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-640x453@2x.jpg 1280w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-768x544@2x.jpg 1536w, https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-8-624x442@2x.jpg 1248w" sizes="(max-width: 1600px) 100vw, 1600px" /></p>
            <div class="post_details">
                <h3 class="date">&nbsp;</h3>
                <h5 class="share_story">Share this story</h5>
                <h3 class="author">&nbsp;</h3>
                <div class="clear"></div>
            </div>
            <div class="share">
                <span class="move_that_f"><div class="fb-like" data-href="https://www.thisiscolossal.com/2020/01/human-chairs-chris-wolston/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div></span>
                <span class="move_that_tweet"><a class="twitter-share-button" href="https://twitter.com/share" data-size="medium" data-url="https://www.thisiscolossal.com/2020/01/human-chairs-chris-wolston/" data-via="colossal" data-related="hyperallergic,booooooom,juxtapozMag,hifructosemag" data-text="Playful Chairs Designed by Chris Wolston Impersonate the Humans Who Sit on Them">Tweet</a></span>
                <span class="move_that_pin"><a href="https://pinterest.com/pin/create/button/?url=https://www.thisiscolossal.com/2020/01/human-chairs-chris-wolston/&media=https://www.thisiscolossal.com/wp-content/uploads/2020/01/wolston-1-150x150.jpg&description=Playful Chairs Designed by Chris Wolston Impersonate the Humans Who Sit on Them" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                <div class="clear share_break"><br /></div>
                <a href="https://www.thisiscolossal.com/2020/01/human-chairs-chris-wolston/emailpopup/" onclick="if (!window.__cfRLUnblockHandlers) return false; email_popup(this.href); return false;" rel="nofollow" title="" class="email_article" data-cf-modified-68fa48e47a44e50bd58b7be7-=""></a>
                <div class="clear"></div>
            </div>
        </div>
            <div class="clear"></div>
            <div id="pagination">
                <div class='wp-pagenavi' role='navigation'>
                    <span class='pages'>Page 1 of 930</span><span aria-current='page' class='current'>1</span><a class="page larger" title="Page 2" href="https://www.thisiscolossal.com/page/2/">2</a><a class="page larger" title="Page 3" href="https://www.thisiscolossal.com/page/3/">3</a><a class="page larger" title="Page 4" href="https://www.thisiscolossal.com/page/4/">4</a><span class='extend'>...</span><a class="nextpostslink" rel="next" href="https://www.thisiscolossal.com/page/2/">Next &gt;</a><a class="last" href="https://www.thisiscolossal.com/page/930/">&gt;&gt;</a>
                </div> </div>
            <div class="shop_highlight">
                <div class="shop_highlight_title"><p class="a_colossal"><em><span class="underline">A&nbsp;Colossal</span></em></p> <a href="https://colossalshop.com" target="_blank"><svg version="1.1" id="shop_sidebar" xmlns="httpswww.w3.org/2000/svg" xmlns:xlink="httpswww.w3.org/1999/xlink" x="0px" y="0px" viewBox="180 364.5 360 135" enable-background="new 180 364.5 360 135" xml:space="preserve"><g><polygon points="337.499,363.281 359.998,385.781 359.998,498.281 337.499,498.281 314.997,475.781 337.499,475.781" /><polygon points="292.498,363.281 314.997,385.781 314.997,408.281 292.498,408.281" /><polygon points="269.998,475.781 292.498,498.281 314.997,498.281 314.997,430.781 292.498,430.781 292.498,475.781" /><polygon points="224.998,453.281 224.998,430.781 179.999,430.781 202.499,453.281" /><polygon points="269.998,385.781 247.498,363.281 247.498,385.781 202.499,385.781 202.499,408.281 269.998,408.281" /><polygon points="269.998,430.781 247.498,408.281 247.498,475.781 179.999,475.781 202.499,498.281 269.998,498.281" /><rect x="382.5" y="385.781" width="22.5" height="67.5" /><polygon points="449.999,385.781 427.5,363.281 427.5,475.781 359.998,475.781 382.5,498.281 449.999,498.281" /><rect x="472.499" y="385.781" width="22.5" height="22.5" /><polygon points="517.499,363.281 539.998,385.781 539.998,453.281 494.999,453.281 494.999,498.281 472.499,498.281 449.999,475.781 472.499,475.781 472.499,430.781 517.499,430.781" /></g></svg></a> <p class="highlight"><em><span class="underline">Highlight</span></em></p><span class="stretch"></span></div><a href="https://colossalshop.com/products/stamp-bugs?utm_source=blog&utm_medium=banner&utm_content=Bugs&utm_campaign=Bugs"><img src="https://www.thisiscolossal.com/wp-content/uploads/2019/06/71Pp88u4RkL_1024x1024-1.jpg" alt="DIY Bug Stamp Kit" /></a><div class="shop_highlight_item"><h2><a href="https://colossalshop.com/products/stamp-bugs?utm_source=Colossal&utm_medium=Banner&utm_content=Bugs&utm_campaign=Bugs">DIY Bug Stamp Kit by Princeton Architectural Press</a></h2></div>
            </div>
            <div class="highlights">
                <h1>Editor's Picks: Photography</h1>
                <p class="small_caps">Highlights below. For the full collection <a href="/collections/photography">click here</a>.</p>

                <div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/09/dick-van-duijn-squirrel/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/09/squirrel-4-640x428@2x.jpg)" class="image"></a></span><h2><a href="https://www.thisiscolossal.com/2019/09/dick-van-duijn-squirrel/">Stop and Smell the Flowers: Dick van Duijn Captured a Squirrel&#8217;s Floral Delight</a></h2></div><div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/beauty-standards-and-albinism-by-justin-dingwall/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/25f93b83524511.5d40256f3c715-640x960.jpg)" class="image"></a></span><h2><a href="https://www.thisiscolossal.com/2019/08/beauty-standards-and-albinism-by-justin-dingwall/">Models with Albinism Challenge Standards of Beauty in Photographs by Justin Dingwall</a></h2></div><div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/stationary-milky-way-timelapse/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/Milkyway_01-640x360@2x.jpg)" class="image"></a></span><h2><a href="https://www.thisiscolossal.com/2019/08/stationary-milky-way-timelapse/">Earth&#8217;s Rotation Visualized in a Timelapse of the Milky Way Galaxy by Aryeh Nirenberg</a></h2></div><div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/passport-photos-max-siedentopf/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/MaxSiedentopf_01-640x452@2x.jpg)" class="image"></a></span><h2><a href="https://www.thisiscolossal.com/2019/08/passport-photos-max-siedentopf/">Passport Photos Widened to Reveal Unexpected Chaos Hiding Just Beyond of the Frame</a></h2></div><div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/08/afroart-photo-series/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/08/SFNov16-9747-Edit-640x960@2x.jpg)" class="image"></a></span><h2><a href="https://www.thisiscolossal.com/2019/08/afroart-photo-series/">AfroArt Photo Series Challenges Beauty Standards with Young Black Models</a></h2></div><div class="highlight_module"><span><a href="https://www.thisiscolossal.com/2019/02/andrew-mccarthy-high-def-moon-photography/" style="background-image:url(https://www.thisiscolossal.com/wp-content/uploads/2019/02/moon_crop-640x640@2x.jpg)" class="image"></a></span><h2><a href="https://www.thisiscolossal.com/2019/02/andrew-mccarthy-high-def-moon-photography/">50,000 Photographs Combine to Form a Detailed Image of the Moon and Stars</a></h2></div>
                <p><em><a href="/collections/photography" class="circle">See<br />the rest of the<br />Collection<br />››</a></em></p>
            </div>
        </main>
        <div class="clear"></div>
        <hr />
    </div>
    <div id='nectar_after_content' class='notblocked'>

        <div id='div-gpt-ad-1502230835991-2' class='control'>
            <script type="68fa48e47a44e50bd58b7be7-text/javascript">
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1502230835991-2'); });
    </script>
        </div>
        <hr />
    </div>

    <section id="trending">
        <div class="trending_module red_bk"><h1>Trending on Colossal</h1></div>
        <div class="divider"><span></span></div>
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/paris-musees-free-digital-artworks/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/paris-musees-1.jpg');" class="lazy" title="Paris Musées Releases 100,000 Images of Artworks for Unrestricted Public Use"></a></div>
        <div class="divider on_3 off_2"><span></span></div>
        <hr class="clear off_4 on_2">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2017/10/80-foot-steel-kraken/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2017/10/kodiak_queen_06.jpg');" class="lazy" title="An 80-Foot Steel Kraken Will Create an Artificial Coral Reef Near the British Virgin Islands"></a></div>
        <div class="divider on_4 off_3 on_2"><span></span></div>
        <hr class="clear off_4 on_3 off_2">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/undaily-bread-gregg-segal/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/segal-3.jpg');" class="lazy" title="Portraits of Venezuelan Families Reframe the Harrowing Journey of Immigrants"></a></div>
        <div class="divider off_4 on_3 off_2"><span></span></div>
        <hr class="clear on_4 off_3 on_2">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/fundraiser-australia-bushfire-crisis/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/art-9.jpg');" class="lazy" title="Fundraiser: Buy This Artwork and Support the Bushfire Crisis in Australia"></a></div>
        <div class="divider"><span></span></div>
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2016/01/museum-beetle-cleveland/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2016/01/beetle.jpg');" class="lazy" title="A VW Beetle Spotted in the Insect Collection at the Cleveland Museum of Natural History"></a></div>
        <div class="divider on_4 off_3"><span></span></div>
        <hr class="clear off_4 on_3">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/marble-pillows-by-hakon-anton-fageras/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/DSC1423b.jpg');" class="lazy" title="Realistic Pillows Sculpted from Blocks of White Marble by Håkon Anton Fagerås"></a></div>
        <div class="divider"><span></span></div>
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/microbe-puppets-judith-hope/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/bloop.gif');" class="lazy" title="Tiny Organisms Escape Life Under a Microscope in Oversized Puppets by Judith Hope"></a></div>
        <div class="divider off_4 on_3 off_2"><span></span></div>
        <hr class="clear on_4 off_3 on_2">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/dracula-bbc-one-billboard/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/dracula-outdoor-ad-2020.gif');" class="lazy" title="Teeth-Baring Dracula Reveals Himself on Sinister Billboard Only When the Sun Sets"></a></div>
        <div class="divider on_4 off_3 on_2"><span></span></div>
        <hr class="clear off_4 on_3 off_2">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/cy-bo-kenji-abe/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/abe-1.jpg');" class="lazy" title="Small Shapes Slot Together to Construct Vessels That Can Be Reconfigured"></a></div>
        <div class="divider off_2"><span></span></div>
        <hr class="clear off_4 off_3 on_2">
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/wood-tree-wade-kavanaugh-stephen-b-nguyen/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/kavanaugh-1.jpg');" class="lazy" title="Giant Ribbons of Wood Form Twisting Root Structures in Expansive Installation"></a></div>
        <div class="divider"><span></span></div>
        <div class="trending_module lazy"><a href="https://www.thisiscolossal.com/2020/01/sinister-sunrise-elias-chasiotis/" style="background-image:url('https://www.thisiscolossal.com/wp-content/uploads/2020/01/81137551_10218267837141478_873708141710147584_o.jpg');" class="lazy" title="Sinister Sunrise Captured by Photographer Elias Chasiotis During an Eclipse in Qatar"></a></div>
        <hr class="clear">
    </section>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js" type="68fa48e47a44e50bd58b7be7-text/javascript"></script>

    <script type="68fa48e47a44e50bd58b7be7-text/javascript">
  $(document).ready(function() {
    $("#toggle-full-layout").on('click', function(event){
      event.preventDefault();
      event.stopPropagation();
      Cookies.remove('co-index-view-style');
      location.reload();
    });
    $("#toggle-excerpt-layout").on('click', function(event){
      event.preventDefault();
      event.stopPropagation();
      Cookies.set('co-index-view-style', 'excerpt');
      location.reload();
    });
  })
</script>
    <footer>
        <div class="ruler"></div>
        <div id="row_01">
            <div class="logo">
                <a href="/"><svg version="1.1" id="logo_footer" xmlns="httpswww.w3.org/2000/svg" xmlns:xlink="httpswww.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 285 54" enable-background="new 0 0 285 54" xml:space="preserve"><path d="M17.812,35.628H8.906V8.906h17.813V0l8.906,8.906v8.906H17.812V35.628z M26.719,44.531H0l8.906,8.906h26.719v-8.906l-8.906-8.903V44.531z M62.344,0v44.531H35.625l8.906,8.906H71.25v-8.906V8.906L62.344,0z M97.97,44.531H71.25l8.906,8.906h26.719v-8.906l-8.905-8.903V44.531z M231.562,8.906h-8.909v8.905h8.909V8.906z M267.185,8.906L258.281,0v35.628h8.903V8.906z M160.312,35.628v-8.909H142.5l8.906,8.909H160.312z M124.688,8.906h-8.907v26.72h8.907V8.906z M89.062,8.906L80.156,0v35.628h8.907V8.906z M53.436,8.906h-8.905v26.72h8.905V8.906z M276.094,35.628v8.903h-26.719l8.906,8.906H285v-8.906L276.094,35.628z M222.653,26.719v17.812h-8.903l8.903,8.906h8.909v-8.906V26.719H222.653z M195.938,35.628v-8.909h-17.812l8.906,8.909H195.938z M213.75,8.906L204.844,0v8.906h-17.812v8.906h17.812h8.906V8.906z M169.219,17.812v26.719H142.5l8.906,8.906h26.719v-8.906V26.719L169.219,17.812z M142.5,8.906L133.594,0v44.531h-26.719l8.905,8.906h26.72v-8.906V26.719V8.906z M178.125,8.906L169.219,0v8.906h-17.812v8.906h17.812h8.906V8.906z M204.844,17.812v26.719h-26.719l8.906,8.906h26.719v-8.906V26.719L204.844,17.812z M240.469,0v44.531h-8.906l8.906,8.906h8.906v-8.906V8.906L240.469,0z" /></svg></a>
            </div>
            <div class="social">
                <a href="https://instagram.com/colossal" target="_blank" class="big_target social_media_instagram"></a>
                <a href="https://apple.news/TPte4elrHQ1O1tz8cUnLlpQ" target="_blank" class="big_target social_media_applenews"></a>
                <a href="https://facebook.com/thisiscolossal" target="_blank" class="big_target social_media_facebook"></a>
                <a href="https://twitter.com/colossal" target="_blank" class="big_target social_media_twitter"></a>
                <a href="https://pinterest.com/itscolossal/colossal/" target="_blank" class="big_target social_media_pinterest"></a>
                <span class="stretch off_large"></span>
                <a href="http://links.thisiscolossal.com/" target="_blank" class="big_target social_media_tumblr"></a>
                <a href="https://www.thisiscolossal.com/feed/" target="_blank" class="big_target social_media_rss"></a>
                <span class="stretch"></span>
            </div>
            <div class="clear"></div>
        </div>
        <div id="row_02">
            <div class="info">
                <div class="wrap"><a href="https://www.thisiscolossal.com/members/">Membership</a>
                    <span class="divider"></span>
                    <a href="https://www.thisiscolossal.com/about/">About</a>
                    <span class="divider"></span>
                    <a href="https://www.thisiscolossal.com/visual-archive/">Archive</a>
                    <span class="divider"></span>
                    <a href="https://colossal.art/projects/">Projects</a>
                    <span class="divider"></span>
                    <a href="https://www.thisiscolossal.com/contact/">Contact</a>
                    <span class="divider"></span>
                    <span class="stretch"></span></div> </div>
        </div>
        <div id="row_03">

            <div class="mailing_list_footer">
                <div id="mc_embed_signup" style="margin:40px 0 0 0;">
                    <form action="//thisiscolossal.us5.list-manage.com/subscribe/post?u=d6c1d1d7c7f055e30321f9a6a&amp;id=2f67bf3f83" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="frequency">
                                <h2 class="title">Join Our Mailing List</h2>
                            </div>
                            <div class="subscriber">
                                <div class="mc-field-group">
                                    <label for="mce-FNAME">First Name </label>
                                    <input type="text" value="" name="FNAME" class="required input_text" id="mce-FNAME" placeholder="First Name">
                                </div>
                                <div class="mc-field-group">
                                    <label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
                                    <input type="email" value="" name="EMAIL" class="required email input_text" id="mce-EMAIL" placeholder="E-mail address">
                                </div>
                                <input type="submit" value="Join" name="subscribe" id="mc-embedded-subscribe" class="button input_text">
                                <div class="clear"></div>
                            </div>
                            <div class="mc-field-group input-group" style="display:none">
                                <strong>Art in Your Inbox from Colossal </strong>
                                <ul>
                                    <li><input type="checkbox" value="2" name="group[9][2]" id="mce-group[9]-9-0" checked><label for="mce-group[9]-9-0">Colossal Daily Update (weekdays)</label></li>
                                    <li><input type="checkbox" value="1" name="group[9][1]" id="mce-group[9]-9-1"><label for="mce-group[9]-9-1">Best of Colossal Weekly Art Digest (Tuesdays)</label></li>
                                    <li><input type="checkbox" value="8" name="group[9][8]" id="mce-group[9]-9-2"><label for="mce-group[9]-9-2">Five Things You Should See in NYC (Thursdays)</label></li>
                                    <li><input type="checkbox" value="16" name="group[9][16]" id="mce-group[9]-9-3"><label for="mce-group[9]-9-3">Five Things You Should See in LA (Thursdays)</label></li>
                                    <li><input type="checkbox" value="32" name="group[9][32]" id="mce-group[9]-9-4"><label for="mce-group[9]-9-4">Five Things You Should See in London (Thursdays)</label></li>
                                    <li><input type="checkbox" value="64" name="group[9][64]" id="mce-group[9]-9-5"><label for="mce-group[9]-9-5">Five Things You Should See in Chicago (Thursdays)</label></li>
                                    <li><input type="checkbox" value="4" name="group[9][4]" id="mce-group[9]-9-6"><label for="mce-group[9]-9-6">The Colossal Shop (occasional)</label></li>
                                </ul>
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>
                            <a href="#" onclick="if (!window.__cfRLUnblockHandlers) return false; hide('mce-success-response')" class="mailing_list_x input_text" data-cf-modified-68fa48e47a44e50bd58b7be7-="">&times;</a>
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d6c1d1d7c7f055e30321f9a6a_2f67bf3f83" tabindex="-1" value=""></div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="shop_footer">
                <a href="https://www.thisiscolossal.com/members/"><svg version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="200px" height="38px" viewBox="0 0 200 38" style="enable-background:new 0 0 200 38;" xml:space="preserve"><path class="st0" d="M22.6,20.1c-0.2,0.1-0.4,0.2-0.6,0.3c-0.2,0.1-0.4,0.1-0.6,0.1c-0.9-0.6-1.3-1.7-1.3-3.3s0.3-3.6,1-6c0.1-0.1,0.2-0.2,0.2-0.4l-0.3-0.3c-0.1,0-0.2,0-0.2,0c-0.1,0-0.1,0.1-0.2,0.2c-0.6,0.4-1.2,0.9-1.9,1.3c-0.7,0.5-1.4,0.9-2.1,1.3c-0.7,0.4-1.5,0.7-2.2,0.9c-0.8,0.2-1.5,0.4-2.2,0.4c-1.8,0-3.2-0.9-4.1-2.8l-0.3-0.5l-0.3,0.4c-0.1,0.1-0.2,0.3-0.3,0.5c-0.1,0.3-0.3,0.6-0.4,0.9c-0.1,0.4-0.2,0.7-0.3,1.1c-0.1,0.4-0.1,0.7-0.1,0.9c-0.2,0.2-0.4,0.4-0.6,0.7c-0.2,0.3-0.4,0.6-0.5,0.9c0,0.2,0,0.3,0.1,0.5c0,0.2-0.1,0.4-0.3,0.5c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0.1-0.2,0.2-0.2,0.3c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.2,0,0.3c0,0.2,0,0.3-0.1,0.4c-0.4,0.8-0.7,1.3-0.9,1.5c-0.2,0.2-0.5,0.3-0.9,0.3c-0.2,0-0.5,0-0.9-0.1c-0.3-0.3-0.5-0.7-0.8-1.3c-0.2-0.5-0.4-1.1-0.5-1.7C0,17.5,0,16.9,0,16.3c0-0.6,0.1-1.2,0.3-1.6c0,0,0.1-0.2,0.3-0.7c0.2-0.4,0.4-1,0.7-1.6c0.3-0.7,0.6-1.4,0.9-2.2c0.4-0.8,0.7-1.6,1.1-2.3s0.7-1.5,1-2.1c0.3-0.6,0.6-1.1,0.8-1.4C5.2,4,5.4,3.8,5.5,3.6c0.1-0.2,0.3-0.5,0.4-0.7c0.4-0.7,0.8-1.2,1.1-1.7C7.4,0.8,8,0.4,8.7,0.1c0.5,0.2,0.8,0.4,1.1,0.7l0.1,0.4c0.1,0.2,0.2,0.3,0.4,0.3c0.2,0.1,0.4,0.1,0.6,0.2c0.2,0.1,0.3,0.1,0.5,0.1c0.2,0.2,0.3,0.4,0.4,0.6c0.2,0.3,0.3,0.5,0.5,0.8c0.2,0.3,0.3,0.5,0.4,0.8c0.1,0.3,0.2,0.5,0.2,0.8c0,0.3,0,0.5-0.1,0.8l0,0.3c0.1,0.2,0.1,0.5,0.1,0.8c0,0.2,0,0.3,0,0.5c0,0.2,0,0.3,0.1,0.5c0.4,0.3,0.7,0.5,1.1,0.5c0.4,0,0.7-0.1,1-0.2c0.3-0.2,0.6-0.3,0.8-0.5c0.2-0.1,0.4-0.2,0.5-0.3C16.4,7,16.6,7,16.7,6.9c0.4-0.4,0.8-0.7,1.1-1c0.3-0.3,0.7-0.6,1-0.9c0.7-0.5,1.3-1.1,1.9-1.7c0.6-0.6,1.1-1.4,1.5-2.3c0.2-0.1,0.4-0.2,0.7-0.2c0.2,0,0.5,0.1,0.8,0.2c0.3,0.1,0.5,0.2,0.8,0.3c0.1,0,0.1,0.1,0.2,0.1c0.1,0,0.1,0,0.1,0.1c0,0.2,0.1,0.4,0.3,0.6c0.1,0.1,0.1,0.1,0.2,0.1c0,0,0.1,0,0.1,0.1c0.3,0.2,0.5,0.4,0.6,0.5c0.2,0.5,0.4,0.9,0.8,1.2c0,0.2,0,0.4,0,0.6C26.8,4.8,26.9,5,27,5.2c0,0.1,0,0.1,0.1,0.2c0,0.1,0.1,0.1,0.1,0.2l-0.6-0.1l0.1,0.6c0,0.3,0,0.6-0.1,1c-0.1,0.4-0.2,0.7-0.3,0.9c0,0.6,0,1,0,1.3c0,0.3,0,0.6,0,0.9c0,0.3-0.1,0.5-0.2,0.8c-0.1,0.2-0.2,0.5-0.3,0.8c0.1,1.4-0.1,2.7-0.5,3.8c-0.4,1.2-0.8,2.2-1.3,3.2c0,0.1-0.1,0.2-0.1,0.3c-0.1,0.1-0.1,0.2-0.1,0.3c-0.2,0.1-0.4,0.1-0.5,0.2C22.9,19.9,22.8,20,22.6,20.1z M46.3,2.1c0,0.2,0,0.3,0,0.3c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2s0,0.2,0.1,0.3c-0.5,0.7-1,1.3-1.6,1.5c-0.6,0.3-1.3,0.5-2,0.6c-0.7,0.1-1.4,0.2-2.2,0.4c-0.7,0.1-1.5,0.4-2.2,0.9c-0.3-0.1-0.5-0.1-0.8,0c-0.2,0.1-0.6,0.1-1,0.1c-0.1,0.1-0.3,0.2-0.5,0.3C36,7,35.8,7.1,35.6,7.1c0.1,0.2,0.1,0.4,0.1,0.6c0,0.2,0,0.4,0.1,0.5c0.8,0.1,1.6,0.1,2.6,0c0.9-0.1,1.8-0.1,2.7-0.2c0,0.1,0,0.2,0,0.2c0,0-0.1,0.1,0,0.2c0.2,0.1,0.3,0.2,0.5,0.4C41.6,9,41.8,9.2,42,9.4c0.2,0.2,0.3,0.3,0.5,0.4c0.2,0.1,0.4,0.1,0.6,0.1c0.1,0.2,0.1,0.4,0.2,0.6c0,0.2,0,0.4-0.1,0.6c-0.4,0.2-0.7,0.5-1.1,0.7c-0.4,0.3-0.7,0.5-1.1,0.7c-0.4,0.2-0.8,0.4-1.2,0.5c-0.4,0.1-0.9,0.2-1.4,0.1c-0.1,0-0.2,0-0.2,0s-0.1,0-0.2-0.1c-0.2,0.1-0.4,0.1-0.7,0.1c-0.3,0-0.6,0-0.7,0.1c-0.2-0.1-0.5-0.2-0.9-0.1c-0.3,0-0.6,0-0.9-0.1c-0.4,0.5-0.7,1-1,1.5c-0.3,0.5-0.5,1.1-0.8,1.7c0.7,0.1,1.4,0.2,1.9,0.1c0.5,0,1-0.1,1.4-0.1c0.4-0.1,0.9-0.1,1.4-0.2c0.5-0.1,1-0.1,1.7,0c0.4-0.2,1-0.4,1.6-0.5c0.6-0.1,1.2-0.2,1.7-0.4c0.2,0.1,0.3,0.2,0.5,0.3c0.1,0.2,0.3,0.3,0.4,0.5c0.1,0.1,0.2,0.3,0.4,0.4c0.1,0.1,0.3,0.2,0.5,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0.1,0.2,0.1,0.3c0.1,0.1,0.1,0.1,0.3,0.1c-0.3,0.7-0.7,1.3-1.1,1.7c-0.4,0.4-0.9,0.7-1.5,0.9c-0.5,0.2-1.2,0.4-1.8,0.5c-0.7,0.1-1.4,0.2-2.2,0.4c-0.2,0-0.4,0-0.5,0c-0.1-0.1-0.3-0.1-0.5-0.1c-0.5,0.1-1.1,0.2-1.8,0.3c-0.7,0.1-1.4,0.1-2.1,0.1c-0.7,0-1.4-0.1-2.1-0.3c-0.7-0.2-1.2-0.5-1.6-0.9c-0.2-0.2-0.3-0.3-0.3-0.5c-0.1-0.2-0.2-0.4-0.4-0.5c0-0.3-0.1-0.6-0.1-1c-0.1-0.4-0.1-0.7-0.2-1.1c-0.1-0.4-0.1-0.8-0.1-1.1c0-0.4,0-0.7,0-1c0.1-0.3,0.2-0.6,0.4-0.9c0.2-0.3,0.3-0.5,0.2-0.9c0.1,0,0.2-0.1,0.2-0.2c0-0.1,0.1-0.1,0.3-0.1c0.1-0.4,0.3-0.8,0.4-1.1c0.2-0.4,0.3-0.7,0.5-1c0.2-0.3,0.3-0.7,0.5-1c0.2-0.3,0.3-0.7,0.5-1.1c-0.3-0.3-0.5-0.6-0.7-0.9c-0.1-0.3-0.2-0.7-0.3-1c-0.1-0.3-0.1-0.7-0.2-1.1c0-0.4-0.1-0.7-0.2-1.1c0.2-0.3,0.5-0.6,0.8-0.8c0.3-0.2,0.7-0.4,1-0.5c0.4-0.1,0.8-0.2,1.2-0.3c0.4-0.1,0.9-0.2,1.4-0.3C35.3,2,36,1.8,36.7,1.6c0.7-0.2,1.4-0.3,2-0.4c0.7-0.1,1.4-0.2,2.1-0.3s1.5-0.2,2.5-0.4c0.2,0.2,0.4,0.3,0.6,0.3c0.1-0.2,0.2-0.3,0.3-0.2c0.1,0.1,0.2,0,0.1-0.2c0.2,0.4,0.5,0.7,0.8,0.9C45.5,1.7,45.9,1.9,46.3,2.1z M69,20.1c-0.2,0.1-0.4,0.2-0.6,0.3c-0.2,0.1-0.4,0.1-0.6,0.1c-0.9-0.6-1.3-1.7-1.3-3.3s0.3-3.6,1-6c0.1-0.1,0.2-0.2,0.2-0.4l-0.3-0.3c-0.1,0-0.2,0-0.2,0c-0.1,0-0.1,0.1-0.2,0.2c-0.6,0.4-1.2,0.9-1.9,1.3c-0.7,0.5-1.4,0.9-2.1,1.3c-0.7,0.4-1.5,0.7-2.2,0.9c-0.8,0.2-1.5,0.4-2.2,0.4c-1.8,0-3.2-0.9-4.1-2.8L54,11.2l-0.3,0.4c-0.1,0.1-0.2,0.3-0.3,0.5c-0.1,0.3-0.3,0.6-0.4,0.9c-0.1,0.4-0.2,0.7-0.3,1.1c-0.1,0.4-0.1,0.7-0.1,0.9c-0.2,0.2-0.4,0.4-0.6,0.7c-0.2,0.3-0.4,0.6-0.5,0.9c0,0.2,0,0.3,0.1,0.5c0,0.2-0.1,0.4-0.3,0.5c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0.1-0.2,0.2-0.2,0.3c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.2,0,0.3c0,0.2,0,0.3-0.1,0.4c-0.4,0.8-0.7,1.3-0.9,1.5c-0.2,0.2-0.5,0.3-0.9,0.3c-0.2,0-0.5,0-0.9-0.1c-0.3-0.3-0.5-0.7-0.8-1.3c-0.2-0.5-0.4-1.1-0.5-1.7c-0.1-0.6-0.2-1.2-0.2-1.9c0-0.6,0.1-1.2,0.3-1.6c0,0,0.1-0.2,0.3-0.7c0.2-0.4,0.4-1,0.7-1.6c0.3-0.7,0.6-1.4,0.9-2.2c0.4-0.8,0.7-1.6,1.1-2.3c0.4-0.8,0.7-1.5,1-2.1c0.3-0.6,0.6-1.1,0.8-1.4c0.2-0.3,0.3-0.5,0.5-0.7c0.1-0.2,0.3-0.5,0.4-0.7c0.4-0.7,0.8-1.2,1.1-1.7c0.4-0.4,0.9-0.8,1.6-1.1c0.5,0.2,0.8,0.4,1.1,0.7l0.1,0.4c0.1,0.2,0.2,0.3,0.4,0.3c0.2,0.1,0.4,0.1,0.6,0.2c0.2,0.1,0.3,0.1,0.5,0.1C57.9,2,58,2.2,58.2,2.5c0.2,0.3,0.3,0.5,0.5,0.8C58.8,3.5,59,3.7,59.1,4c0.1,0.3,0.2,0.5,0.2,0.8c0,0.3,0,0.5-0.1,0.8l0,0.3c0.1,0.2,0.1,0.5,0.1,0.8c0,0.2,0,0.3,0,0.5c0,0.2,0,0.3,0.1,0.5c0.4,0.3,0.7,0.5,1.1,0.5c0.4,0,0.7-0.1,1-0.2c0.3-0.2,0.6-0.3,0.8-0.5c0.2-0.1,0.4-0.2,0.5-0.3C62.8,7,63,7,63.1,6.9c0.4-0.4,0.8-0.7,1.1-1c0.3-0.3,0.7-0.6,1-0.9c0.7-0.5,1.3-1.1,1.9-1.7c0.6-0.6,1.1-1.4,1.5-2.3c0.2-0.1,0.4-0.2,0.7-0.2c0.2,0,0.5,0.1,0.8,0.2s0.5,0.2,0.8,0.3c0.1,0,0.1,0.1,0.2,0.1c0.1,0,0.1,0,0.1,0.1c0,0.2,0.1,0.4,0.3,0.6c0.1,0.1,0.1,0.1,0.2,0.1c0,0,0.1,0,0.1,0.1c0.3,0.2,0.5,0.4,0.6,0.5c0.2,0.5,0.4,0.9,0.8,1.2c0,0.2,0,0.4,0,0.6c0.1,0.2,0.1,0.4,0.2,0.6c0,0.1,0,0.1,0.1,0.2c0,0.1,0.1,0.1,0.1,0.2l-0.6-0.1L73,6.2c0,0.3,0,0.6-0.1,1c-0.1,0.4-0.2,0.7-0.3,0.9c0,0.6,0,1,0,1.3c0,0.3,0,0.6,0,0.9c0,0.3-0.1,0.5-0.2,0.8c-0.1,0.2-0.2,0.5-0.3,0.8c0.1,1.4-0.1,2.7-0.5,3.8c-0.4,1.2-0.8,2.2-1.3,3.2c0,0.1-0.1,0.2-0.1,0.3c-0.1,0.1-0.1,0.2-0.1,0.3c-0.2,0.1-0.4,0.1-0.5,0.2C69.3,19.9,69.2,20,69,20.1z M93.8,15.3c0.1,0.1,0.2,0.2,0.2,0.3c0,0.2,0,0.3,0,0.4c-0.4,0.5-0.7,0.9-0.8,1.1c-0.2,0.2-0.3,0.3-0.3,0.4c-0.1,0.1-0.1,0.2-0.2,0.2c-0.1,0.1-0.2,0.2-0.5,0.4c-1,0.6-2,1.1-3,1.6c-1,0.5-2.1,0.8-3.3,1c-0.2,0.2-0.5,0.4-1,0.5c-0.5,0.1-0.9,0.2-1.4,0.3c-0.1,0-0.1-0.1-0.1-0.2c0-0.1-0.1-0.1-0.1-0.2c0,0.2-0.1,0.3-0.3,0.3c-0.2,0-0.4,0-0.6-0.1c-0.3-0.1-0.5-0.2-0.8-0.4c-0.3-0.2-0.5-0.3-0.7-0.4c-0.1,0.2-0.2,0.4-0.3,0.5c-0.1,0.2-0.3,0.3-0.4,0.4c-0.2,0-0.4-0.1-0.5-0.1c-0.1,0-0.2,0-0.3,0.1c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.2-0.2-0.2-0.2c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.2-0.1-0.4-0.2c0-0.1,0-0.2-0.1-0.4c-0.1-0.2-0.2-0.3-0.4-0.3c0.1-0.2,0-0.3-0.1-0.6c-0.2-0.2-0.3-0.5-0.4-0.8c0-0.2,0.1-0.5,0.1-0.8c0-0.3,0.2-0.4,0.4-0.5c0-0.2-0.1-0.3-0.1-0.4c0-0.1-0.1-0.2-0.1-0.3c0.2-0.1,0.3-0.3,0.3-0.5c0.1-0.2,0.1-0.4,0.2-0.6c0-0.2,0.1-0.4,0.2-0.6c0.1-0.2,0.2-0.4,0.4-0.5c0.2-0.7,0.4-1.1,0.5-1.5c0.1-0.3,0.2-0.6,0.2-0.9c0.1-0.3,0.1-0.6,0.2-1c0.1-0.4,0.2-0.8,0.4-1.3c0-0.3,0-0.6,0.1-0.9c0.1-0.3,0.2-0.5,0.3-0.8c0.1-0.3,0.2-0.5,0.2-0.8c0.1-0.3,0.1-0.6,0-1c-0.3,0-0.5,0.1-0.7,0.3c-0.1,0.2-0.2,0.4-0.3,0.7c-0.2,0.2-0.4,0.3-0.6,0.2c-0.3,0-0.5,0-0.7,0.1c-0.8-0.3-1.4-0.7-1.9-1.3S75,5,74.7,4.2c0.2-0.5,0.5-0.9,0.9-1.3c0.4-0.3,0.8-0.6,1.3-0.9c0.5-0.2,1-0.4,1.6-0.6c0.6-0.1,1.1-0.3,1.6-0.4C81,1,81.9,0.8,82.7,0.6c0.9-0.2,1.8-0.3,2.8-0.2c0.1-0.1,0.3-0.1,0.5-0.1c0.2,0,0.4,0,0.6,0s0.4,0,0.6,0c0.2,0,0.3,0,0.5-0.1c0.1,0.1,0.3,0.2,0.5,0.2c0.2,0,0.4,0.1,0.7,0.3c0.3-0.2,0.5-0.2,0.7-0.1C89.6,0.7,89.8,0.8,90,1c0.2,0.1,0.3,0.3,0.4,0.4c0.1,0.1,0.3,0.2,0.4,0.2C91,1.7,91.2,1.9,91.4,2s0.4,0.4,0.7,0.5c0.2,0.2,0.4,0.4,0.5,0.6c0.1,0.2,0.2,0.5,0.2,0.8c0,0.1,0,0.2,0.1,0.2c0.1,0,0.2,0.1,0.2,0.1C93.1,5,93,5.5,92.8,6S92.3,7,92,7.4c-0.3,0.4-0.7,0.8-1.1,1.2c-0.4,0.4-0.8,0.8-1.1,1.2c0.2,0.3,0.5,0.5,0.8,0.8c0.3,0.2,0.6,0.4,0.9,0.7c0.3,0.2,0.6,0.5,0.8,0.7c0.3,0.2,0.5,0.5,0.7,0.9c0,0,0.1,0.1,0.2,0.1s0.1,0.1,0.2,0.1c0.1,0.2,0.2,0.4,0.3,0.7c0.1,0.2,0.2,0.5,0.3,0.7c0.1,0.2,0.1,0.4,0.1,0.6C93.9,15,93.9,15.2,93.8,15.3z M88,4.3c-1.1,0-2.2,0.1-3.3,0.3c-1.1,0.2-2,0.6-2.8,1c0.4,0.3,0.8,0.6,1.1,0.8c0.4,0.2,0.7,0.6,0.9,1c0.9-0.4,1.6-0.8,2.3-1.3C86.8,5.5,87.4,4.9,88,4.3z M88.4,14.8c0.1-0.2,0.2-0.4,0.4-0.5c0.2-0.1,0.4-0.3,0.5-0.4c0.2-0.1,0.3-0.3,0.5-0.4c0.1-0.1,0.2-0.3,0.2-0.6c-0.7-0.4-1.3-0.6-1.9-0.7c-0.6-0.1-1.1-0.1-1.6,0c-0.5,0.1-1,0.2-1.5,0.3c-0.5,0.2-1,0.3-1.4,0.3c-0.2,0.6-0.5,1.3-0.8,2.1c-0.3,0.7-0.5,1.5-0.8,2.2c0.7-0.1,1.3-0.2,1.8-0.4c0.5-0.2,1.1-0.4,1.6-0.6c0.5-0.2,1-0.4,1.5-0.7C87.3,15.2,87.9,15,88.4,14.8z M114.1,2.1c0,0.2,0,0.3,0,0.3c0,0.1,0,0.1,0,0.2c0,0.1,0,0.1,0,0.2s0,0.2,0.1,0.3c-0.5,0.7-1,1.3-1.6,1.5c-0.6,0.3-1.3,0.5-2,0.6c-0.7,0.1-1.4,0.2-2.2,0.4c-0.7,0.1-1.5,0.4-2.2,0.9c-0.3-0.1-0.5-0.1-0.8,0c-0.2,0.1-0.6,0.1-1,0.1c-0.1,0.1-0.3,0.2-0.5,0.3c-0.2,0.1-0.4,0.1-0.6,0.2c0.1,0.2,0.1,0.4,0.1,0.6c0,0.2,0,0.4,0.1,0.5c0.8,0.1,1.6,0.1,2.6,0c0.9-0.1,1.8-0.1,2.7-0.2c0,0.1,0,0.2,0,0.2c0,0-0.1,0.1,0,0.2c0.2,0.1,0.3,0.2,0.5,0.4c0.2,0.2,0.3,0.3,0.5,0.5c0.2,0.2,0.3,0.3,0.5,0.4c0.2,0.1,0.4,0.1,0.6,0.1c0.1,0.2,0.1,0.4,0.2,0.6c0,0.2,0,0.4-0.1,0.6c-0.4,0.2-0.7,0.5-1.1,0.7c-0.4,0.3-0.7,0.5-1.1,0.7c-0.4,0.2-0.8,0.4-1.2,0.5c-0.4,0.1-0.9,0.2-1.4,0.1c-0.1,0-0.2,0-0.2,0s-0.1,0-0.2-0.1c-0.2,0.1-0.4,0.1-0.7,0.1c-0.3,0-0.6,0-0.7,0.1c-0.2-0.1-0.5-0.2-0.9-0.1c-0.3,0-0.6,0-0.9-0.1c-0.4,0.5-0.7,1-1,1.5c-0.3,0.5-0.5,1.1-0.8,1.7c0.7,0.1,1.4,0.2,1.9,0.1c0.5,0,1-0.1,1.4-0.1c0.4-0.1,0.9-0.1,1.4-0.2c0.5-0.1,1-0.1,1.7,0c0.4-0.2,1-0.4,1.6-0.5c0.6-0.1,1.2-0.2,1.7-0.4c0.2,0.1,0.3,0.2,0.5,0.3c0.1,0.2,0.3,0.3,0.4,0.5c0.1,0.1,0.2,0.3,0.4,0.4c0.1,0.1,0.3,0.2,0.5,0.1c0.1,0.1,0.1,0.2,0.2,0.3c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0.1,0.2,0.1,0.3c0.1,0.1,0.1,0.1,0.3,0.1c-0.3,0.7-0.7,1.3-1.1,1.7c-0.4,0.4-0.9,0.7-1.5,0.9c-0.5,0.2-1.2,0.4-1.8,0.5c-0.7,0.1-1.4,0.2-2.2,0.4c-0.2,0-0.4,0-0.5,0c-0.1-0.1-0.3-0.1-0.5-0.1c-0.5,0.1-1.1,0.2-1.8,0.3c-0.7,0.1-1.4,0.1-2.1,0.1c-0.7,0-1.4-0.1-2.1-0.3c-0.7-0.2-1.2-0.5-1.6-0.9c-0.2-0.2-0.3-0.3-0.3-0.5c-0.1-0.2-0.2-0.4-0.4-0.5c0-0.3-0.1-0.6-0.1-1c-0.1-0.4-0.1-0.7-0.2-1.1c-0.1-0.4-0.1-0.8-0.1-1.1c0-0.4,0-0.7,0-1c0.1-0.3,0.2-0.6,0.4-0.9c0.2-0.3,0.3-0.5,0.2-0.9c0.1,0,0.2-0.1,0.2-0.2c0-0.1,0.1-0.1,0.3-0.1c0.1-0.4,0.3-0.8,0.4-1.1c0.2-0.4,0.3-0.7,0.5-1c0.2-0.3,0.3-0.7,0.5-1c0.2-0.3,0.3-0.7,0.5-1.1c-0.3-0.3-0.5-0.6-0.7-0.9c-0.1-0.3-0.2-0.7-0.3-1C98,6.1,98,5.7,97.9,5.4c0-0.4-0.1-0.7-0.2-1.1c0.2-0.3,0.5-0.6,0.8-0.8c0.3-0.2,0.7-0.4,1-0.5c0.4-0.1,0.8-0.2,1.2-0.3c0.4-0.1,0.9-0.2,1.4-0.3c0.9-0.3,1.7-0.5,2.4-0.7c0.7-0.2,1.4-0.3,2-0.4c0.7-0.1,1.4-0.2,2.1-0.3c0.7-0.1,1.5-0.2,2.5-0.4c0.2,0.2,0.4,0.3,0.6,0.3c0.1-0.2,0.2-0.3,0.3-0.2s0.2,0,0.1-0.2c0.2,0.4,0.5,0.7,0.8,0.9C113.3,1.7,113.7,1.9,114.1,2.1z M119.6,17.2c-0.2,0.9-0.5,1.5-0.6,2c-0.2,0.5-0.5,0.9-0.8,1.4c-0.3,0-0.5,0-0.6,0.1s-0.2,0.2-0.4,0.3c-0.2,0-0.3,0-0.4,0c-0.1,0-0.2-0.1-0.2-0.1c-0.1-0.1-0.1-0.1-0.2-0.2c-0.1-0.1-0.2-0.1-0.4-0.1c0-0.2-0.1-0.3-0.1-0.5c-0.1-0.2-0.2-0.2-0.4-0.2c0-0.2,0.1-0.3,0-0.4c0-0.1,0-0.2-0.1-0.3c0-0.1-0.1-0.2-0.1-0.3c0-0.1,0-0.2,0-0.4c-0.1,0-0.2-0.1-0.3-0.2c-0.1-0.2-0.1-0.3-0.2-0.5c0.1-0.4,0.1-0.8,0.2-1.1c0.1-0.4,0.1-0.7,0.2-1c0.1-0.3,0.2-0.6,0.2-0.9c0.1-0.3,0-0.6-0.1-1c0.1,0.1,0.2,0,0.2,0c0-0.1,0.1-0.2,0.1-0.4c0-0.2,0-0.3,0-0.5s0-0.3,0-0.4c0.2-0.6,0.4-1.2,0.5-1.8s0.3-1.2,0.4-1.8c0.1-0.6,0.2-1.2,0.4-1.7c0.1-0.6,0.3-1.1,0.5-1.6c0.2,0,0.5,0,0.8,0.1c0.3,0.1,0.5,0.2,0.5,0.4c0.6-0.4,1.1-1,1.5-1.6c0.4-0.6,0.9-1.2,1.4-1.8c0.5-0.6,1-1.1,1.5-1.6c0.6-0.5,1.3-0.7,2.1-0.8c0.4,0.3,0.9,0.7,1.5,1c0.5,0.3,1,0.8,1.3,1.3c0.2,0.1,0.3,0.2,0.4,0.3c0.1,0.1,0.3,0.2,0.4,0.3v0.4c0.3,0.2,0.5,0.5,0.6,0.8s0.3,0.7,0.4,1c0.1,0.4,0.3,0.7,0.4,1.1c0.1,0.4,0.3,0.7,0.5,1c0,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.2,0,0.2c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0,0.2,0,0.4c-0.1,1-0.4,1.7-0.9,2.3c-0.6,0.6-1.2,1.1-2,1.5c-0.8,0.4-1.6,0.7-2.5,1c-0.9,0.3-1.7,0.5-2.4,0.9c0,0.1,0,0.1,0.1,0.1c0.1,0,0.1,0.1,0.1,0.2c0.5,0.3,0.9,0.6,1.3,0.8c0.3,0.2,0.6,0.4,0.9,0.5c0.3,0.1,0.6,0.2,0.9,0.3c0.3,0.1,0.8,0.2,1.3,0.3c0.6,0.1,1,0.2,1.3,0.3c0.3,0.1,0.6,0.1,0.9,0.2c0.3,0.1,0.5,0.1,0.8,0.1c0.3,0,0.7,0.1,1.2,0.1c0.2,0.2,0.4,0.4,0.5,0.7c0.1,0.3,0.2,0.5,0.3,0.8c0,0.3,0.1,0.5,0,0.8c0,0.3-0.1,0.5-0.2,0.6c0,0.1,0.1,0.1,0.1,0.1c0.1,0,0.2,0,0.2,0c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0.1-0.2,0.3-0.3,0.4c-0.1,0.1-0.2,0.2-0.3,0.3c-0.1,0.1-0.3,0.2-0.5,0.2c-0.7,0.3-1.5,0.3-2.6,0c-1-0.2-2.3-0.7-3.9-1.3c-0.8-0.3-1.4-0.6-1.9-0.8c-0.5-0.2-0.9-0.5-1.3-0.7c-0.4-0.2-0.7-0.5-1-0.7C120.4,17.9,120,17.6,119.6,17.2z M124.3,5.9c-0.2,0.4-0.5,0.7-0.8,1c-0.3,0.3-0.6,0.6-0.9,0.9c-0.3,0.3-0.5,0.6-0.7,1c-0.2,0.4-0.4,0.8-0.5,1.3c0.8-0.1,1.6-0.3,2.4-0.6c0.8-0.3,1.6-0.7,2.4-1c-0.1-0.7-0.3-1.2-0.7-1.6C125.1,6.3,124.7,6.1,124.3,5.9z M133.7,5.8c0.2-0.5,0.4-0.9,0.7-1.2c0.3-0.3,0.7-0.6,1.1-0.8c0.4-0.2,0.8-0.4,1.3-0.6c0.4-0.2,0.9-0.3,1.3-0.4c0.9-0.4,1.8-0.7,2.8-1.1s2-0.6,3.1-0.9c1-0.3,2.1-0.4,3.2-0.5c1.1-0.1,2.2,0.1,3.5,0.4c0.1,0.2,0.2,0.4,0.3,0.6c0.1,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.3,0.4,0.4,0.6c0.1,0.2,0.2,0.4,0.2,0.6c0.2,0.2,0.4,0.4,0.5,0.6s0.2,0.5,0.1,0.9c-0.2,0.2-0.4,0.3-0.6,0.5c-0.2,0.1-0.4,0.3-0.7,0.4c-0.8-0.2-1.6-0.3-2.5-0.2s-1.6,0.1-2.2,0.2c-0.2-0.2-0.4-0.3-0.7-0.3c-0.3,0-0.6,0-0.9,0.1c-0.3,0.1-0.6,0.2-0.9,0.3s-0.6,0.1-0.8,0.1C143,5.8,142.7,6,142.4,6c-0.3,0.1-0.7,0.1-1,0.1c0,0.1,0,0.2,0.1,0.2c0.1,0,0.1,0.1,0,0.2c0.1,0.2,0.3,0.3,0.5,0.3c0.2,0.1,0.4,0.1,0.7,0.2c0.2,0.1,0.4,0.1,0.6,0.2c0.2,0.1,0.3,0.3,0.4,0.5c0.3,0,0.6,0.1,0.9,0.2c0.2,0.2,0.4,0.3,0.6,0.6c0.2,0.2,0.3,0.5,0.3,0.7c0.1,0.3,0.1,0.5,0.1,0.7c0.1,0.2,0.1,0.3,0.2,0.2c0.1,0,0.2,0,0.3,0.1c0.1,0.3,0.2,0.6,0.5,0.9c0.3,0.3,0.5,0.6,0.6,1c0,0.5,0.1,1,0.1,1.5c0,0.4-0.1,0.9-0.2,1.3c-0.3,0.8-0.8,1.5-1.4,2.2c-0.6,0.6-1.2,1.2-2,1.7c-0.7,0.5-1.5,0.9-2.4,1.3c-0.8,0.4-1.6,0.6-2.4,0.9c-0.1,0-0.2,0-0.3,0s-0.1,0-0.1-0.2c-0.2,0.2-0.4,0.3-0.6,0.3c-0.2,0-0.4,0.1-0.7,0c-0.2,0-0.5,0-0.7,0c-0.3,0-0.5,0-0.7,0c-0.2-0.2-0.3-0.3-0.4-0.4c-0.1-0.1-0.2-0.3-0.3-0.5c-0.1,0-0.2-0.1-0.1-0.2c0-0.1,0-0.2-0.1-0.2c0.1-0.2,0.2-0.3,0.2-0.4c0,0,0-0.1-0.1-0.1s-0.1,0-0.1-0.1c0,0-0.1-0.1,0-0.1c0.1-0.2,0.1-0.4,0.1-0.6c0-0.2,0-0.5-0.1-0.7c0-0.2,0-0.5,0-0.7c0.1-0.2,0.2-0.4,0.5-0.5c0.7,0.1,1.5,0.1,2.1-0.1c0.7-0.1,1.3-0.3,1.9-0.6c0.6-0.3,1.2-0.5,1.7-0.9c0.5-0.3,1.1-0.6,1.6-0.8c0.1-0.2,0.2-0.4,0.3-0.5c0.1-0.1,0.2-0.3,0.4-0.4c0.1-0.1,0.3-0.2,0.4-0.4c0.1-0.1,0.2-0.3,0.2-0.5c0-0.2,0-0.2-0.1-0.3s-0.1-0.1-0.1-0.2l-0.4-0.1c-0.4-0.2-0.9-0.4-1.4-0.6c-0.5-0.2-1-0.3-1.4-0.5c-0.2,0.1-0.5,0.2-0.7,0.1c-0.2-0.1-0.5-0.1-0.8-0.2c-0.7,0-1.5-0.1-2.2-0.2c-0.7-0.1-1.4-0.3-2-0.6c0-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.2-0.1-0.4-0.1c-0.1-0.2-0.3-0.3-0.4-0.5c-0.1-0.2-0.3-0.3-0.4-0.5c0-0.3,0.1-0.5,0.1-0.7c0-0.2,0-0.4-0.2-0.5c0.1-0.3,0.2-0.5,0.2-0.7C133.8,6.3,133.8,6.1,133.7,5.8z M168.4,17.5c0,0.6,0,1-0.2,1.4c-0.2,0.4-0.4,0.7-0.7,1c-0.3,0.3-0.7,0.5-1.1,0.6c-0.4,0.2-0.8,0.3-1.2,0.4c-0.2-0.1-0.3-0.2-0.5-0.3s-0.4-0.2-0.6-0.3c-0.3-0.5-0.6-1-0.8-1.4c-0.3-0.4-0.4-0.9-0.4-1.4c0,0,0-0.1,0-0.4c0-0.2,0.1-0.5,0.1-0.7c0.1-0.3,0.1-0.5,0.1-0.7c0-0.2,0-0.4,0-0.4c-0.5,0-1,0.2-1.6,0.3c-0.5,0.2-1.1,0.4-1.6,0.6c-0.5,0.2-1,0.5-1.4,0.7c-0.5,0.2-0.8,0.5-1.1,0.6c0,0.1,0,0.2-0.1,0.5c-0.1,0.3-0.2,0.5-0.3,0.8c-0.1,0.3-0.3,0.5-0.4,0.7c-0.2,0.2-0.3,0.3-0.5,0.3c0,0.1,0.1,0.1,0.1,0.2c0,0.1,0,0.2,0,0.3c-0.3,0.1-0.6,0.2-0.8,0.3c-0.2,0.1-0.5,0.3-0.8,0.4c-0.3,0.1-0.5,0.1-0.9,0.1c-0.3,0-0.7-0.1-1.1-0.3c0-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.2-0.1-0.3-0.2c-0.1-0.1-0.1-0.1-0.1-0.1c-0.1,0-0.2-0.1-0.1-0.2c0-0.2,0.1-0.2,0.3-0.1c-0.2-0.3-0.4-0.6-0.4-0.8c-0.1-0.2-0.2-0.5-0.4-0.9c0,0,0.1-0.1,0.1-0.2c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0.1-0.2,0.1-0.2c0,0,0.1,0,0.2,0c0-0.2,0-0.4,0-0.5c0-0.1,0.1-0.3,0.1-0.4c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.4c0.4-0.5,0.6-1,0.7-1.6c0.1-0.6,0.2-1.1,0.2-1.5c0.2-0.2,0.3-0.4,0.4-0.7c0.1-0.3,0.2-0.6,0.3-0.9c0.1-0.3,0.1-0.6,0.1-0.9c0-0.3,0-0.5-0.1-0.7c0-0.1,0.1-0.1,0.4-0.1c0.1-0.7,0.2-1.5,0.3-2.5c0.1-0.9,0.1-1.8,0.3-2.6c0.1-0.8,0.4-1.5,0.7-2.2c0.3-0.6,0.8-1,1.5-1.2c0.1,0.5,0.3,0.9,0.5,1.2c0.2,0.4,0.4,0.7,0.7,1c0.3,0.3,0.5,0.6,0.8,1c0.3,0.3,0.5,0.7,0.7,1.1c0,0.4-0.1,0.9-0.1,1.4s-0.1,1.1-0.2,1.7c-0.1,0.6-0.2,1.1-0.3,1.6c-0.1,0.5-0.2,1-0.3,1.3c0,0.1,0.1,0.2,0.2,0.2c0.1,0,0.1,0.1,0,0.3c0.5-0.1,1.1-0.3,1.6-0.4c0.6-0.1,1.1-0.2,1.7-0.4c0.6-0.1,1.1-0.3,1.7-0.4s1-0.4,1.5-0.6c0.1-0.9,0.3-1.7,0.4-2.6s0.2-1.7,0.1-2.6c0.1-0.1,0.2-0.2,0.3-0.3c0.1-0.1,0.2-0.2,0.3-0.3c-0.1-0.4-0.2-0.9-0.1-1.3c0-0.5,0.1-0.9,0.2-1.3c0.1-0.4,0.4-0.8,0.6-1.1c0.3-0.3,0.6-0.5,1-0.6c0.2,0.4,0.4,0.7,0.6,1.1s0.4,0.8,0.5,1.2c0.2,0.4,0.4,0.8,0.6,1.2c0.2,0.4,0.5,0.7,0.8,0.9c0.1,0.6,0.1,1.3,0.1,2s0.1,1.4,0.1,2.1c-0.2,0.8-0.4,1.6-0.6,2.4c-0.2,0.8-0.4,1.6-0.4,2.5c-0.2,0.2-0.3,0.4-0.4,0.6c-0.1,0.2-0.2,0.4-0.3,0.7c-0.1,0.2-0.2,0.4-0.3,0.7c-0.1,0.2-0.2,0.4-0.4,0.5c0.1,0.3,0.1,0.5,0,0.7S168.2,17.1,168.4,17.5z M178.2,18.3c0.1,0.7,0.1,1.2-0.1,1.5c-0.2,0.4-0.4,0.7-0.8,0.8c-0.3,0.2-0.7,0.3-1.2,0.3c-0.4,0-0.9,0-1.3-0.1c-0.2-0.3-0.4-0.7-0.6-1.2s-0.3-0.9-0.5-1.4c-0.2-0.5-0.3-1-0.4-1.5c-0.1-0.5-0.2-0.9-0.3-1.3c0.1-0.3,0.2-0.6,0.2-0.9s0.1-0.7,0.1-1c0-0.4,0-0.7,0-1.1c0-0.4,0-0.7,0.1-1c0.2-0.9,0.4-1.8,0.7-2.6c0.3-0.8,0.5-1.7,0.7-2.6c0-0.2,0-0.4,0-0.6c-0.1-0.2-0.1-0.3,0-0.5c0-0.1,0.1-0.1,0.2-0.2c0.1,0,0.2-0.1,0.2-0.2c0.1-0.2,0.1-0.4,0-0.6c0-0.2,0-0.3,0.1-0.6c0.1-0.2,0.2-0.4,0.4-0.8c0.2-0.3,0.2-0.7,0.2-1.1c0.2-0.1,0.4-0.3,0.6-0.5s0.4-0.3,0.5-0.5c0.2-0.1,0.4-0.3,0.6-0.4c0.2-0.1,0.5-0.2,0.8-0.2c0.2,0.2,0.3,0.5,0.3,0.7c0.1,0.3,0.1,0.5,0.2,0.8c0.1,0.3,0.1,0.5,0.2,0.7c0.1,0.2,0.3,0.4,0.6,0.5c0,0.2,0,0.4,0,0.7c0,0.3,0,0.5,0,0.8c0,0.3,0.1,0.5,0.1,0.7c0.1,0.2,0.2,0.3,0.4,0.4c-0.4,1.1-0.8,2.1-1.1,3.1c-0.3,1-0.6,2.1-0.8,3.1c-0.2,1-0.4,2.1-0.4,3.1C177.9,16.2,178,17.3,178.2,18.3z M193.7,0.2c0.9,0.1,1.7,0.3,2.5,0.6c0.7,0.3,1.3,0.6,1.8,1.1c0.4,0.5,0.7,1.1,1,1.8s0.6,1.3,0.7,2S200,7,200,7.7c0,0.7-0.2,1.5-0.5,2.2c-0.6,0.8-1.4,1.4-2.3,1.9c-0.9,0.5-1.9,0.9-2.9,1.2c-1.1,0.3-2.1,0.6-3.3,0.8c-1.1,0.2-2.2,0.4-3.3,0.7c0,0.5,0,1,0,1.6c0,0.6-0.1,1.1-0.2,1.7c-0.1,0.6-0.2,1.1-0.4,1.6c-0.2,0.5-0.5,1-0.8,1.4c-0.4,0-0.8,0-1.2,0.1c-0.4,0.1-0.7,0.2-1.1,0.2c-0.2-0.1-0.3-0.2-0.4-0.2c-0.1-0.1-0.3-0.1-0.5-0.1c0-0.2,0-0.4-0.2-0.7c-0.2-0.2-0.3-0.5-0.5-0.7c-0.2-0.2-0.3-0.5-0.3-0.7c-0.1-0.2,0-0.4,0.3-0.6c-0.2-0.2-0.2-0.4-0.2-0.6c0.2-0.3,0.3-0.6,0.4-1.1c0.1-0.5,0.2-0.9,0.3-1.4c0.1-0.4,0.1-0.9,0-1.2c0-0.4-0.3-0.6-0.7-0.8c0,0,0.1-0.1,0.1-0.1c0.1,0,0.1,0,0.2,0c-0.2-0.1-0.3-0.3-0.3-0.6c0-0.2-0.1-0.4-0.2-0.6c0-0.1,0.1-0.2,0.1-0.3c0-0.1,0.1-0.1,0.2-0.1c-0.1-0.4-0.1-0.8,0-1.1c0.1-0.4,0.2-0.7,0.3-1c0.2-0.3,0.3-0.6,0.6-0.9c0.2-0.3,0.4-0.6,0.6-0.9c0.2,0,0.2,0,0.3,0.1s0.1,0.1,0.2,0c0.3-0.2,0.5-0.4,0.6-0.7c0.1-0.2,0.2-0.5,0.2-0.8c0.2-0.5,0.3-0.9,0.6-1.4c0.2-0.5,0.5-0.9,0.7-1.3c0.3-0.4,0.6-0.7,0.9-1.1c0.3-0.3,0.7-0.5,1-0.7c0.3-0.2,0.6-0.4,1-0.6c0.4-0.2,0.8-0.3,1.3-0.4s0.9-0.2,1.4-0.2c0.5,0,0.9,0,1.3,0.1c0.1,0,0.2,0,0.2-0.1C193.6,0.3,193.7,0.2,193.7,0.2z M192.4,8.1c0.5-0.2,1.1-0.4,1.8-0.6c0.6-0.1,1.1-0.4,1.6-0.8c-0.3-0.2-0.6-0.5-1.2-0.6c-0.5-0.2-1.1-0.3-1.6-0.3c-0.6,0-1.2,0-1.8,0.1c-0.6,0.1-1.1,0.4-1.6,0.8c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.5-0.4,0.8c0,0.1,0.1,0.2,0.2,0.2c0.1,0.1,0.2,0.1,0.2,0.2c0.5-0.3,1-0.4,1.5-0.4S191.8,8.3,192.4,8.1z M178.8,31.7c-0.2-0.2-0.4-0.3-0.7-0.3c-0.3-0.1-0.5-0.1-0.8-0.2c-0.3-0.1-0.5-0.1-0.7-0.2c-0.2-0.1-0.4-0.3-0.5-0.5c-0.2,0-0.4,0-0.7,0c-0.3,0-0.5,0-0.8-0.1c-0.3,0-0.5-0.1-0.7-0.2c-0.2-0.1-0.3-0.2-0.4-0.3c-1.1,0.3-2.1,0.6-3.2,0.8c-0.9,0.2-2.2,0.1-3.1,0.1c-0.8,0-1.6,0-2.4,0.1c-0.1,0-0.2,0-0.2,0c-0.1,0-0.4,0-0.5-0.1c0-0.1-0.2-0.3-0.5-0.3c-0.2,0-0.3,0.1-0.2,0.2c-0.4-0.2-0.7-0.2-1-0.2c-0.3,0-0.7-0.1-1.1-0.3c-0.1,0-0.2,0-0.3,0.1c-0.2,0-0.3,0-0.4,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0.1,0,0.1c-0.3,0-0.5,0-0.7,0c-0.2,0-0.4,0-0.5,0.1c-0.2,0-0.3,0-0.5,0c-0.2,0-0.3,0-0.5-0.1c-0.7,0.2-1.5,0.1-2.2,0.2c-0.8,0-1.5,0.1-2.1,0c-0.2,0.1-0.5,0.2-1,0.3c-0.2,0-0.3,0.1-0.5,0.1c-0.8,0-1.6,0-2.5,0c-0.1,0-0.2,0-0.3-0.1c-0.1,0-0.4,0-0.7,0.1c-4,0-8,0-12.1,0.3c-1.2,0.1-2.4,0.2-3.6,0.3c-2.5,0-5-0.4-7.4-0.2c-1.8,0.1-3.5,0.1-5.3,0.2l0.2,0c-1.3,0-1.6,0-2.6,0c-0.3-0.2-0.8-0.2-0.9-0.2c-0.2,0.1-0.5,0.1-0.9,0.2c-0.4,0-0.8,0.1-1.2,0c-0.4,0-0.8-0.1-1.2-0.1c-0.4-0.1-0.7-0.2-1-0.3c-0.6,0-1.3,0-2.1,0c-0.8,0-1.5,0.1-2.2-0.1c-0.2,0.1-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5,0c-0.2,0-0.3,0-0.5,0c-0.2,0-0.4,0-0.7,0c0.1,0,0.1-0.1,0-0.1c-0.1,0-0.2,0-0.3,0c-0.1,0-0.3,0-0.4,0c-0.2,0-0.3,0-0.3,0c-0.4,0.1-0.8,0.3-1.1,0.3c-0.3,0.1-0.6,0-1,0.2c0.1-0.1,0-0.2-0.2-0.2c-0.2,0-0.5,0.2-0.5,0.3c-0.1,0-0.3,0-0.5,0.1c-0.2,0-1.6,0.2-2.6,0.3c-3.6,0.1-7.2,0.1-10.9,0.2c-0.1,0-0.2,0-0.3,0c-2.4,0-4.9,0.2-7.3,0.2c-1.2,0-2.5,0-3.7,0c-0.3,0-3.6,0.1-3.6,0c-2.3,0-3.7,0-5.5,0c-0.2-0.2-0.6-0.1-0.9-0.1c-0.4,0.1-1,0.1-1.6,0.2c-0.7,0-1.4,0.1-2.1,0c-0.7,0-1.5-0.1-2.2-0.1c-0.7-0.1-1.3-0.2-1.7-0.3c-1.1,0-2.4,0.1-3.8,0c-1.4,0-2.8,0.1-4-0.1c-0.3,0.1-0.6,0.1-0.9,0.1c-0.3,0-0.6,0-0.8,0c-0.3,0-0.6,0-0.9,0c-0.3,0-0.7,0-1.2,0.1c0.1,0,0.2-0.1,0-0.1c-0.1,0-0.3,0-0.5,0c-0.2,0-0.5,0-0.8,0c-0.3,0-0.5,0-0.6,0c-0.8,0.2-1.5,0.3-2,0.3c-0.5,0.1-1.2,0-1.8,0.2c0.2-0.1,0-0.2-0.4-0.2c-0.4,0-1,0.2-0.9,0.3c-0.1,0-0.6,0-0.9,0.1c-0.4,0-4.6,0.2-6.2,0.3c-1.7,0-3.3,0-4.9,0.2c-1.1,0.1-2.1,0.2-3.2,0.2c-1.1,0-2.1-0.1-3.2-0.3c-0.7-0.1-1.2-0.1-1.6,0c-0.4,0.1-0.7,0.4-0.9,0.7c-0.2,0.3-0.3,0.6-0.4,1c0,0.4,0,0.7,0,1.1c0.3,0.2,0.7,0.4,1.1,0.6c0.5,0.2,0.9,0.3,1.4,0.5c0.5,0.2,1,0.3,1.4,0.4c0.5,0.1,0.9,0.2,1.3,0.3c0.3-0.1,0.6-0.1,0.9-0.1c0.3,0,0.7,0,1,0c0.4,0,0.7,0,1.1,0c0.4,0,0.7,0,1,0c2-0.2,4.1-0.2,6.1-0.2l-0.9,0c0.1,0,0.3,0,0.6,0c0.3,0,0.6,0,1,0c0.4,0,0.7,0,1.1,0.1c0.4,0,0.7,0.1,0.9,0.2c0.3-0.1,0.6-0.2,0.9-0.3c0.3-0.1,0.7-0.1,1-0.1c0.2,0,0.3,0.1,0.4,0.2c0.2,0,0.4,0,0.5-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2,0,0.3-0.1c0.1,0,0.2,0,0.3,0c0,0,0.1,0,0.1,0c0.1,0,0.2,0.1,0.3,0.1c0.1,0.1,0.3,0.1,0.4,0.1c0.8,0,1.4-0.1,1.7-0.2c4.3-0.1,8.5-0.1,12.8-0.2c1.9,0,3.1-0.2,3.6-0.1c0.5-0.1,0.9-0.2,1.3-0.2c0.4,0,0.8,0,1.3,0c0.5-0.2,0.9-0.2,1.4-0.2c0.5,0,0.9,0,1.2,0l0.5,0c0.2-0.1,0.3-0.1,0.4-0.1c0.1,0,0.2,0,0.3-0.1c0.3,0,0.6,0.1,0.9,0c0.1,0,0.2,0,0.4,0c0.1,0,0.2,0,0.3,0.1c0.2,0,0.4,0,0.6,0c0.1,0,0.3,0.1,0.4,0.1c0.1,0,0.3,0,0.4,0.1c0,0,1,0,1.8,0c4.9-0.1,5.7-0.1,5.7-0.1c1.1,0,3.9,0.2,4.7,0.2c0.1,0,0.3,0,0.4-0.1c0.1,0,0.2-0.1,0.3-0.1c0.1,0,0.2,0,0.3-0.1c0.1,0,0.3,0,0.4,0.1c0.1,0,0.3,0.1,0.4,0.1c0.2,0.1,0.3,0.1,0.5,0.1c0.1-0.1,0.2-0.2,0.4-0.2c0.3,0,0.7,0,1,0.1c0.3,0.1,0.6,0.2,0.9,0.2c0.2-0.1,0.5-0.2,0.9-0.3c0.4-0.1,0.7-0.1,1.1-0.1c0.4,0,0.7,0,1,0c0.2,0,3-0.1,4.2-0.2c3.2,0,6.3-0.1,9.5-0.1c0,0,0.1,0,0.1,0c0.7,0,1.8-0.1,1.8-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.2,0,0.4,0,0.6,0c0.1,0,0.1-0.1,0.3-0.1c0.1,0,0.3,0,0.4,0c0.3,0,0.6,0,0.9-0.1c0.1,0,0.2,0,0.3,0c0.1,0,0.2,0,0.4,0.1l0.5-0.1c0.3,0,0.7,0,1.2-0.1c0.5,0,0.9,0,1.4,0.2c0.5-0.1,0.9-0.1,1.3-0.1c0.4,0,0.8,0.1,1.3,0.1c0.6-0.1,1.7,0.1,3.6,0.1c2.6,0,5.3,0,7.9-0.1c3.2,0,6.5-0.1,9.7-0.2c1.1,0,2.3,0.1,2.5,0.1c0.3,0,0.6,0,1,0c0.4,0,0.7,0,1.1,0.1c0.4,0,0.7,0.1,0.9,0.2c0.3-0.1,0.6-0.2,0.9-0.3c0.3-0.1,0.7-0.1,1-0.1c0.2,0,0.3,0.1,0.4,0.2c0.2,0,0.4,0,0.5-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.3-0.1,0.4-0.1c0.1,0,0.2,0,0.3,0c0.1,0,0.2,0.1,0.3,0.1c0.1,0.1,0.3,0.1,0.4,0.1c0.9,0,3.7-0.5,4.8-0.5c0,0,4.3-0.1,8.4-0.2c2.5,0,5-0.1,7.5-0.2c0.9,0,2,0.1,2.9-0.1c0.8-0.2,1.7-0.4,2.6-0.5c0.2,0,0.4,0,0.6,0.1c0.2,0.1,0.3,0.1,0.5,0.1c0.1,0,0.2-0.1,0.2-0.2c0-0.1,0.1-0.1,0.2-0.2c0.2,0,0.4-0.1,0.6,0c0.2,0,0.3,0,0.6-0.1c0.2-0.1,0.4-0.2,0.8-0.3c0.3-0.1,0.7-0.2,1.1-0.1c0.1-0.2,0.3-0.3,0.5-0.5c0.2-0.1,0.4-0.3,0.5-0.5c0.2-0.2,0.3-0.3,0.4-0.5C178.8,32.3,178.8,32,178.8,31.7z" /></svg></a>
            </div>
        </div>
        <div id="row_04">
            <h2>© 2019 Colossal, all rights reserved. We try our best to attribute images, videos, and quotes to their creators and original sources. If you see something on Colossal that is misattributed or you would like removed, please <a href="/contact/">contact us</a>. Colossal participates in affiliate marketing programs and may earn a commission on sales through links to Etsy, Amazon, Society6, and iTunes.</h2>
        </div>
        <div class="clear"></div>
    </footer>
</div>

<script type="68fa48e47a44e50bd58b7be7-text/javascript">
    (function(){
      var test = document.createElement('div');
      test.innerHTML = '&nbsp;';
      test.className = 'adsbox';
      document.body.appendChild(test);
      window.setTimeout(function() {
        if (test.offsetHeight === 0) {
          document.body.classList.add('adblock');
        }
        test.remove();
      }, 100);
    })();
    </script>

<script type="68fa48e47a44e50bd58b7be7-text/javascript">document.querySelector('.searchbox [type="reset"]').addEventListener('click', function() {  this.parentNode.querySelector('input').focus();});</script>

<div id="fb-root"></div>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1453681758275451";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script type="68fa48e47a44e50bd58b7be7-text/javascript">window.twttr = (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function(f) {
    t._e.push(f);
    };

    return t;
    }(document, "script", "twitter-wjs"));</script>

<script async defer src="//assets.pinterest.com/js/pinit.js" type="68fa48e47a44e50bd58b7be7-text/javascript"></script>

<script src="https://apis.google.com/js/platform.js" async defer type="68fa48e47a44e50bd58b7be7-text/javascript"></script>

<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type="68fa48e47a44e50bd58b7be7-text/javascript">(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='ENTRYID';ftypes[4]='text';fnames[5]='ORDERID';ftypes[5]='text';fnames[6]='EVENT';ftypes[6]='text';fnames[7]='COMPANY';ftypes[7]='text';fnames[8]='TITLE';ftypes[8]='text';fnames[9]='TWITTER';ftypes[9]='text';fnames[10]='WEBSITE';ftypes[10]='text';fnames[11]='MMERGE11';ftypes[11]='text';fnames[12]='WEEKLY';ftypes[12]='radio';fnames[13]='DAILY';ftypes[13]='radio';fnames[14]='FREQUENCY';ftypes[14]='radio';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

<script src="/wp-content/themes/colossal-v4/js/fitvids.js" type="68fa48e47a44e50bd58b7be7-text/javascript"></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
    /*
     * jQuery throttle / debounce - v1.1 - 3/7/2010
     * httpsbenalman.com/projects/jquery-throttle-debounce-plugin/
     *
     * Copyright (c) 2010 "Cowboy" Ben Alman
     * Dual licensed under the MIT and GPL licenses.
     * httpsbenalman.com/about/license/
     */
    (function(b,c){var $=b.jQuery||b.Cowboy||(b.Cowboy={}),a;$.throttle=a=function(e,f,j,i){var h,d=0;if(typeof f!=="boolean"){i=j;j=f;f=c}function g(){var o=this,m=+new Date()-d,n=arguments;function l(){d=+new Date();j.apply(o,n)}function k(){h=c}if(i&&!h){l()}h&&clearTimeout(h);if(i===c&&m>e){l()}else{if(f!==true){h=setTimeout(i?k:l,i===c?e-m:e)}}}if($.guid){g.guid=j.guid=j.guid||$.guid++}return g};$.debounce=function(d,e,f){return f===c?a(d,e,false):a(d,f,e!==false)}})(this);
</script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
    $(function() {
        // BJ Lazy Load/fitVids shim
        // Create array of eventual iFrames
        var candidates = [];

        $('.lazy-hidden[data-lazy-type="iframe"]')
            .each(function() {
                $t = $(this);

                candidates.push({
                    img: $t,
                    p: $t.parent('p')
                });
            });

        // Check all lazyloadable elements to see if they have been loaded
        var checkCandidates = function() {
            var initedIndexes = [];

            $.each(candidates, function(i, v) {
                if (v.img.closest('body').length === 0) {
                    v.p.addClass('video-parent')
                        .fitVids();

                    initedIndexes.push(i);
                }
            });

            if (initedIndexes) {
                for (var i = 0; i < initedIndexes.length; i++) {
                    candidates.splice(initedIndexes[i],1);
                }
            }
        };

        // call checkCandidate onScroll once per half second
        $(window).on('scroll', $.throttle(500, $.proxy(checkCandidates, this)));

        // Init fitVids for immediate iFrames
        setTimeout(function() {
            $('.entry-content iframe')
                .parent('p')
                .addClass('video-parent')
                .fitVids();
        }, 1000);
    });
</script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
            (function(){
                document.addEventListener('DOMContentLoaded', function(){
                    let wpp_widgets = document.querySelectorAll('.popular-posts-sr');

                    if ( wpp_widgets ) {
                        for (let i = 0; i < wpp_widgets.length; i++) {
                            let wpp_widget = wpp_widgets[i];
                            WordPressPopularPosts.theme(wpp_widget);
                        }
                    }
                });
            })();
        </script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
            var WPPImageObserver = null;

            function wpp_load_img(img) {
                if ( ! 'imgSrc' in img.dataset || ! img.dataset.imgSrc )
                    return;

                img.src = img.dataset.imgSrc;

                if ( 'imgSrcset' in img.dataset ) {
                    img.srcset = img.dataset.imgSrcset;
                    img.removeAttribute('data-img-srcset');
                }

                img.classList.remove('wpp-lazyload');
                img.removeAttribute('data-img-src');
                img.classList.add('wpp-lazyloaded');
            }

            function wpp_observe_imgs(){
                let wpp_images = document.querySelectorAll('img.wpp-lazyload'),
                    wpp_widgets = document.querySelectorAll('.popular-posts-sr');

                if ( wpp_images.length || wpp_widgets.length ) {
                    if ( 'IntersectionObserver' in window ) {
                        WPPImageObserver = new IntersectionObserver(function(entries, observer) {
                            entries.forEach(function(entry) {
                                if (entry.isIntersecting) {
                                    let img = entry.target;
                                    wpp_load_img(img);
                                    WPPImageObserver.unobserve(img);
                                }
                            });
                        });

                        if ( wpp_images.length ) {
                            wpp_images.forEach(function(image) {
                                WPPImageObserver.observe(image);
                            });
                        }

                        if ( wpp_widgets.length ) {
                            for (var i = 0; i < wpp_widgets.length; i++) {
                                let wpp_widget_images = wpp_widgets[i].querySelectorAll('img.wpp-lazyload');

                                if ( ! wpp_widget_images.length && wpp_widgets[i].shadowRoot ) {
                                    wpp_widget_images = wpp_widgets[i].shadowRoot.querySelectorAll('img.wpp-lazyload');
                                }

                                if ( wpp_widget_images.length ) {
                                    wpp_widget_images.forEach(function(image) {
                                        WPPImageObserver.observe(image);
                                    });
                                }
                            }
                        }
                    } /** Fallback for older browsers */
                    else {
                        if ( wpp_images.length ) {
                            for (var i = 0; i < wpp_images.length; i++) {
                                wpp_load_img(wpp_images[i]);
                                wpp_images[i].classList.remove('wpp-lazyloaded');
                            }
                        }

                        if ( wpp_widgets.length ) {
                            for (var j = 0; j < wpp_widgets.length; j++) {
                                let wpp_widget = wpp_widgets[j],
                                    wpp_widget_images = wpp_widget.querySelectorAll('img.wpp-lazyload');

                                if ( ! wpp_widget_images.length && wpp_widget.shadowRoot ) {
                                    wpp_widget_images = wpp_widget.shadowRoot.querySelectorAll('img.wpp-lazyload');
                                }

                                if ( wpp_widget_images.length ) {
                                    for (var k = 0; k < wpp_widget_images.length; k++) {
                                        wpp_load_img(wpp_widget_images[k]);
                                        wpp_widget_images[k].classList.remove('wpp-lazyloaded');
                                    }
                                }
                            }
                        }
                    }
                }
            }

            document.addEventListener('DOMContentLoaded', function() {
                wpp_observe_imgs();

                // When an ajaxified WPP widget loads,
                // Lazy load its images
                document.addEventListener('wpp-onload', function(){
                    wpp_observe_imgs();
                });
            });
        </script>
<link rel='stylesheet' id='arpw-style-css' href='https://www.thisiscolossal.com/wp-content/plugins/advanced-random-posts-widget/assets/css/arpw-frontend.css?ver=5.2.5' type='text/css' media='all' />
<link rel='stylesheet' id='mediaelement-css' href='https://www.thisiscolossal.com/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css' href='https://www.thisiscolossal.com/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.2.5' type='text/css' media='all' />
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
/* <![CDATA[ */
var emailL10n = {"ajax_url":"https:\/\/www.thisiscolossal.com\/wp-admin\/admin-ajax.php","max_allowed":"5","text_error":"The Following Error Occurs:","text_name_invalid":"- Your Name is empty\/invalid","text_email_invalid":"- Your Email is empty\/invalid","text_remarks_invalid":"- Your Remarks is invalid","text_friend_names_empty":"- Friend Name(s) is empty","text_friend_name_invalid":"- Friend Name is empty\/invalid: ","text_max_friend_names_allowed":"- Maximum 5 Friend Names allowed","text_friend_emails_empty":"- Friend Email(s) is empty","text_friend_email_invalid":"- Friend Email is invalid: ","text_max_friend_emails_allowed":"- Maximum 5 Friend Emails allowed","text_friends_tally":"- Friend Name(s) count does not tally with Friend Email(s) count","text_image_verify_empty":"- Image Verification is empty"};
/* ]]> */
</script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/plugins/wp-email/email-js.js?ver=2.67.6'></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-content/themes/colossalv3/js/navigation.js?ver=1.0'></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/wp-embed.min.js?ver=5.2.5'></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
</script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1'></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.2.5'></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript">
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.2.5'></script>
<script type="68fa48e47a44e50bd58b7be7-text/javascript" src='https://www.thisiscolossal.com/wp-includes/js/mediaelement/renderers/vimeo.min.js?ver=4.2.6-78496d1'></script>

<script src="https://www.dwin2.com/pub.227305.min.js" type="68fa48e47a44e50bd58b7be7-text/javascript"></script>

<script type="68fa48e47a44e50bd58b7be7-text/javascript">
  (function (w,i,d,g,e,t,s) {w[d] = w[d]||[];t= i.createElement(g);
    t.async=1;t.src=e;s=i.getElementsByTagName(g)[0];s.parentNode.insertBefore(t, s);
  })(window, document, '_gscq','script','//widgets.getsitecontrol.com/37023/script.js');
</script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="68fa48e47a44e50bd58b7be7-|49" defer=""></script></body>
</html>
